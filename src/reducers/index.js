import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import karma from './karma/'
import authorization from './authorization/'
import market from './market/'


const app = combineReducers({
  routing: routerReducer,
  karma,
  authorization,
  market,
})

export default app

