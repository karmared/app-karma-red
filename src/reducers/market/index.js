import { combineReducers } from 'redux'

import filters from './filters'


const app = combineReducers({
  filters,
})

export default app
