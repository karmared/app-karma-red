const defaults = {
  rate: [0, 100],
  collateral: [0, 3000000],
  amount: [0, 100000000],
  currency: '',
};

const reducer = (state = defaults, action) => {
  switch (action.type) {
    case 'SET_FILTERS':
      return action.payload;

    case 'LOG_OUT':
      return defaults;

    default:
      return state
  }
};

export default reducer
