import {combineReducers} from 'redux'

import currencies from './currencies'
import user from './user'
import keys from './keys'
import balance from './balance'
import quotations from './quotations'
import global from './global'
import gateways from './gateways'
import fees from './fees'


const app = combineReducers({
    currencies,
    user,
    keys,
    balance,
    quotations,
    global,
    gateways
});

export default app

export const getUserData = state => state.karma.user;
export const getUserId = state => state.karma.user.id;
export const getCurrencies = state => state.karma.currencies;
export const getUserKeys = state => state.karma.keys;
export const getUserBalance = state => state.karma.balance;
export const getQuotations = state => state.karma.quotations;
export const getGlobal = state => state.karma.global;
export const getUserGateways = state => state.karma.gateways;
export const getFees = state => state.karma.fees;
