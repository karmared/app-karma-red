import { combineReducers } from 'redux'

import init from './init'
import authorized from './authorized'
import loading from './loading'
import requestPassword from './request-password'
import prvKey from './prv-key'


const app = combineReducers({
  init,
  requestPassword,
  prvKey,
  authorized,
  loading,
})

export default app

export const isInit = state => state.authorization.init
export const isAuthorized = state => state.authorization.authorized
export const isLoading = state => state.authorization.loading
export const isRequestPassword = state => state.authorization.requestPassword
export const getPrvKey = state => state.authorization.prvKey
