const reducer = (state = false, action) => {
  switch (action.type) {
    case 'INIT_LOADING':
      return true

    case 'SET_USER':
      return false

    case 'INIT_ERROR':
      return false

    case 'NOT_LOGGED_IN':
      return false

    default:
      return state
  }
}

export default reducer
