const reducer = (state = false, action) => {
  switch (action.type) {
    case 'SET_USER':
      return true

    case 'NOT_LOGGED_IN':
      return true

    case 'INIT_ERROR':
      return false

    default:
      return state
  }
}

export default reducer
