const reducer = (state = '', action) => {
  switch (action.type) {
    case 'INIT_LOADING':
      return ''

    case 'SET_PRIVATE_KEY':
      return action.payload

    default:
      return state
  }
}

export default reducer
