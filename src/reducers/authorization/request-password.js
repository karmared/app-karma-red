const reducer = (state = false, action) => {
  switch (action.type) {
    case 'INIT_LOADING':
      return false

    case 'REQUEST_PASSWORD':
      return action.payload

    case 'SET_PRIVATE_KEY':
      return false

    default:
      return state
  }
}

export default reducer
