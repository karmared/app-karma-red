// Application's config data

// define Mainnet & Testnet
const NODE_ENV = process.env.NODE_ENV || 'development';

export const isTestnet = NODE_ENV !== 'production';

// // API Addresses
const protocol = window.location.protocol;

// Facebook Administrator ID
export const FacebookId = '1755375728090614';

// general data for MainNet
let chainIdString = 'c85b4a30545e09c01aaa7943be89e9785481c1e7bd5ee7d176cb2b3d8dd71a70',
    core_asset = 'KRM',
    prefix = 'KRM',
    paths = {
        extraBase: `${protocol}//api.karma.red/`,
        extraApi: `${protocol}//api.karma.red/api/v1`,
        chainSocket: `${protocol === 'https:' ? 'wss' : 'ws'}://node.karma.red`,
        chainApi: `${protocol}//faucet.karma.red/api/v1`,
        multiFAApi: `https://2fa.karma.red/api`,
        explorer: `https://explorer.karma.red/#/`,
        gate: `https://gate.karma.red/`,
        gateway: 'https://krmgw.karma.red'
    }

// If testnet detected
if (isTestnet) {
        chainIdString = 'e81bea67cebfe8612010fc7c26702bce10dc53f05c57ee6d5b720bbe62e51bef';
        core_asset = 'KRMT';
        prefix = 'KRMT';
        paths = {
            extraBase: `${protocol}//testnet-api.karma.red/`,
            extraApi: `${protocol}//testnet-api.karma.red/api/v1`,
            chainSocket: `${protocol === 'https:' ? 'wss' : 'ws'}://testnet-node.karma.red`,
            chainApi: `${protocol}//testnet-faucet.karma.red/api/v1`,
            multiFAApi: `https://testnet-middleware.karma.red/api`,
            explorer: `https://testnet-explorer.karma.red/#/`,
            gate: `https://testnet-gate.karma.red/`,
            gateway: 'https://testnet-krmgw.karma.red'
        }
}

export const ChainId = chainIdString;
export const ApiURI = paths;
export const CoreAsset = core_asset;
export const Prefix = prefix;
