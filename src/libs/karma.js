import {Apis, ChainConfig} from 'karmajs-ws'
import {ChainStore, PrivateKey} from 'karmajs'
import {ChainId, ApiURI, Prefix, CoreAsset} from './'

ChainConfig.networks.Karma = {
    core_asset: CoreAsset,
    address_prefix: Prefix,
    chain_id: ChainId,
}

ChainConfig.setPrefix(Prefix)


// Main Karma class to connect to ChainStore
export class Karma {
    constructor() {
        this.dbApi = null
        this.historyApi = null
    }

    // Initialization
    static async init() {
        const a = await Apis.instance(ApiURI.chainSocket, true).init_promise
        const b = await ChainStore.init()
        Promise.all([
            a, //await Apis.instance(ApiURI.chainSocket, true).init_promise,
            b, //await ChainStore.init(),
            this.dbApi = await Apis.instance().db_api(),
            this.historyApi = await Apis.instance().history_api(),
        ])
    }

    // Generate keys
    static generateKeys(login, password) {
        const seed = `${login}active${password}`
        const prv = PrivateKey.fromSeed(seed)
        const privKey = prv.toWif()
        const pubKey = prv.toPublicKey()
            .toString()

        const seedOwner = `${login}owner${password}`
        const pubKeyOwner = PrivateKey.fromSeed(seedOwner)
            .toPublicKey()
            .toString()

        return {privKey, pubKey, pubKeyOwner}
    }
}
