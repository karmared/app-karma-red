import React from 'react'
import ReactDom from 'react-dom'
import {AppContainer} from 'react-hot-loader'
import {Provider} from 'react-redux'
import {applyMiddleware, createStore, compose} from 'redux'
import {routerMiddleware, ConnectedRouter} from 'react-router-redux'
import {Route} from 'react-router-dom'
import {createLogger} from 'redux-logger'
import _ from 'lodash'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'

import './styles/style.scss'
import reducers from './reducers'
import Root from './root'


const logger = createLogger({
    predicate: (getState, action) => {
        const hideActions = []

        return !_.some(_.map(hideActions, (hideAction) => {
            if (action.type !== hideAction) return null
            return true
        }), null)
    },
})


const history = createHistory()
const middleware = routerMiddleware(history)

let store = createStore(
    reducers,
    {},
    compose(applyMiddleware(thunk, middleware)),
)

if (process.env.NODE_ENV !== 'production') {
    store = createStore(
        reducers,
        {},
        compose(
            applyMiddleware(thunk, logger, middleware),
            window.devToolsExtension ? window.devToolsExtension() : f => f,
        ),
    )
}

if (module.hot) {
    module.hot.accept('./reducers', () => {
        const nextRootReducer = require('./reducers')

        store.replaceReducer(nextRootReducer)
    })
}

const render = (Component) => {
    ReactDom.render(
        <AppContainer>
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Route component={Component}/>
                </ConnectedRouter>
            </Provider>
        </AppContainer>,
        document.getElementById('root'),
    )
}

render(Root)

if (module.hot) {
    module.hot.accept('./root', () => {
        render(Root)
    })
}
