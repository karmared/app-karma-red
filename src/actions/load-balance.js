import {Karma, KarmaMultiplier} from '../libs/'
import {getUserData, getCurrencies} from '../reducers/karma/'
import {CoreAsset} from '../libs'

export const loadBalance = userId => async (dispatch, getState) => {
    const {id} = getUserData(getState());
    const currencies = getCurrencies(getState());

    const ret = await Karma.dbApi.exec('get_account_balances', [(userId || id), []])
        .then(e => e)
        .catch((error) => {
            throw new Error(error.message)
        });

    const data = Array.from(ret
        .filter(item => currencies.filter(asset => asset.id === item.asset_id).length > 0)
        .map((item) => {
            let precision, symbol;
            const consilience = currencies.filter(asset => asset.id === item.asset_id);
            symbol = consilience[0].symbol;
            precision = consilience[0].precision
            return ({
                amount: item.amount / (10**precision),
                id: item.asset_id,
                symbol,
                symbolPublic: (symbol === CoreAsset ) ? symbol : symbol.substr(1),
                crypto: (symbol === CoreAsset || symbol === 'KBTC' || symbol === 'KETH'),
            })
        })
    )

    dispatch({type: 'SET_BALANCE', payload: data})
};
