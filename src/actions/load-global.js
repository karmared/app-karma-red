import {Karma} from '../libs'
import {ChainTypes} from 'karmajs'

export const loadGlobal = () => async (dispatch) => {
    let data = await Karma.dbApi.exec('get_global_properties', []).then(e => e);

    const operations = data.parameters.current_fees.parameters.map((e, index) => {
        let operation = '';
        for ( let i in ChainTypes.operations) {
            if(e[0] === ChainTypes.operations[i]){
                operation = i;
            }
        }

        return {
            operation,
            fee: e[1].fee || 0,
            price_per_kbyte: e[1].price_per_kbyte
        }
    })

    data = {...data, operations}

    dispatch({type: 'SET_GLOBAL', payload: data})
};
