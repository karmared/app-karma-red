import { Karma, CoreAsset } from '../libs'

export const loadQuotations = () => async (dispatch) => {

    const getCurrency = () => Karma.dbApi.exec('list_last_exchange_rates', []).then(e => e);

    const karmaCurrency = await getCurrency()
        .then(data => data
            .filter(e => e[0].indexOf('KARMA') === -1 )
            .map(item => {
                let USD = data.filter(cur => cur[0] === 'KUSD' || cur[0] === 'KUSDT')[0];
                return {
                    id: item[0],
                    symbol: item[0] === CoreAsset ? item[0] : item[0].substring(1, item[0].length),
                    price_usd: +item[1] / +USD[1]
                }
            })
        )
        .catch(function (error) {
            console.log(error);
        });

    dispatch({type: 'SET_QUOTATIONS', payload: karmaCurrency })
};
