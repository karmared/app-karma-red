import {Karma} from '../libs'

export const loadFees = () => async (dispatch) => {
    const data = await Karma.dbApi.exec('get_global_properties', []).then(e => e.parameters.current_fees.parameters);
    dispatch({type: 'SET_FEES', payload: data})
};
