import {Karma} from '../../libs/karma'

export const getCommittee = async (votes = [], global = []) => {
    const usersID = await Karma.dbApi.exec('lookup_committee_member_accounts', ['', 100])
        .then(e => e)
        .then(e => {
            let arrID = []
            e.map(item => arrID.push(item[1]))
            return arrID
        })

    let allCommittee = await Karma.dbApi.exec('get_committee_members', [usersID])
        .then(e => e)

    const filterList = allCommittee.map(item => item['committee_member_account']) //get id`s array
    const userInfo = await Karma.dbApi.exec('get_accounts', [filterList])
        .then(e => e) //get accounts

    allCommittee = await allCommittee
        .map(user => ({
            ...user,
            name: userInfo.filter(item => item.id === user.committee_member_account)[0].name,
            status: global.filter(e => e === user.id).length > 0
        }))
        .sort((prev, next) => {
            return (prev.total_votes > next.total_votes) ? -1 : ((next.total_votes > prev.total_votes) ? 1 : 0)
        })

    const committee = allCommittee.filter(user => {
        let status = false
        for (let i = 0; i < votes.length; i++) {
            if (votes[i] === user.vote_id) {
                status = true
            }
        }
        if (status === true) {
            return user
        }
    })
        .map((item, i) => ({vote_id: item.vote_id, member: item.name, approve: true})) //array reduction to the actual format

    return {committee, allCommittee}
}
