import {PrivateKey, TransactionBuilder} from 'karmajs'

import {getUserData, getUserKeys} from '../../reducers/karma/'
import {setUser} from "../user";


export const voteMembers = (data) => async (dispatch, getState) => {
    const {id, options} = getUserData(getState());
    const user = getUserData(getState());
    const {privKey, pubKey} = getUserKeys(getState());

    const prv = PrivateKey.fromWif(privKey);

    let new_options = {
        ...options,
        voting_account: data.id,
        votes: data.votes
    };

    const tr = new TransactionBuilder();

    await tr.add_type_operation("account_update", {
        account: id,
        new_options
    });

    await tr.set_required_fees();
    await tr.add_signer(prv, pubKey);
    await tr.broadcast();
    console.log('result: Completed.');

    await dispatch(setUser({...user, options: new_options}));

    return 'done'
};
