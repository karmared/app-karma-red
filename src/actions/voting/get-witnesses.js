import {Karma} from '../../libs/karma'

export const getWitnesses = async (votes = [], global = []) => {
    const usersID = await Karma.dbApi.exec('lookup_witness_accounts', ['', 100])
        .then(e => e)
        .then(e => e.map(item => item[1]))

    let allWitness = await Karma.dbApi.exec('get_witnesses', [usersID])
        .then(e => e)

    const filterList = allWitness.map(item => item['witness_account']) //get id`s array
    const userInfo = await Karma.dbApi.exec('get_accounts', [filterList])
        .then(e => e) //get accounts

    allWitness = await allWitness
        .map(user => ({
            ...user,
            name: userInfo.filter(item => item.id === user.witness_account)[0].name,
            status: global.filter(e => e === user.id).length > 0
        }))

    const witness = allWitness
        .filter(user => {
            let status = false
            for (let i = 0; i < votes.length; i++) {
                if (votes[i] === user.vote_id) {
                    status = true
                }
            }
            if (status === true) {
                return user
            }
        })
        .map((item, i) => ({vote_id: item.vote_id, member: item.name, approve: true})) //array reduction to the actual format

    return {witness, allWitness}
}
