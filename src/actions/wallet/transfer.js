import {Aes, PrivateKey, TransactionBuilder, TransactionHelper} from 'karmajs'

import {Karma} from '../../libs/'
import {getUserData} from '../../reducers/karma/'
import {loadBalance} from '../'


export const transfer = data => async (dispatch, getState) => {
    const {id, options, pubKey} = getUserData(getState());
    const {sum, recipient, memoMessage, assetId, prvKey} = data;

    const currencies = await Karma.dbApi.exec('list_assets', ['', 10]).then(e => e);
    const recipientData = await Karma.dbApi.exec('get_account_by_name', [recipient]).then(e => e).catch((error) => {
        throw new Error(error.message)
    });

    if (!recipientData) {
        throw new Error(`User with login ${recipient} not found.`)
    }

    const prv = PrivateKey.fromWif(prvKey);
    const nonce = TransactionHelper.unique_nonce_uint64();

    let memo = null;

    const tr = new TransactionBuilder();

    const precision = currencies.filter(cur => cur.id === assetId)[0].precision;
    let operationParams = {
        fee: {amount: 0, asset_id: '1.3.0'},
        from: id,
        to: recipientData.id,
        amount: {amount: Math.floor(parseFloat(sum*10**precision+0.0001)), asset_id: assetId},
    };

    console.log( sum, operationParams.amount )
    if (memoMessage) {
        memo = {
            from: options.memo_key,
            to: recipientData.options.memo_key,
            nonce,
            message: Aes.encrypt_with_checksum(
                prv,
                recipientData.options.memo_key,
                nonce,
                memoMessage,
            ),
        };
        operationParams = { ...operationParams, memo }
    }

    await tr.add_type_operation(
        'transfer',
        operationParams,
    );

    await tr.set_required_fees();
    await tr.add_signer(prv, pubKey);

    const result = await tr.broadcast().then(e => e).catch((error) => {
        throw new Error(error.message)
    });

    if (!result[0].block_num) {
        throw new Error('An error has occurred. Verify the data and try again.')
    }

    await dispatch(loadBalance())

    return result
}
