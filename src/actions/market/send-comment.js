import {PrivateKey, TransactionBuilder} from 'karmajs'

import {getUserKeys, getUserData} from '../../reducers/karma/'
import {loadBalance} from '../'


export const sendComment = ({memo, id, uuid}) => async (dispatch, getState) => {
    const {privKey, pubKey} = getUserKeys(getState())
//   const { name } = getUserData(getState())
//   const post = await request({
//     method: 'POST',
//     uri: `${ApiURI.extraApi}/request_comments`,
//     json: {
//       sender: name,
//       receiver: id,
//       body: memo,
//     },
//   }).catch((error) => {
//     throw new Error(error.message)
//   })

    const tr = new TransactionBuilder()
    const prv = PrivateKey.fromWif(privKey)

    await tr.add_type_operation(
        'comment_credit_request_operation',
        {
            fee: {amount: 0, asset_id: '1.3.0'},
            creditor: id,
            credit_memo: memo,
            credit_request_uuid: uuid,
        },
    )

    await tr.set_required_fees()
    await tr.add_signer(prv, pubKey)

    const result = await tr.broadcast()
        .then(e => e)
        .catch((error) => {
            throw new Error(error.message)
        })

    if (!result[0].block_num) {
        throw new Error('An error has occurred. Verify the data and try again.')
    }

    await dispatch(loadBalance())

    return 'done'
}
