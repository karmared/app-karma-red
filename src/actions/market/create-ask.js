import {PrivateKey, TransactionBuilder} from 'karmajs'

import {getUserData} from '../../reducers/karma/'
import {loadBalance} from '../'
import {Karma} from "../../libs";


export const createAsk = params => async (dispatch, getState) => {
    const currencies = await Karma.dbApi.exec('list_assets', ['', 10]).then(e => e);
    const {id, pubKey} = getUserData(getState());
    const {loan, interest, month, deposit, memo, prvKey} = params;
    const tr = new TransactionBuilder();
    const prv = PrivateKey.fromWif(prvKey);

    let operationParams = {
        fee: {amount: 0, asset_id: '1.3.0'},
        borrower: id,
        loan_period: month,
        loan_persent: interest,
        loan_memo: memo,
        loan_asset: {asset_id: loan[1]},
        deposit_asset: {asset_id: deposit[1]},
    };

    const precisionL = currencies.filter(cur => cur.id === operationParams.loan_asset.asset_id)[0].precision;
    const precisionD = currencies.filter(cur => cur.id === operationParams.deposit_asset.asset_id)[0].precision;
    operationParams = {
        ...operationParams,
        loan_asset: {amount: loan[0]*(10**precisionL), asset_id: loan[1]},
        deposit_asset: {amount: deposit[0]*(10**precisionD), asset_id: deposit[1]},
    };

    await tr.add_type_operation(
        'credit_request_operation',
        operationParams,
    );
    await tr.set_required_fees();
    await tr.add_signer(prv, pubKey);

    const result = await tr.broadcast()
        .then(e => e)
        .catch((error) => {
            throw new Error(error.message)
        });

    if (!result[0].block_num) {
        throw new Error('An error has occurred. Verify the data and try again.')
    }

    await dispatch(loadBalance());

    return 'done'
};
