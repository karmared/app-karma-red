import {PrivateKey, TransactionBuilder} from 'karmajs'

import {getUserData, getUserKeys} from '../../reducers/karma/'
import {loadBalance} from '../'
import {setUser} from '../index'
import {Karma} from '../../libs'


export const redeemAsk = uuid => async (dispatch, getState) => {
    const {id} = getUserData(getState());
    const userData = getUserData(getState());
    const {privKey, pubKey} = getUserKeys(getState());
    const user = await Karma.dbApi.exec('get_accounts', [[id]]).then(e => e[0]);
    console.log(user)

    const tr = new TransactionBuilder();
    const prv = PrivateKey.fromWif(privKey);

    const params = {
        fee: {amount: 0, asset_id: '1.3.0'},
        borrower: id,
        credit_request_uuid: uuid,
    };

    await tr.add_type_operation('settle_credit_operation', params);

    await tr.set_required_fees();
    await tr.add_signer(prv, pubKey);

    const result = await tr.broadcast().then(e => e).catch((error) => {
        throw new Error(error.message)
    });

    console.log(result);

    if (!result[0].block_num) {
        throw new Error('An error has occurred. Verify the data and try again.')
    }

    await dispatch(loadBalance());
    await dispatch(setUser({...userData, karma: user.karma}));

    return 'done'
};
