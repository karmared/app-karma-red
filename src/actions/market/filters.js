export const setMarketFilters = data => async (dispatch) => {
  dispatch({ type: 'SET_FILTERS', payload: data })
};
