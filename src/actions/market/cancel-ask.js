import {PrivateKey, TransactionBuilder} from 'karmajs'

import {getUserData, getUserKeys} from '../../reducers/karma/'
import {loadBalance} from '../'


export const cancelAsk = uuid => async (dispatch, getState) => {
    const {id} = getUserData(getState());
    const {privKey, pubKey} = getUserKeys(getState());

    const tr = new TransactionBuilder();
    const prv = PrivateKey.fromWif(privKey);

    await tr.add_type_operation(
        'credit_request_cancel_operation',
        {
            fee: {amount: 0, asset_id: '1.3.0'},
            borrower: id,
            borrow_memo: '',
            credit_request_uuid: uuid,
        },
    );

    await tr.set_required_fees();
    await tr.add_signer(prv, pubKey);

    const result = await tr.broadcast().then(e => e).catch((error) => {
        throw new Error(error.message)
    });

    if (!result[0].block_num) {
        throw new Error('An error has occurred. Verify the data and try again.')
    }

    await dispatch(loadBalance());

    return 'done'
};
