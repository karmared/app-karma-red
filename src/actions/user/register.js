import request from 'request-promise-native'

import {Karma, ApiURI} from '../../libs/'
import {loadBalance, setUser, setKeys, setGateways, setCurrencies} from '../'


export const register = ({login, password, referrer}) => async (dispatch) => {
    const {pubKey, privKey, pubKeyOwner} = Karma.generateKeys(login, password);

    let ltm = false;

    if(referrer && referrer !== null) {
        ltm = await Karma.dbApi.exec('get_account_by_name', [referrer])
            .then(e => e.id === e.lifetime_referrer)
    }

    const post = await request({
        method: 'POST',
        uri: `${ApiURI.chainApi}/accounts`,
        json: {
            account: {
                name: login,
                owner_key: pubKeyOwner,
                active_key: pubKey,
                memo_key: pubKey,
                refcode: null,
                referrer: ltm ? referrer : null,
                credit_referrer: referrer
            },
        },
    });

    if (!post.account) {
        if (post.base) {
            throw new Error(post.base[0])
        }
        else if (post.error) {
            let msg = post.error.base[0];

            if (msg === 'Only cheap names allowed!') {
                msg = 'You cannot use this login. Please type with numbers and/or special characters.'
            }
            throw new Error(msg)
        }
        else {
            throw new Error(post)
        }
    }

    const user = await Karma.dbApi.exec('get_account_by_name', [login]).then(e => e).catch((error) => {
        throw new Error(error.message)
    });

    if (!user) {
        throw new Error('Cannot get user data!')
    }

    const extra = await request({
        method: 'POST',
        uri: `${ApiURI.extraApi}/users/auth`,
        json: {
            username: login,
        },
    });

    if (!extra.username) {
        throw new Error('Cannot get user extra data!')
    }

    await dispatch(setCurrencies());
    await dispatch(setKeys({privKey, pubKey}));
    await dispatch(setUser({...user, extra, pubKey}));
    await dispatch(setGateways(login));
    await dispatch(loadBalance());

    sessionStorage.setItem('login', login);
    sessionStorage.setItem('public_key', pubKey);
    sessionStorage.setItem('private_key', privKey)
};
