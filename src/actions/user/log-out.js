export const logOut = () => (dispatch) => {
  dispatch({ type: 'LOG_OUT' });

  sessionStorage.setItem('login', null);
  sessionStorage.setItem('public_key', null);
  sessionStorage.setItem('private_key', null);
};
