import React from 'react'
import request from 'request-promise-native'
import Translate from 'react-translate-component'

import {Karma, ApiURI} from '../../libs/'
import {loadBalance, setUser, setKeys, setGateways, setCurrencies} from '../'


export const logIn = ({login, password}) => async (dispatch) => {
    const user = await Karma.dbApi.exec('get_account_by_name', [login])
        .then(e => e)
        .catch((error) => {
            throw new Error(error.message)
        });

    if (!user) {
        throw new Error(<Translate content="auth.error_login_not_found" />)
    }

    // get user's pubKey from Chain to compare
    const retKey = user.active.key_auths[0][0];
    // generate current keys
    const {pubKey, privKey} = Karma.generateKeys(login, password);

    if (pubKey !== retKey) {
        throw new Error(<Translate content="auth.error_pass_wrong" />)
    }

    const extra = await request({
        method: 'POST',
        uri: `${ApiURI.extraApi}/users/auth`,
        json: {
            username: login,
        },
    });

    if (!extra.username) {
        throw new Error(<Translate content="auth.error_cannot_get_extra" />)
    }

    await dispatch(setCurrencies());
    await dispatch(setKeys({privKey, pubKey}));
    await dispatch(setUser({...user, extra, pubKey}));
    await dispatch(setGateways(login));
    await dispatch(loadBalance());

    sessionStorage.setItem('login', login);
    sessionStorage.setItem('public_key', pubKey);
    sessionStorage.setItem('private_key', privKey);
};
