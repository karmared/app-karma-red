import {getUserData} from '../../reducers/karma/'
import {ApiURI} from '../../libs/'
import request from 'request-promise-native'

export const setGateways = userId => async (dispatch, getState) => {
  const {name} = getUserData(getState())
  const login = userId || name

  let ethGW = 'Cannot get ethereum gateway account!';
  let fees = {}
  const gateway = await request({
      method: 'POST',
      uri: `${ApiURI.gateway}`,
      json: {
          jsonrpc: "2.0",
          id: "1",
          method: "get_address",
          params: [ login ]
      },
    });

  if( gateway.result ) {
      ethGW = gateway.result
  }

  const feesR = await request({
    method: 'POST',
    uri: `${ApiURI.gateway}`,
    json: {
        jsonrpc: "2.0",
        id: "1",
        method: "get_params",
        params: []
    },
  });

  if( feesR.result ) {
    fees = feesR.result[0]
  }

  dispatch({ type: 'SET_GATEWAYS', payload: {ethGW, fees} })
};
