import axios from 'axios'
import {PrivateKey, Signature} from "karmajs"
import {ApiURI} from '../../libs/'
import {Karma} from '../../libs'

export const multiFactor = ({operation, username, keys, token}) => async (dispatch) => {

    let signature = null;

    const serializedData = btoa(JSON.stringify({token: token}));

    switch(operation){
        case 'GET_STATUS':
            signature = await Signature.signBuffer(username, PrivateKey.fromWif(keys.privKey)).toHex();
            return await axios.get(`${ApiURI.multiFAApi}/user`, {
                headers: {
                    username: username,
                    signature: signature
            }
            })
            .then(e => e)
            .catch(() => false);

        case 'PUT_ENABLE':
            signature = await Signature.signBuffer(username, PrivateKey.fromWif(keys.privKey)).toHex();
            return await axios.put(`${ApiURI.multiFAApi}/user/2fa/enable`, null, {
                headers: {
                    username: username,
                    signature: signature
                }
            })
                .then(e => e)
                .catch(() => false);

        case 'PUT_ACTIVATE_TOKEN':
            signature = await Signature.signBuffer(serializedData, PrivateKey.fromWif(keys.privKey)).toHex();

            return await axios.put(`${ApiURI.multiFAApi}/user/2fa/activate?data=${serializedData}`, null, {
                headers: {
                    username: username,
                    signature: signature
                }
            })
                .then(e => e)
                .catch(() => false);

        case 'PUT_DISABLE':
            signature = await Signature.signBuffer(serializedData, PrivateKey.fromWif(keys.privKey)).toHex();

            return await axios.put(`${ApiURI.multiFAApi}/user/2fa/disable?data=${serializedData}`, null, {
                headers: {
                    username: username,
                    signature: signature
                }
            })
                .then(e => e)
                .catch(() => false);

        case 'GET_CHECK':
            signature = await Signature.signBuffer(serializedData, PrivateKey.fromWif(keys.privKey)).toHex();

            return await axios.get(`${ApiURI.multiFAApi}/user/2fa/check?data=${serializedData}`, {
                headers: {
                    username: username,
                    signature: signature
                }
            })
                .then(e => e)
                .catch(() => false);
    }
};
