import React from 'react'
import Translate from 'react-translate-component'
import { Karma } from '../../libs/'
import { getUserData } from '../../reducers/karma/'


export const toggleRequestPassword = () => async (dispatch, getState) => {
  const state = getState().authorization.requestPassword;

  dispatch({ type: 'REQUEST_PASSWORD', payload: !state })
};


export const setPrvKey = ({ password }) => async (dispatch, getState) => {
  if (!password) {
    throw new Error(<Translate content="modal_confirm.pass_please" />)
  }

  const { name, pubKey: currentPubKey } = getUserData(getState());
  const { pubKey, privKey } = Karma.generateKeys(name, password);

  if (pubKey !== currentPubKey) {
    throw new Error(<Translate content="modal_confirm.pass_incorrect" />)
  }

  dispatch({ type: 'SET_PRIVATE_KEY', payload: privKey })
};


export const clearPrvKey = () => async (dispatch) => {
  dispatch({ type: 'SET_PRIVATE_KEY', payload: '' })
};
