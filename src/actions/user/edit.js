import request from 'request-promise-native'

import {Karma, ApiURI} from '../../libs/'
import {getUserData} from '../../reducers/karma/'
import {setUser, loadBalance} from '../'
import {setKeys} from '../index'

export const userEdit = data => async (dispatch, getState) => {
    const {id, name} = getUserData(getState());

    let params = {
        first_name: data.first_name,
        last_name: data.last_name,
        email: data.email,
        facebook: data.facebook,
        phone: data.phone,
        tax_residence: data.tax_residence,
        bank_name: data.bank_name,
        bank_swift: data.bank_swift,
        bank_account: data.bank_account,
        bank_name_of_beneficiary: data.bank_name_of_beneficiary,
        about_me: data.about_me,
        company_name: data.company_name,
        company_activity: data.company_activity,
        company_vat_id: data.company_vat_id,
        company_website: data.company_website,
        company_youtube: data.company_youtube,
        company_pdf_presentation: data.company_pdf_presentation,
        passport_number: data.passport_number,
    };

    if (data.tempAvatar) {
        params = {...params, avatar: data.tempAvatar}
    }

    const extra = await request({
        method: 'PATCH',
        uri: `${ApiURI.extraApi}/users/${name}`,
        json: params,
    });

    if (!extra.username) {
        throw new Error('Cannot save user extra data!')
    }

    const user = await Karma.dbApi.exec('get_accounts', [[id]]).then(e => e[0]);

    if (!user) {
        throw new Error('Unable to update user data. Please reload the page.')
    }

    await dispatch(setUser({...user, extra, pubKey: sessionStorage.getItem('public_key')}));
    await dispatch(setKeys({privKey: sessionStorage.getItem('private_key'), pubKey: sessionStorage.getItem('public_key')}));
    await dispatch(loadBalance());

    return 'done'
};
