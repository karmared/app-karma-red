import request from 'request-promise-native'

import {Karma, ApiURI} from '../../libs/'
import {loadBalance, setUser, setKeys, setCurrencies} from '../'


export const checkUser = ({login, password}) => async () => {
    const user = await Karma.dbApi.exec('get_account_by_name', [login])
        .then(e => e)
        .catch((error) => {
            throw new Error(error.message)
        });

    if (!user) {
        throw new Error('Login not found!')
    }

    // get user's pubKey from Chain to compare
    const retKey = user.active.key_auths[0][0];
    // generate current keys
    const {pubKey, privKey} = Karma.generateKeys(login, password);

    if (pubKey !== retKey) {
        throw new Error('Password is wrong. Try better!')
    }

    const extra = await request({
        method: 'POST',
        uri: `${ApiURI.extraApi}/users/auth`,
        json: {
            username: login,
        },
    });

    if (!extra.username) {
        throw new Error('Cannot get user extra data!')
    }

    return true
};
