export const setKeys = data => (dispatch) => {
  dispatch({ type: 'SET_KEYS', payload: data })
};
