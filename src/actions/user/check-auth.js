import request from 'request-promise-native'

import {Karma, ApiURI} from '../../libs/'
import {loadBalance, setUser, setKeys, setCurrencies, setGateways} from '../'


export const checkAuth = ({login, privKey, pubKey}) => async (dispatch) => {
    const user = await Karma.dbApi.exec('get_account_by_name', [login])
        .then(e => e)
        .catch((error) => {
            throw new Error(error.message)
        });

    if (!user) {
        return dispatch({type: 'NOT_LOGGED_IN'})
    }

    // get user's pubKey from Chain to compare
    const retKey = user.active.key_auths[0][0];

    if (pubKey !== retKey) {
        return dispatch({type: 'NOT_LOGGED_IN'})
    }

    const extra = await request({
        method: 'POST',
        uri: `${ApiURI.extraApi}/users/auth`,
        json: {
            username: login,
        },
    });

    //
    // if (!extra.username) {
    //     return dispatch({type: 'NOT_LOGGED_IN'})
    // }

    await dispatch(setCurrencies());
    await dispatch(setKeys({privKey, pubKey}));
    await dispatch(setUser({...user, extra, pubKey}));
    await dispatch(setGateways(login));
    await dispatch(loadBalance());

    return true
}
