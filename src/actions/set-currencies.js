import {Karma, CoreAsset, ApiURI} from '../libs'
import request from 'request-promise-native'

export const setCurrencies = () => async (dispatch) => {
    const currencies = await Karma.dbApi.exec('list_assets', ['', 10]).then(e => Promise.all(
        e.filter( e => e.symbol.indexOf("KARMA") === -1 ).map(async item => {
            const currency = item.symbol;
            let withDrawFee = 0;
            if(currency === 'KBTC' || currency === 'KETH'){
                withDrawFee = await request({
                    method: 'POST',
                    uri: `${ApiURI.gate}/${currency.substr(1).toLowerCase()}`,
                    json: {
                        "jsonrpc": "2.0",
                        "id": "1",
                        "method": "info",
                        "params": [currency.substr(1)]
                    }
                }).then( ans => ans.result.fee)
            }

            return await {
                ...item,
                symbolPublic: currency === CoreAsset ? currency : currency.substr(1),
                crypto: (currency === CoreAsset || currency === 'KBTC' || currency === 'KETH'),
                withDrawFee
            }
        })
        ).then(e => e)
    );

    dispatch({type: 'SET_CURRENCIES', payload: currencies})
};
