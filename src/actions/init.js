import { Karma } from '../libs/'
import { checkAuth } from './'


const login = sessionStorage.getItem('login') || null
const pubKey = sessionStorage.getItem('public_key') || null
const privKey = sessionStorage.getItem('private_key') || null

export const Init = () => (dispatch) => {
  dispatch({ type: 'INIT_LOADING' })

  Karma.init()
    .then(() => {
      dispatch(checkAuth({ login, privKey, pubKey }))
    })
    .catch((error) => {
      console.error(error)
      dispatch({ type: 'INIT_ERROR' })
    })
}
