export { default as SvgDeals } from './icon-deals.svg'
export { default as SvgMarket } from './icon-market.svg'
export { default as SvgProfile } from './icon-profile.svg'
export { default as SvgWallet } from './icon-wallet.svg'
export { default as SvgExit } from './icon-exit.svg'
export { default as SvgClose } from './icon-close.svg'
export { default as SvgMenu } from './icon-menu.svg'
export { default as SvgDown } from './icon-down.svg'
export { default as SvgLeft } from './icon-left.svg'
export { default as SvgVoting } from './icon-voting.svg'
export { default as SvgHelp } from './icon-help.svg'


import { isTestnet } from '../libs/'

import LogoMainnet from './logo.svg'
import LogoTestnet from './logo-testnet.svg'

export const Logo = isTestnet ? LogoTestnet : LogoMainnet;
