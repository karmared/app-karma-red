import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import MediaQuery from 'react-responsive'
import Translate from 'react-translate-component'
import ReactGA from 'react-ga'

import {isAuthorized, isLoading, isRequestPassword} from './reducers/authorization'
import {Init, toggleRequestPassword} from './actions/'

import {isTestnet} from './libs/'
import {Spinner, Modal} from './components/helpers/'
import Auth from './components/auth'
import Account from './components/account/'
import ModalRequestPassword from './components/helpers/modal-request-password'
import getCookie from './actions/getCookie'
import counterpart from 'counterpart'

import en from './locales/en'
import counterpartRu from "counterpart/locales/ru"
import ru from './locales/ru'
import ko from './locales/ko'
import ach from './locales/ach'

counterpart.registerTranslations('en', en);
counterpart.registerTranslations('ru', counterpartRu);
counterpart.registerTranslations('ru', ru);
counterpart.registerTranslations('ko', ko);
counterpart.registerTranslations('ach', ach);


class Root extends Component {
    componentDidMount() {
        this.props.init();
        if(isTestnet){
            ReactGA.initialize('UA-115315177-1', {
                // debug: true,
                titleCase: false,
            });
        } else {
            ReactGA.initialize('UA-116284795-1', {
                // debug: true,
                titleCase: false,
            });
            ReactGA.initialize('UA-115315177-2', {
                // debug: true,
                titleCase: false,
            });
        }
        const lang = getCookie('lang');
        lang ? counterpart.setLocale(lang) : counterpart.setLocale('en');
    }

    render() {
        const {loading, authorized, showModal, toggleModal} = this.props

        if (loading) {
            return (<div className="app-loading">
                <Spinner className='spinner--absolute'/>
            </div>)
        }

        if(window.location.href.split('?')[1] === '=register'){
            return <Auth register/>
        }

        if(window.location.href.split('?r=')[1] && !authorized){
            return <Auth register referrer={window.location.href.split('?r=')[1]}/>
        }

        if (authorized) {
            return [
                <MediaQuery key="testnetMessage" minWidth={576}>
                    {!isTestnet
                        ? <div className="testnet">
                            <Translate with={{link: <a href="https://testnet-app.karma.red/" target="_blank"
                                                       rel="noopener noreferrer">https://testnet-app.karma.red/</a> }} content="link.testnet" />
                        </div>
                        : <div className="testnet">
                            <Translate with={{link: <a href="https://app.karma.red/" target="_blank"
                                                       rel="noopener noreferrer">https://app.karma.red/</a> }} content="link.mainnet" />
                        </div>
                    }

                </MediaQuery>,

                <Account key="accountPages"/>,

                (showModal &&
                    <Modal key="modalRequestPass" title={<Translate className="heading--sm" content="modal_confirm.title" />} toggleModal={toggleModal}>
                        <ModalRequestPassword/>
                    </Modal>
                ),
            ]
        }

        return <Auth/>
    }

    static propTypes = {
        init: PropTypes.func.isRequired,
        loading: PropTypes.bool.isRequired,
        authorized: PropTypes.bool.isRequired,
        showModal: PropTypes.bool.isRequired,
        toggleModal: PropTypes.func.isRequired,
    }
}

const mapStateToProps = state => ({
    loading: isLoading(state),
    authorized: isAuthorized(state),
    showModal: isRequestPassword(state),
})

const mapDispatchToProps = dispatch => ({
    init: () => dispatch(Init()),
    toggleModal: () => dispatch(toggleRequestPassword()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Root)
