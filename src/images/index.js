import { isTestnet } from '../libs/'

import LogoMainnet from '../svg/logo.svg'
import LogoTestnet from '../svg/logo-testnet.svg'

export { default as Image2fa1 } from './fa-1.jpg'
export { default as Image2fa2 } from './fa-2.jpg'

export const Logo = isTestnet ? LogoTestnet : LogoMainnet;
