import React from "react"
import Translate from 'react-translate-component'
import { Field } from "/components"
import { Checkbox } from 'semantic-ui-react'
import { Karma_input } from "/components/helpers"

const render = props => {
  const { values, onChange } = props

  return (
    <React.Fragment>
      <Translate
        content="edit_profile.title_personal"
        className="heading--md"
        component="h4"
      />

      <Field
        label="contacts.first_name"
        required
      >
        <Karma_input
          name="first_name"
          value={values.first_name}
          onChange={onChange.input}
          required
        />
      </Field>

      <Field
        label="contacts.last_name"
        required
      >
        <Karma_input
          name="last_name"
          value={values.last_name}
          onChange={onChange.input}
          required
        />
      </Field>

      <Field
        label="contacts.email"
        required
      >
        <Karma_input
          name="email"
          value={values.email}
          onChange={onChange.input}
          required
        />
      </Field>

      <Field
        label="edit_profile.multi_factor"
      >
        <Translate
          name="check2FA"
          with={{
            enabled: ""
          }}
          className="karma__toggle text--main"
          component={Checkbox}
          attributes={{
            label: "edit_profile.multi_factor"
          }}
          checked={values.check2fa}
          onChange={onChange.otp}
          toggle
        />
      </Field>
    </React.Fragment>
  )
}


export default render
