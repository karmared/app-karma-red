import React from "react"
import Translate from 'react-translate-component'


const render = props => {
  const { user, avatar } = props
  const title = `edit_profile.${ avatar ? "title_edit" : "title" }`

  return (
    <React.Fragment>
      <Translate
        component="h4"
        className='heading--md'
        with={{
          name: user.name
        }}
        content={title}
      />

      <Translate
        component="p"
        className='heading--sm'
        content="edit_profile.complete_all_fields"
      />

      <div className="avatar__upload">

      </div>
    </React.Fragment>
  )
}


export default render
