import React from "react"
import { Grid } from 'semantic-ui-react'
import Translate from 'react-translate-component'

import Avatar from "./Avatar"
import Personal from "./Personal"


class ProfileForm extends React.Component {

  render() {
    return (
      <section id='settings_page' className='content'>
        <Grid celled className='karma__grid vertical'>
          <Grid.Row className='karma__wrapper fullspace'>
            <Grid.Column
                tablet={16}
                computer={8}
                className="karma__column karma__column--scroll"
            >
              <div>
                <div className="settings__block">
                  <Avatar
                    user={this.props.user}
                  />
                </div>

                <div className="settings__block">
                  <Personal
                    values={this.props.values}
                    onChange={this.props.onChange}
                  />
                </div>

                {/* <div className="settings__block">
                  Contacts
                </div>

                <div className="settings__block">
                  About me
                </div> */}
              </div>
            </Grid.Column>

            <Grid.Column
                tablet={16}
                computer={8}
                className="karma__column karma__column--scroll"
            >
              <div>
                {/* <div className="settings__block">
                  Bank Account
                </div>

                <div className="settings__block">
                  About my company
                </div> */}
              </div>
            </Grid.Column>
          </Grid.Row>

          <div className="btn_panel">
            <Translate
              type="submit"
              content="btn.edit_save"
              className="btn btn--red"
              component="button"
            />
          </div>
        </Grid>
      </section>
    )
  }

}



export default ProfileForm
