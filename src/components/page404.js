import React from 'react'
import {Grid, Message} from 'semantic-ui-react'


const Page404 = () =>  <section id="detail_page" className='content'>
    <Grid celled className='karma__grid'>
        <Grid.Row className='karma__wrapper'>
            <Grid.Column className='karma__column'>
                <div>
                    <Message className='message--danger'>
                        <p>404 - Page not found</p>
                    </Message>
                </div>
            </Grid.Column>
        </Grid.Row>
    </Grid>
</section>;

export default Page404
