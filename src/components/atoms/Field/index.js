import React from "react"
import Translate from 'react-translate-component'


const renderLabel = ({ label, required }) => {
  if (label === null || label === void 0)
    return null

  const className = [
    "karma__label",
    required ? "required" : false
  ].filter(Boolean).join(" ")

  return (
    <Translate
      content={label}
      className={className}
    />
  )
}


const render = ({ label, required, children }) => {
  return (
    <div className="karma__field">
      {renderLabel({ label, required })}
      {children}
    </div>
  )
}


render.defaultProps = {
  label: null,
  required: false,
}


export default render
