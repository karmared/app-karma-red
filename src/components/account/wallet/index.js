import React from 'react'
import Translate from 'react-translate-component'
import ReactGA from 'react-ga'
import {Grid} from 'semantic-ui-react'

import Balance from './balance'
import Gateways from './gateways'
import History from './history'

const Wallet = () => {
    ReactGA.pageview(window.location.pathname + window.location.search);

    return(
        <section id="wallet_page" className='content'>
            <Grid celled className='karma__grid'>
                <Grid.Row className='karma__wrapper wallet__wrapper'>
                    <Grid.Column computer={7} mobile={16} className='karma__column'>
                        <div id="cur_list">
                            <Translate component="h4" className='heading--md' content="wallet.title"/>
                            <Balance/>
                        </div>
                        <div id="gateways">
                            <Translate component="h4" className='heading--md' content="modal_key.ether_gw"/>
                            <Gateways/>
                        </div>
                    </Grid.Column>
                    <Grid.Column computer={9} mobile={16} className='karma__column karma__column--scroll'>
                        <div id="history_list">
                            <Translate component="h4" className='heading--md' content="history.title"/>
                            <History/>
                        </div>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </section>
    )
}

export default Wallet
