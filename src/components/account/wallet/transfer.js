import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {Grid, Message} from 'semantic-ui-react'
import ReactGA from 'react-ga'
import Translate from 'react-translate-component'

import {transfer, toggleRequestPassword, clearPrvKey, setGateways} from '../../../actions/'
import {getUserBalance, getUserData, getGlobal, getUserGateways} from '../../../reducers/karma/'
import {getPrvKey} from '../../../reducers/authorization/'

import {SvgClose} from '../../../svg/'
import {Spinner, Karma_input} from '../../helpers/index'
import {ApiURI, CoreAsset, Karma} from '../../../libs'
import request from 'request-promise-native'
import {getCurrencies} from '../../../reducers/karma'

const checkUser = async (login) => {
    const check = await Karma.dbApi.exec('get_account_by_name', [login]).then(e => e)
    return check !== null
};

class WalletTransfer extends Component {
    state = {
        sum: '',
        recipient: '',
        memoMessage: '',
        alert: '',
        loading: false,
        success: false,
        trBlock: '',
        fee: {}
    };
    gatewayUpdateTick = null;

    componentDidMount(){
        ReactGA.pageview(window.location.pathname + window.location.search);
        this.setState({fee: this.props.global.parameters.current_fees.parameters[0][1]});
        this.props.loadGateways();
        this.gatewayUpdateTick = setInterval(() => this.props.loadGateways(), 30000);
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.prvKey) return;

        if (this.props.prvKey !== nextProps.prvKey) {
            this.submitData({prvKey: nextProps.prvKey})
        }
    }

    componentWillUnmount() {
        this.props.clearKey()
        clearInterval(this.gatewayUpdateTick);
    }

    handleSum = (val) => {
        const {currency, balance} = this.props;
        const data = balance.filter(item => item.symbolPublic === currency)[0];
        const max = parseFloat(data.amount).toFixed(data.crypto ? 5 : 2);
        const sum = parseFloat(val) > max ? max : val;
        this.setState({ sum })
    };

    handleInput = (e) => {
        if (e.target.name) {
            this.setState({[e.target.name]: e.target.value});
        }
    };

    submitData = ({prvKey}) => {
        const {transferAction, balance, currency} = this.props;
        const {sum, recipient, memoMessage} = this.state;
        const assetId = balance.filter(item => item.symbolPublic === currency)[0].id;
        transferAction({sum, recipient, memoMessage, assetId, prvKey})
            .then(e => {
                this.setState({loading: false, alert: '', success: true, trBlock: e[0].block_num});
            })
            .catch((error) => {
                this.setState({loading: false, alert: `${error.message}`.split('bitshares-crypto')[0]});
            })
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const {sum, recipient, fee} = this.state;
        const {toggleModal, balance, currency, currencies, user, prvKey} = this.props;
        const allowedAmount = balance.filter(item => item.symbolPublic === currency)[0].amount;
        const allowCoreAsset = balance.filter(item => item.symbolPublic === CoreAsset)[0].amount;

        const feeSize = fee.fee/10**currencies.filter(item => item.symbolPublic === CoreAsset)[0].precision;

        if (allowCoreAsset <= feeSize) {
            return this.setState({alert: <Translate component="p" content='transfer.message_insufficient_funds' fee={`${feeSize} ${CoreAsset}`} />})
        }

        if (parseFloat(sum) <= 0 || !recipient) {
            return this.setState({alert: <Translate component="p" content='transfer.message_fill_up' />})
        }

        if (parseFloat(sum) > allowedAmount) {
            return this.setState({alert: <Translate component="p" content='transfer.message_sum' sum={`${parseFloat(sum)} ${currency}`} />})
        }

        if (recipient === user.name) {
            return this.setState({alert: <Translate component="p" content='transfer.message_yourself' />})
        }

        this.setState({alert: null, success: false, loading: true});

        checkUser(recipient).then( e => {
            if (e){
                if (prvKey) {
                        return this.submitData({prvKey})
                    }
                return toggleModal()
            } else {
                return this.setState({alert: <Translate component="p" content='transfer.error_login_not_found' />, loading: false})
            }
        })

    };


    navigateBack = () => {
        window.history.back();
    };

    render() {
        const {sum, fee, recipient, memoMessage, loading, alert, success, trBlock} = this.state;

        const {currency, balance, global, gateways} = this.props;

        if(!global.parameters){
            return <Redirect to="/wallet"/>
        }

        if (balance.length === 0) {
            return (
                <section id="currency_page" className='content'>
                    <Grid celled className='karma__grid'>
                        <Grid.Row className='karma__wrapper currency__wrapper'>
                            <Grid.Column className='currency__column karma__column'>
                                <Spinner className='spinner--absolute'/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </section>
            )
        }

        const data = balance.filter(item => item.symbolPublic === currency)[0];
        const feePerTransfer = fee.fee/100000;
        const feePerMemo = (fee.price_per_kbyte/1024*memoMessage.length)/100000;
        const max = currency === CoreAsset
            ? parseFloat(data.amount - feePerMemo - feePerTransfer).toFixed(data.crypto ? 5 : 2)
            : parseFloat(data.amount).toFixed(data.crypto ? 5 : 2);

        return (

            <section id="currency_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper currency__wrapper'>
                        <Grid.Column computer={7} tablet={16}
                                     className='currency__column karma__column--scroll karma__column'>
                            <div>
                                <Translate component="h4" className='heading--md' content='transfer.title' with={{
                                    currency: <span className='cur'> {currency}</span>,
                                    close_btn: <button className='btn--link btn--red_text' onClick={this.navigateBack}>
                                        <SvgClose width={25} height={25} className="fill-red"/>
                                    </button>
                                }} />

                                { alert && <Message warning className='message--warning'>{alert}</Message> }

                                { success &&
                                <Message className='message--success'>
                                    <Translate component="p" content='transfer.successfully_transfered' />
                                    <Translate component="a" content='btn.link'
                                               target="_blank" href={`${ApiURI.explorer}operations/blocks/${trBlock}`} />
                                </Message>
                                }

                                <div className="props">
                                    <Translate className="props__label" content='transfer.currency' currency={currency}/>
                                    <span className="props__value">{data.amount} {data.symbolPublic}</span>
                                </div>

                                <form onSubmit={this.handleSubmit}>
                                    <Karma_input type="number" required min="0" max={max} step={data.crypto ? "0.00001" : "0.01"}
                                                 label={<Translate component="label" className="karma__label" content='transfer.amount' />}
                                                 btn={<Translate component="button" className="karma__inside-btn btn--link" type="button"
                                                                 onClick={() => this.handleSum(max)} content='btn.maximum' />}
                                                 value={ sum }
                                                 placeholder='0'
                                                 onChange={e => this.handleSum(e.target.value)}
                                                 validate="sum"/>
                                    {
                                        recipient === gateways.fees.account &&
                                        <strong><Translate component="p" className="statistic__total karma__label" content='modal_key.min_amount' with={{
                                            min: `${gateways.fees.minKRM / 100000} KRM`
                                        }}/></strong>
                                    }
                                    <Karma_input required name="recipient" value={recipient} onChange={this.handleInput}
                                                 label={<Translate component="label" className="karma__label" content='transfer.recipient' />}/>
                                    <div className="karma__field">
                                        <Translate component="label" className="karma__label" content='transfer.memo' />
                                        <textarea rows="4" name="memoMessage" className="karma__input" value={memoMessage}
                                            onChange={this.handleInput}/>
                                    </div>

                                    <Translate component="p" className="statistic__total karma__label" content='transfer.fee' with={{
                                        sum: `${sum > 0 ? ' ' + feePerTransfer : ' 0'} KRM`,
                                        memo: memoMessage.length > 0 ? `+ ${feePerMemo} KRM` : '',
                                        gateway: recipient === `${gateways.fees.account}` ? `+ ${gateways.fees.feeFromKRM / 100000} KRM` : ''
                                    }}/>

                                    <button disabled={loading} type="submit"
                                            className={`btn btn--red ${loading ? 'loading' : ''}`}>

                                        {loading
                                            ? <Spinner className='spinner--inside'/>
                                            : <Translate content='btn.transfer' />
                                        }
                                    </button>
                                </form>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )
    }

    static propTypes = {
        currency: PropTypes.string.isRequired,
        balance: PropTypes.arrayOf(PropTypes.any).isRequired,
        gateways: PropTypes.objectOf(PropTypes.any).isRequired,
        user: PropTypes.objectOf(PropTypes.any).isRequired,
        global: PropTypes.objectOf(PropTypes.any).isRequired,
        transferAction: PropTypes.func.isRequired,
        toggleModal: PropTypes.func.isRequired,
        clearKey: PropTypes.func.isRequired,
        prvKey: PropTypes.string,
    };

    static defaultProps = {
        prvKey: '',
    }
}

const mapStateToProps = state => ({
    currency: state.routing.location.pathname.split('/').pop().toUpperCase(),
    currencies: getCurrencies(state),
    balance: getUserBalance(state),
    global: getGlobal(state),
    user: getUserData(state),
    prvKey: getPrvKey(state),
    gateways: getUserGateways(state),
});

const mapDispatchToProps = dispatch => ({
    transferAction: data => dispatch(transfer(data)),
    toggleModal: () => dispatch(toggleRequestPassword()),
    clearKey: () => dispatch(clearPrvKey()),
    loadGateways: () => dispatch(setGateways()),
});

export default connect(mapStateToProps, mapDispatchToProps)(WalletTransfer)
