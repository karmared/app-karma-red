import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import {getUserGateways} from '../../../reducers/karma/index'
//import {Karma} from "../../../libs/karma";
//import {CoreAsset} from '../../../libs'
import Translate from 'react-translate-component'

const CopyToCliboard = (e) => {
  var textField = document.createElement('textarea')
  textField.innerText = e
  document.body.appendChild(textField)
  textField.select()
  document.execCommand('copy')
  textField.remove()
  var elem = document.getElementById('ethGWCopyButton')
  elem.className = 'fa fa-check'
  setTimeout( () => { elem.className = 'fa fa-clipboard' }, 5000 )
}

const Gateways = ({gateways}) => {
    if( gateways.ethGW === undefined || gateways.fees === undefined ) {
      return '';
    }
    
    return (
      <div>
        <p key='etherGW' className='statistic__label heading--sm'>{gateways.ethGW} <button title='Copy to clipboard' style={{margin:"0px 13px"}} onClick={()=> CopyToCliboard(gateways.ethGW)}><i id='ethGWCopyButton' className="fa fa-clipboard" style={{color:'black',margin:"4px 0px"}}></i></button></p>
        <p>
          <Translate key='minKRM' className='statistic__label heading--sm' content="modal_key.min_amount" min={gateways.fees.minKRM / 100000} currency="KRM" />
          <Translate key='fee' className='statistic__label heading--sm' content="withdraw.fee" fee={gateways.fees.feeToKRM / 100000} currency="KRM" />
        </p>
      </div>
    )
};

Gateways.propTypes = {
    gateways: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
    gateways: getUserGateways(state),
});

export default connect(mapStateToProps)(Gateways)
