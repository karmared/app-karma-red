import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import _ from 'lodash'

import { WalletItem } from './wallet-item'

import {getCurrencies, getUserBalance} from '../../../reducers/karma/index'
import {Karma} from "../../../libs/karma";
import {CoreAsset} from '../../../libs'

const Balance = ({currencies, balance}) => {
    const arr = [];
    currencies
        .filter(e => e.symbol.indexOf("KARMA") === -1)
        .map((item) => {
        const symbol = item.symbol !== CoreAsset ? item.symbol.substr(1) : item.symbol,
            getAmount = balance.filter(e => e.id === item.id)[0],
            amount = getAmount ? getAmount.amount : 0;

        return arr.push({symbol, amount, crypto: item.crypto})
    });

    const wallet = _.orderBy(arr, 'amount', 'desc');
    let walletList = wallet.map((item) => {
        return <WalletItem key={item.symbol} data={item}/>;
    });

    return (
        <ul className="currency__block">
            { walletList }
        </ul>
    )
};

Balance.propTypes = {
    currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
    balance: PropTypes.arrayOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    balance: getUserBalance(state),
});

export default connect(mapStateToProps)(Balance)
