import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import _ from 'lodash'
import {ChainTypes} from 'karmajs'
import Translate from 'react-translate-component'
import {Table} from 'semantic-ui-react';
import MediaQuery from 'react-responsive'

import {clearPrvKey} from '../../../actions/'
import {getUserData} from '../../../reducers/karma'
import {Spinner} from '../../helpers/'
import {Karma} from '../../../libs/karma'
import {SvgDown} from '../../../svg/'

import History_item from './history-item'


const getHistory = async ({history, historyLast, userData}) => {
    const queryParams = [
        0, // from_index
        100, // elements_count
        0, // loan_persent_from
        10000000, // loan_persent_to
        0, // deposit_persent_from
        10000000, // deposit_persent_to
        0, // loan_volume_from
        1000000000, // loan_volume_to
        '', // cyrrency_symbol
        userData.name, // user login
        6, // status
    ];
    const countToDisplay = 25;
    const historyParams = [
        userData.id, // user ID
        '1.11.0', // operation index
        countToDisplay, // count items
        historyLast // last operation index
    ];
    const result = await Karma.historyApi.exec('get_account_history', historyParams)
        .then(e =>  Promise.all(
            e.map( async item => {
                let operation = '';

                for ( let i in ChainTypes.operations) {
                    if(item.op[0] === ChainTypes.operations[i]){
                        operation = i;
                    }
                }


                return await Karma.dbApi.exec('get_block', [item.block_num]).then(e => ({
                    ...item,
                    type: operation,
                    time: e.timestamp
                }))
            })
            ).then( e => e )
        );


    const deals = await Karma.dbApi.exec('fetch_credit_requests_stack', queryParams).then(e => e);
    const cred = await Karma.dbApi.exec('fetch_credit_requests_stack_by_creditor', queryParams).then(e => e);
    let dealsHistory = [];
    deals.concat(cred).map((item, index) => {
        const itemHistory = JSON.parse(item.history_json);
        for( let item in itemHistory ){
            dealsHistory.push({time: item, ...itemHistory[item]});
        }
    })

    if (!result) {
        return {
            history: [],
            loading: false,
        }
    }

    const lastId = result[result.length - 1].id;
    const data = [
        ...history,
        ...result.concat(dealsHistory).sort((prev, next) => {
            return (new Date(prev['time']) > new Date(next['time'])) ? -1 : (new Date(next['time']) > new Date(prev['time'])) ? 1 : 0
        })
    ];

    return {
        loading: false,
        showMore: result.length === countToDisplay,
        history: _.uniqBy(data, 'id'),
        historyLast: lastId,
        resultLength: result.length
    }
};

class History extends Component {
    state = {
        history: [],
        historyLast: '1.11.0',
        showMore: true,
        loading: false,
        resultLength: 1
    };

    componentDidMount() {
        this.loadMore()
    }

    componentWillUnmount() {
        this.props.clearKey()
    };

    loadMore = () => {
        const {history, historyLast} = this.state;
        const {userData} = this.props;

        this.setState({loading: true});
        getHistory({history, historyLast, userData}).then(e => this.setState(e))
    };

    render() {
        const {history, showMore, loading, resultLength} = this.state;

        if (history.length === 0) {
            return <Spinner/>
        }

        return (
            <MediaQuery minWidth={810}>
                {(matches) => {
                    if (matches) {
                        return (
                            <Table basic='very' className='karma__table history__table'>
                                <Table.Header className='karma__table-header'>
                                    <Table.Row>
                                        <Table.HeaderCell width={4} className='karma__cell karma__cell--header'>
                                            <Translate content='history.date' />
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={2} className='karma__cell karma__cell--header history__cell--block'>
                                            <Translate content='history.block' />
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={10} className='karma__cell karma__cell--header history__cell--desc'>
                                            <Translate content='history.description' />
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {history.map((data, index) => <History_item key={`${data.block_num}_${index}`} data={data}/>)}
                                </Table.Body>
                                {(showMore && resultLength > 0) &&
                                <Table.Footer>
                                    <Table.Row>
                                        <Table.Cell colSpan={3}>
                                            {loading
                                                ? <Spinner/>
                                                : <button type="button" onClick={this.loadMore} className="btn btn--more">
                                                    <Translate content='btn.show_more' />
                                                    <SvgDown width={14} height={6} className='ico'/>
                                                </button>
                                            }
                                        </Table.Cell>
                                    </Table.Row>
                                </Table.Footer>
                                }
                            </Table>
                        )
                    }

                    return (
                        <div className="list--history">
                            {history.map((data, index) => <History_item key={`${data.block_num}_${index}`} data={data}/>)}
                        </div>
                    )
                }}
            </MediaQuery>


        )
    }

    static propTypes = {
        userData: PropTypes.string.isRequired,
        clearKey: PropTypes.func.isRequired,
    }
}

const mapStateToProps = state => ({
    userData: getUserData(state),
});

const mapDispatchToProps = dispatch => ({
    clearKey: () => dispatch(clearPrvKey()),
});

export default connect(mapStateToProps, mapDispatchToProps)(History)
