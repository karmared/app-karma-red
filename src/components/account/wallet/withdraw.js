import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Grid} from 'semantic-ui-react'
import ReactGA from 'react-ga'
import {Redirect} from 'react-router-dom'
import Translate from 'react-translate-component'

import {getUserBalance} from '../../../reducers/karma/index'

import {SvgClose} from '../../../svg/'
import {Spinner, Karma_input} from '../../helpers/index'
import {CoreAsset} from '../../../libs'
import {clearPrvKey, toggleRequestPassword, transfer} from '../../../actions'
import {getPrvKey} from '../../../reducers/authorization'
import {getCurrencies} from '../../../reducers/karma'


class Withdraw extends Component {
    state = {
        sum: '',
        fee: 0,
        cryptoAddress: '',
        bankBeneficiary: '',
        bankAccount: '',
        bankIBAN: '',
        bankAccountNumber: '',
        redirect: false,
        loading: false
    };

    componentDidMount(){
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.prvKey) return;

        if (this.props.prvKey !== nextProps.prvKey) {
            this.submitData({prvKey: nextProps.prvKey})
        }
    }

    handleSum = (val) => {
        const {currency, balance, currencies} = this.props;
        const data = {
            ...balance.filter(item => item.symbolPublic === currency)[0],
            ...currencies.filter(item => item.symbolPublic === currency)[0]
        };
        const max = parseFloat(data.amount).toFixed(data.crypto ? 5 : 2);
        const sum = parseFloat(val) > max ? max : val;
        let fee = 0;
        if (data.crypto) {
            fee = currency === CoreAsset
                ? 0.01
                : data.symbolPublic !== 'BTC' && data.symbolPublic !== 'ETH'
                    ? fee
                    : data.withDrawFee
        }
        else {
            const int = 0.1;
            fee = (sum * int).toFixed(2)
        }

        this.setState({
            sum,
            fee,
        })
    };

    handleInput = (e) => {
        if (e.target.name) {
            this.setState({[e.target.name]: e.target.value})
        }
    };

    submitData = ({prvKey}) => {
        const { sum, cryptoAddress } = this.state
        const {currency, balance, transferAction} = this.props;
        const assetId = balance.filter(item => item.symbolPublic === currency)[0].id;
        transferAction({
            sum,
            recipient: 'gateway',
            memoMessage: cryptoAddress,
            assetId,
            prvKey
        })
            .then(() => this.setState({redirect: true, loading: false}))
            .catch((error) => console.log(error.message))
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.setState({loading: true})
        const {prvKey, toggleModal} = this.props;
        if (prvKey) {
            return this.submitData({prvKey})
        }

        return toggleModal()
    }

    navigateBack = () => {
        window.history.back();
    };

    render() {
        const {
            sum, fee, redirect, loading, cryptoAddress, bankBeneficiary, bankAccount, bankIBAN, bankAccountNumber,
        } = this.state;

        const {currency, balance, currencies} = this.props;

        if (redirect){
            return <Redirect to='/wallet'/>
        }

        if (balance.length === 0) {
            return (
                <section id="currency_page" className='content'>
                    <Grid celled className='karma__grid'>
                        <Grid.Row className='karma__wrapper currency__wrapper'>
                            <Grid.Column className='currency__column karma__column'>
                                <Spinner className='spinner--absolute'/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </section>
            )
        }
        const data = balance.filter(item => item.symbolPublic === currency)[0];

        const max = parseFloat(data.amount).toFixed(data.crypto ? 5 : 2);
        const total = (parseFloat(sum) - parseFloat(fee)).toFixed(data.crypto ? 5 : 2);

        return (

            <section id="currency_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper currency__wrapper'>
                        <Grid.Column computer={7} tablet={16} className='currency__column karma__column karma__column--scroll'>
                            <form onSubmit={this.handleSubmit} className='karma__form'>
                                <Translate component="h4" className='heading--md' content='withdraw.title' with={{
                                    currency: <span className='cur'> {currency}</span>,
                                    close_btn: <button className='btn--link btn--red_text' onClick={this.navigateBack}>
                                        <SvgClose width={25} height={25} className="fill-red"/>
                                    </button>
                                }} />

                                <div className="props">
                                    <Translate className="props__label" content='withdraw.currency' currency={currency}/>
                                    <span className="props__value">{data.amount} {data.symbolPublic}</span>
                                </div>
                                {currency === 'ETH' || currency === 'BTC' || currency === CoreAsset
                                    ? [
                                        <Karma_input
                                            key='quantity'
                                            className='settings__input'
                                            type="number"
                                            label={<Translate component="label" className="karma__label" content='withdraw.quantity_to_receive' />}
                                            btn={<Translate component="button" className="karma__inside-btn btn--link" type="button" onClick={() => this.handleSum(max)} content='btn.maximum' />}
                                            required
                                            min="0"
                                            max={max}
                                            step='0.001'
                                            placeholder='0'
                                            value={ sum }
                                            onChange={e => this.handleSum(e.target.value)} validate="sum"/>,

                                        <Karma_input
                                            key="cryptoAddress"
                                            className='settings__input'
                                            label={<Translate component="label" className="karma__label" content='withdraw.wallet_address' />}
                                            required name="cryptoAddress"
                                            value={cryptoAddress}
                                            onChange={this.handleInput}/>
                                    ]

                                    : [
                                        <Karma_input
                                            key="bankBeneficiary"
                                            className='settings__input'
                                            label={<Translate component="label" className="karma__label" content='withdraw.bank_beneficiary' />}
                                            required name="bankBeneficiary"
                                            value={bankBeneficiary}
                                            onChange={this.handleInput}/>,

                                        <div key="bankAccount" className="karma__inputs-group">
                                            <Karma_input
                                                className='settings__input'
                                                label={<Translate component="label" className="karma__label" content='withdraw.account' />}
                                                required name="bankAccount"
                                                value={bankAccount}
                                                onChange={this.handleInput}/>
                                            <Karma_input
                                                className='settings__input'
                                                label={<Translate component="label" className="karma__label" content='withdraw.IBAN' />}
                                                required name="bankIBAN"
                                                value={bankIBAN}
                                                onChange={this.handleInput}/>
                                        </div>,

                                        <Karma_input
                                            className='settings__input'
                                            key="bankAccountNumber"
                                            label={<Translate component="label" className="karma__label" content='withdraw.account_number' />}
                                            required name="bankAccountNumber"
                                            placeholder='0'
                                            value={bankAccountNumber}
                                            onChange={this.handleInput}
                                            validate="digits"/>,

                                        <Karma_input
                                            className='settings__input'
                                            type="number"
                                            key="quantity"
                                            label={<Translate component="label" className="karma__label" content='withdraw.quantity_to_receive' />}
                                            btn={<Translate component="button" className="karma__inside-btn btn--link" type="button" onClick={() => this.handleMax(max)} content='btn.maximum' />}
                                            required min="0"
                                            placeholder='0'
                                            max={max}
                                            value={sum}
                                            onChange={e => this.handleSum(e.target.value)}
                                            validate="sum"/>
                                    ]
                                }

                                <div className="statistic__total">
                                    <Translate component="p" className='statistic__label heading--sm' content='withdraw.total' />
                                    <p className="statistic__value"> {total > 0 ? total : 0} <span className='cur'> {currency}</span> </p>
                                    <Translate component="p" content='withdraw.fee' with={{ fee, currency }} />
                                </div>
                                {loading
                                    ? <span className='btn btn--red btn--disabled'><Spinner className='spinner--inside'/></span>
                                    : <Translate component="button" className='btn btn--red' content='btn.submit' />
                                }


                                <Translate component="p" className='currency__hint' content='withdraw.currency_hint' />
                            </form>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )
    }

    static propTypes = {
        sum: PropTypes.number,
        currency: PropTypes.string.isRequired,
        balance: PropTypes.arrayOf(PropTypes.any).isRequired,
        transferAction: PropTypes.func.isRequired,
        toggleModal: PropTypes.func.isRequired,
        prvKey: PropTypes.string,
    }
}

const mapStateToProps = state => ({
    currency: state.routing.location.pathname.split('/').pop().toUpperCase(),
    currencies: getCurrencies(state),
    balance: getUserBalance(state),
    prvKey: getPrvKey(state),
});
const mapDispatchToProps = dispatch => ({
    transferAction: data => dispatch(transfer(data)),
    toggleModal: () => dispatch(toggleRequestPassword()),
    clearKey: () => dispatch(clearPrvKey()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Withdraw)
