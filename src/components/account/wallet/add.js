import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {CopyToClipboard} from 'react-copy-to-clipboard'
import ReactGA from 'react-ga'
import axios from "axios"

import Translate from 'react-translate-component'
import {Grid} from 'semantic-ui-react'

import {SvgClose} from '../../../svg/'
import {getUserData, getCurrencies} from '../../../reducers/karma'
import {ApiURI, CoreAsset} from '../../../libs'


class Add extends React.Component {
    state={
        address: '',
        showAddress: false,
        copied: false,
        exist: false
    };

    componentDidMount(){
        ReactGA.pageview(window.location.pathname + window.location.search);
        const {match, user} = this.props;
        const currency = match.params.currency;
        axios.post(ApiURI.gate+currency, {
            "jsonrpc": "2.0",
            "id": "1",
            "method": "is_exist",
            "params": [currency.toUpperCase(), user.name]
        }).then(e => {
            if (e.data.result.exists){
                this.getAddress()
            }
        })
    }

    navigateBack = () => {
        window.history.back();
    };

    handleAddress = () => {
        const {match, currencies} = this.props;
        const currency = match.params.currency.toUpperCase();
        const check = currencies.filter(e => e.symbol.substr(1) === currency && e.crypto).length > 0;
        if (!check){
            return this.setState({ showAddress: true })
        }
        return this.getAddress()
    };

    getAddress = () => {
        const {match, user} = this.props;
        const currency = match.params.currency;
        axios.post(ApiURI.gate+currency, {
            "jsonrpc": "2.0",
            "id": "1",
            "method": "generate_address",
            "params": [currency.toUpperCase(), user.name]
        }).then(e => {
            if(e.data.result.address){
                return this.setState({showAddress: true, address: e.data.result.address})
            }
        })
    }

    render() {
        const {match, user} = this.props;
        const {showAddress, address, copied, exist} = this.state;
        const currency = match.params.currency.toUpperCase();

        let info = <Translate component="p" className='currency__hint text--main' content='add.info'/>;

        if (currency === CoreAsset) {
            info = <Translate component="p" className='currency__hint text--main' content='add.info_KRM' with={{ username: <b>{user.name}</b> }}/>;
        }

        return (
            <section id="currency_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper currency__wrapper'>
                        <Grid.Column largeScreen={6} computer={7} tablet={16}
                                     className='currency__column karma__column--scroll karma__column'>
                            <div>
                                <Translate component="h4" className='heading--md' content='add.title' with={{
                                    currency: <span className='cur'> {currency}</span>,
                                    close_btn: <button className='btn--link btn--red_text' onClick={this.navigateBack}>
                                        <SvgClose width={25} height={25} className="fill-red"/>
                                    </button>
                                }} />

                                <div className="info--contacts">
                                    {info}
                                    {currency !== CoreAsset && (
                                        showAddress || exist
                                        ? [
                                            <div className="props">
                                                <Translate className="props__label" content='add.wallet_to_transfer' />
                                                <span className="props__value" ref='address'>{address}</span>
                                            </div>,
                                            copied ? <Translate component="p" className='text--main text--red' content='add.address_copied' /> : '',
                                            <CopyToClipboard text={address} onCopy={() => this.setState({copied: true})}>
                                                <button className="btn btn--red"><Translate content='btn.copy_address' /></button>
                                            </CopyToClipboard>
                                        ]
                                        : <Translate component='button' content='btn.get_address' className="btn btn--red" onClick={this.handleAddress}/>
                                    )}

                                    {currency === CoreAsset &&
                                        [
                                            copied ? <Translate key='copied' component="p" className='text--main text--red' content='add.address_copied' /> : '',
                                            <CopyToClipboard key='btn' text={user.name} onCopy={() => this.setState({copied: true})}>
                                                <Translate component='button' content='btn.copy_login' className="btn btn--red" />
                                            </CopyToClipboard>
                                        ]
                                    }

                                </div>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )
    }
}

Add.propTypes = {
    match: PropTypes.objectOf(PropTypes.any).isRequired,
    user: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    user: getUserData(state),
});

export default connect(mapStateToProps)(Add)
