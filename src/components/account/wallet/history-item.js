import React, {Component} from 'react'
import {Aes, PrivateKey, ChainTypes} from 'karmajs'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import MediaQuery from 'react-responsive'
import {Link} from 'react-router-dom'
import {Table} from 'semantic-ui-react'
import Translate from 'react-translate-component'

import {getCurrencies, getUserData} from '../../../reducers/karma/'
import {getPrvKey} from '../../../reducers/authorization/'
import {Karma, KarmaMultiplier, CoreAsset} from '../../../libs/'
import {Spinner, nFormatter, rFormatter} from '../../helpers/'
import {toggleRequestPassword} from '../../../actions/'
import {ApiURI} from '../../../libs'

const getBlockDetails = async (data, currencies, user) => {
    const result = await Karma.dbApi.exec('get_block', [data.block_num]).then(e => e);
    const details = data.block_num ? data.op.slice(-1)[0] : '';


    let description = '',
        asset, symbol, amount, precision, fee = 0,
        fromLink = <b>{user.name}</b>,
        toLink = <b>{user.name}</b>;

    switch(data.type){
        case 'transfer':
            asset = currencies.filter(e => e.id === details.amount.asset_id)[0];
            symbol = ( asset.symbol !== CoreAsset ) ? asset.symbol.substr(1) : asset.symbol;
            precision = 10**asset.precision;

            amount = details.amount.amount / precision;

            if (details.from !== user.id) {
                const from = await Karma.dbApi.exec('get_accounts', [[details.from]]).then(e => e[0]);
                fromLink = <Link to={`/profile/${from.name}`} className="text--accient">{from.name}</Link>
            }

            if (details.to !== user.id) {
                const to = await Karma.dbApi.exec('get_accounts', [[details.to]]).then(e => e[0]);
                fee = data.block_num ? data.op[1].fee.amount / 10**5 : 0

                toLink = <Link to={`/profile/${to.name}`} className="text--accient">{to.name}</Link>
            }

            description = <Translate
                component='p' content='operation.transfer'
                with={{
                    amount: <b className="text--red">{rFormatter(amount)}&nbsp;{symbol}</b>,
                    fromLink,
                    toLink
                }}
            />;
            break;

        case 'account_create':
            description = <Translate component='p' content='operation.account_create' />;
            break;

        case 'account_update':
            description = <Translate component='p' content='operation.account_update' />;
            break;

        case 'credit_request_operation':
            asset = currencies.filter(e => e.id === details.loan_asset.asset_id)[0];
            symbol = (asset.symbol !== CoreAsset) ? asset.symbol.substr(1) : asset.symbol;
            precision = 10**asset.precision;
            amount = details.loan_asset.amount / precision;
            fee = data.block_num ? data.op[1].fee.amount / 10**5 : 0

            description = <Translate
                component='p'
                content='operation.credit_request_operation'
                with={{
                    amount: <b className="text--red">{rFormatter(amount)}&nbsp;{symbol}</b>,
                    percent: <b>{details.loan_persent}%</b>,
                    period: <b>{details.loan_period}&nbsp;<Translate content='month' /></b>
                }}
            />;
            break;

        case 'credit_approve_operation':
            asset = currencies.filter(e => e.id === data.op[1].fee.asset_id)[0];
            precision = 10**asset.precision;
            fee = data.block_num ? data.op[1].fee.amount / precision : 0
            description = <Translate component='p' with={{
                    idLink: <Link to={`/deals/${details.credit_request_uuid}`} className="text--accient"><Translate content='credit_request' /></Link>
                }} content='operation.credit_approve_operation' />;
            break;

        case 'credit_request_cancel_operation':
            description = <Translate component='p' content='operation.credit_request_cancel_operation' />;
            break;

        case 'comment_credit_request_operation':
            asset = currencies.filter(e => e.id === data.op[1].fee.asset_id)[0];
            precision = 10**asset.precision;
            fee = data.block_num ? data.op[1].fee.amount / precision : 0
            description = [
                <Translate key='info' component='p' content='operation.comment_credit_request_operation'
                           uuid={<Link to={`/deals/${details.credit_request_uuid}`} className='text--success'>{details.credit_request_uuid}</Link>} />,
                <p key='mess'><Translate component='b'  content='btn.comment' />: {details.credit_memo}</p>
            ];
            break;

        case 'settle_credit_operation':
            description = <Translate component='p' with={{
                idLink: <Link to={`/deals/${details.credit_request_uuid}`} className="text--accient"><Translate content='credit' /></Link>
            }} content='operation.settle_credit_operation' />;
            break;

        case 'account_upgrade':
            asset = currencies.filter(e => e.id === data.op[1].fee.asset_id)[0];
            precision = 10**asset.precision;
            fee = data.block_num ? data.op[1].fee.amount / precision : 0
            description = <Translate component='p' content='operation.account_upgrade' />;
            break;


        /*Deals operation*/

        case 'request_creation':
            description = <Translate
                component='p'
                content="operation.request_creation"
                className="detail__item-desc"
                with={{
                    borrower: <Link to={`/profile/${data.borrower}`} className="text--accient">{data.borrower}</Link>,
                    id_credit: <Link to={`/deals/${data.id}`} className='text--success'>{data.id}</Link>
                }} />;
            break;

        case 'request_approved':
            description = <Translate
                component='p'
                className="detail__item-desc"
                content="operation.request_approved"
                creditor={<Link to={`/profile/${data.creditor}`} className="text--accient">{data.creditor}</Link>}
                amount={<span className="text--red">{data.loan_amount}</span>}
                months={<span className="text--red">{data.loan_period_in_month}</span>}
                payment={<span className="text--red">{data.monthly_payment}</span>}
                uuid={<Link to={`/deals/${data.id}`} className='text--success'>{data.id}</Link>} />;
            break;

        case 'credit_complete_forced':
            description = <Translate
                component='p'
                className="detail__item-desc"
                content="operation.credit_complete_forced"
                uuid={<span className='text--success'>{data.id}</span>} />
            break;

        case 'monthly_settlement':
            description = <Translate
                component='p'
                className="detail__item-desc"
                content="operation.monthly_settlement"
                month_elapsed={<span>{`#${data.month_elapsed}`}</span>}
                payment={<span className="text--red">{`${data.sum_settled} ${data.asset_settled}`}</span>}
                balance={<span className="text--success">{`${data.deposit_left_sum} ${data.deposit_asset}`}</span>}
                uuid={<span className='text--success'>{data.id}</span>} />;
            break;

        case 'credit_complete':
            description = <Translate
                component='p'
                className="detail__item-desc"
                content="operation.credit_complete"
                uuid={<span className='text--success'>{data.id}</span>} />
            break;

        case 'stop-loss_order':
            description = <Translate
                component='p'
                className="detail__item-desc"
                content="operation.stop-loss_order"
                uuid={<span className='text--success'>{data.id}</span>} />
            break;

        default:
            description = data.type;
            break;
    }

    return {
        block: result,
        description,
        fee
    }
}


class History_item extends Component {
    state = {
        block: null,
        description: '',
        fee: 0
    }

    componentDidMount() {
        const {data, currencies, user, prvKey} = this.props

        getBlockDetails(data, currencies, user, prvKey).then(e => this.setState(e))
    }

    exDescription = ({type, details, user, prvKey, toggleModal}) => {
        if (!(type === 0 && details.memo)) {
            return ''
        }

        let result = '';

        if (prvKey) {
            const message = Aes.decrypt_with_checksum(
                PrivateKey.fromWif(prvKey),
                (details.from === user.id) ? details.memo.to : details.memo.from,
                details.memo.nonce,
                details.memo.message,
            ).toString();

            result = <span className="memo"><b>Memo:</b> {message}</span>
        }
        else {
            result = <Translate component="button" content='btn.show_memo' type="button" className="btn btn--link text--secondary" onClick={() => toggleModal()} />
        }

        return result
    };

    render() {
        const {data, user, prvKey, toggleModal} = this.props;
        const {block, description, fee} = this.state;

        if (!data) {
            return (<tr>
                <td colSpan="3">
                    <Spinner/>
                </td>
            </tr>)
        }

        const date = block ? new Date(block.timestamp) : new Date(data.time);
        const type = data.type;
        const details = block ? data.op.slice(-1)[0] : '';

        return (
            <MediaQuery minWidth={810}>
                {(matches) => {
                    if (matches) {
                        return (
                            <Table.Row>
                                <Table.Cell width={4} className='karma__cell'>
                                    {date.toLocaleString('ru-RU', {
                                        hour12: false,
                                        day: '2-digit',
                                        month: '2-digit',
                                        year: 'numeric',
                                        hour: '2-digit',
                                        minute: '2-digit',
                                    })} UTC
                                </Table.Cell>
                                <Table.Cell width={2} className='karma__cell history__cell--block'>
                                    <a href={`${ApiURI.explorer}/operations/blocks/${data.block_num}`} className="text--secondary">{data.block_num}</a>
                                </Table.Cell>
                                <Table.Cell width={10} className='karma__cell history__cell--desc'>
                                    <div>{description}</div>
                                    <p>{this.exDescription({type, details, user, prvKey, toggleModal})}</p>
                                    {fee > 0 &&
                                        <Translate component='p' content='withdraw.fee' fee={fee} currency={CoreAsset}/>
                                    }
                                </Table.Cell>
                            </Table.Row>
                        )
                    }

                    return (
                        <div className="item item--history">
                            <div className="item__prop">
                                <Translate className="item__label" content='history.date' />
                                <span className="item__value">
                                    {date.toLocaleString('ru-RU', {
                                        hour12: false,
                                        day: '2-digit',
                                        month: '2-digit',
                                        year: 'numeric',
                                        hour: '2-digit',
                                        minute: '2-digit',
                                    })}
                                </span>
                            </div>
                            <div className="item__prop">
                                <Translate className="item__label" content='history.block' />
                                <a href={`${ApiURI.explorer}/operations/blocks/${data.block_num}`} className="item__value">{data.block_num}</a>
                            </div>
                            <div className="item__prop item__prop--desc">
                            <div className="item__value">
                                <div>{description}</div>
                                <p>{this.exDescription({type, details, user, prvKey, toggleModal})}</p>
                                {fee > 0 &&
                                    <Translate component='p' content='withdraw.fee' fee={fee} currency={CoreAsset}/>
                                }
                            </div>
                            </div>
                        </div>
                    )
                }}
            </MediaQuery>

        )
    }

    static propTypes = {
        data: PropTypes.objectOf(PropTypes.any).isRequired,
        user: PropTypes.objectOf(PropTypes.any).isRequired,
        currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
        prvKey: PropTypes.string,
        toggleModal: PropTypes.func.isRequired,
    };

    static defaultProps = {
        prvKey: '',
    }
}

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    user: getUserData(state),
    prvKey: getPrvKey(state),
});

const mapDispatchToProps = dispatch => ({
    toggleModal: () => dispatch(toggleRequestPassword()),
});

export default connect(mapStateToProps, mapDispatchToProps)(History_item)
