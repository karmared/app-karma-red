import React from 'react';
import {Icon} from 'semantic-ui-react';
import {Link, NavLink} from 'react-router-dom';
import {CoreAsset} from '../../../libs'

export const WalletItem = ({data}) => {
    return (
        <li className="currency__item heading--sm">
            <div>
                <span className='cur'>{data.symbol}</span>
                <span className='amount'>{data.amount.toLocaleString('ru-RU').replace(',', '.')}</span>
            </div>
            <div className="btn_wrapper">

                {[
                    data.crypto &&
                        <Link key='add' className='btn btn--square btn--success' to={`/wallet/add/${data.symbol.toLowerCase()}`}>
                            <Icon name='plus'/>
                        </Link>,
                    data.amount > 0 &&
                        <Link key='transfer' className='btn btn--square btn--warn' to={`/wallet/transfer/${data.symbol.toLowerCase()}`}>
                            <Icon name='exchange' />
                        </Link>,

                    data.symbol === CoreAsset || !data.crypto || data.amount === 0
                        ? ''
                        : <Link key='withdraw' className='btn btn--square btn--red' to={`/wallet/withdraw/${data.symbol.toLowerCase()}`}>
                        <Icon name='minus' />
                        </Link>
                ]}

            </div>
        </li>
    )
}
