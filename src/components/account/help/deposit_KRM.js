import React from 'react'
import { Link } from 'react-router-dom'
import Translate from 'react-translate-component'

export const DepositKRM = () => ([
    <Translate key='title' component='h4' className='heading--md' content="help.KRM_deposit"/>,
    <div className='help__article'>
        <Translate component='p' content='help.deposit_KRM.you_can_buy' className='text--main'/>
        <ul className='text--main'>
            <li><a href="https://openledger.io/market/KRM_bitUSD" target='_blank' className='btn btn--link'>OpenLedger</a></li>
            <li><a href="https://market.rudex.org/#/market/RUDEX.KRM_BTS" target='_blank' className='btn btn--link'>RuDEX</a></li>
        </ul>

        <Translate component='p' className='text--main' content='help.deposit_KRM.upload_to_wallet'/>
        <Link to='/wallet' className='btn btn--red'><Translate content='btn.to_wallet'/></Link>

        <Translate component='p' className='text--main' content='help.deposit_KRM.some_question'/>
        <a href="https://t.me/karmaprojecten" target='_blank' className='btn btn--red'>Telegram</a>
    </div>

])
