import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import MediaQuery from 'react-responsive'
import {ChainTypes} from 'karmajs'
import {Table} from 'semantic-ui-react'
import Translate from 'react-translate-component'
import {CoreAsset} from '../../../libs/'

import {getGlobal, getCurrencies} from '../../../reducers/karma/'
import {loadGlobal} from '../../../actions/'
import {Spinner} from '../../helpers'

class Fees extends Component {
    constructor(props){
        super(props);
        this.state = {
            fees: [],
            redirect: false
        };
    }

    componentDidMount () {
        if(this.props.global.parameters){
            const operations = Object.keys(ChainTypes.operations);

            let fees = this.props.global.parameters.current_fees.parameters
                .map((item, index) => ({...item, title: operations[index].split('_').join(' ')}));

            const ordersOperations = fees.slice(1, 6);
            const assetsOperations = fees.slice(10, 20);
            const otherOperations = fees.slice(39, 46);
            const hideOperations = ordersOperations.concat(assetsOperations, otherOperations);

            fees = fees.filter(i => hideOperations.indexOf(i) < 0);

            this.setState({fees})
        } else {
            this.setState({redirect: true})
        }
    }

    render(){
        const { redirect, fees } = this.state;
        const precision = this.props.currencies.filter(e => e.symbol === CoreAsset)[0].precision;

        if(redirect){
            return <Redirect to="/help"/>
        }

        return (
            <MediaQuery minWidth={810}>
                {(matches) => {
                    if (matches) {
                        return [
                            <Translate key='title' component="h4" className='heading--md' content="help.fees"/>,
                            <Table key='table' celled basic='very' selectable className='karma__table market__table'>
                                <Table.Header className='karma__table-header'>
                                    <Table.Row>
                                        <Table.HeaderCell width={1} className='karma__cell karma__cell--header'>
                                            <Translate content="help.fees_table.id"/>
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={3} className='karma__cell karma__cell--header'>
                                            <Translate content="help.fees_table.title"/>
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={7} className='karma__cell karma__cell--header'>
                                            <Translate content="help.fees_table.desc"/>
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={3} className='karma__cell karma__cell--header'>
                                            <Translate content="help.fees_table.fee_per_op"/>
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={2} className='karma__cell karma__cell--header'>
                                            <Translate content="help.fees_table.fee_per_kb"/>
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {fees.map((item, index) =>
                                        <Table.Row key={index}>
                                            <Table.Cell width={1} className='karma__cell'>{index + 1}</Table.Cell>
                                            <Table.Cell width={3} className='karma__cell market__cell--name voting__cell--name'>
                                                {item.title}
                                            </Table.Cell>
                                            <Table.Cell width={7} className='karma__cell'>Описание</Table.Cell>
                                            <Table.Cell width={3} className='karma__cell'>
                                                <span className='btn btn--red btn--disabled btn--small'>
                                                    {`${item[1].fee ? item[1].fee / 10 ** precision : '0'} ${CoreAsset}`}
                                                </span>
                                            </Table.Cell>
                                            <Table.Cell width={2} className='karma__cell'>
                                                {`${item[1].price_per_kbyte ? item[1].price_per_kbyte / 10 ** precision : '0'} ${CoreAsset}`}
                                            </Table.Cell>
                                        </Table.Row>
                                    )}
                                </Table.Body>
                            </Table>
                        ]
                    }
                    return [
                        <h4 key='title' className='heading--md'>Комиссии</h4>,
                        <ul key='help_list' className='fees__list'>
                            <li className='fees__header text--main text--secondary'>
                                <span className='num'>ID</span>
                                <span className='title'>Название операции</span>
                                <span className='desc'>Описание операции</span>
                                <span className='fee'>Комиссия за операцию в KRM</span>
                                <span className='perKB'>Комиссия за 1KB</span>
                            </li>
                            {fees.map((item, index) =>
                                <li className='fees__item text--main'>
                                    <p className='num'>{index + 1} </p>
                                    <b className='title'>{item.title} </b>
                                    <p className='desc'>Описание </p>
                                    <p className='fee'>
                                        <span className='btn btn--red btn--disabled btn--small'>
                                            {`${item[1].fee ? item[1].fee / 10 ** precision : '0'} ${CoreAsset}`}
                                        </span>
                                    </p>
                                    <p className='perKB'>
                                        {`${item[1].price_per_kbyte ? item[1].price_per_kbyte / 10 ** precision : '0'} ${CoreAsset}`}
                                    </p>
                                </li>
                            )}
                        </ul>
                    ]
                }}
            </MediaQuery>
        )
    }
    static propTypes = {
        currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
        loadGlobal: PropTypes.func.isRequired
    }
}

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    global: getGlobal(state)
});

const mapDispatchToProps = dispatch => ({
    loadGlobal: () => dispatch(loadGlobal())
});

export default connect(mapStateToProps, mapDispatchToProps)(Fees)
