import React, {Component} from 'react'
import { Link, Route, Switch } from 'react-router-dom'
import ReactGA from 'react-ga'
import {Grid} from 'semantic-ui-react'
import Translate from 'react-translate-component'

import Fees from './fees'
import MultiFactor from './multi-factor'
import {DepositKRM} from './deposit_KRM'

class Help extends Component {
    componentDidMount() {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    render() {

        const defaultHelp = () => <div>
            <Translate component="h4" className='heading--sm' content="help.default_subtitle"/>
            <Translate component="p" className='text--main' content="help.default_message"/>
        </div>

        return (
            <section id="wallet_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper wallet__wrapper'>
                        <Grid.Column computer={3} mobile={16} className='karma__column'>
                            <div>
                                <Translate component="h4" className='heading--md' content="help.title"/>
                                <div className='help__list'>
                                    <Link to="/help/fees" className='help__item text--main'>
                                        <Translate content="help.fees"/>
                                    </Link>
                                    <Link to="/help/multi-factor" className='help__item text--main'>
                                        <Translate content="help.multi_factor.title"/>
                                    </Link>
                                    <Link to="/help/deposit_KRM" className='help__item text--main'>
                                        <Translate content="help.KRM_deposit"/>
                                    </Link>
                                </div>
                            </div>
                        </Grid.Column>
                        <Grid.Column computer={13} mobile={16} className='karma__column karma__column--scroll'>
                            <div>
                                <Switch>
                                    <Route exact path='/help/' component={defaultHelp}/>
                                    <Route exact path='/help/fees' component={Fees}/>
                                    <Route exact path='/help/deposit_KRM' component={DepositKRM}/>
                                    <Route exact path='/help/multi-factor' component={MultiFactor}/>
                                </Switch>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )
    }
}

export default Help
