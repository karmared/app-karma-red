import React, {Component} from 'react'
import Translate from 'react-translate-component'
import {Link} from 'react-router-dom'
import { Image2fa1, Image2fa2 } from '../../../images/';

class MultiFactor extends Component {
    render(){
        return [
            <Translate component="h4" key='title' className='heading--md' content="help.multi_factor.title"/>,
            <Translate
                component="div"
                key='instruction'
                className='text--main text--secondary'
                content="help.multi_factor.instruction"
                subtitle2fa=''
                links={[
                    <Translate component="b" key='title' className='text--dark' content="help.multi_factor.links_2fa"/>,
                    <ul key='list'>
                        <li><a href='https://itunes.apple.com/us/app/google-authenticator/id388497605' className='link' target='_blank'>Google authenticator for iOS</a></li>
                        <li><a href='https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2' className='link' target='_blank'>Google Authenticator for Android</a></li>
                        <li><a href='https://www.microsoft.com/en-us/store/p/microsoft-authenticator/9nblgggzmcj6?rtc=1' className='link' target='_blank'>Microsoft Authenticator for Windows Phone</a></li>
                    </ul>
                ]}
                profile_link={<Link to='/profile'>{window.location.origin}/profile</Link>}
                warning={<Translate component="b" className='text--dark' content="help.multi_factor.backup_digits"/>}
                help_email={<a href='mailto:help@karma.red' className='link'>help@karma.red</a>}
                images={[
                    <img src={Image2fa1} className='help__image'/>,
                    <img src={Image2fa2} className='help__image'/>
                ]}
            />
        ]
    }
}

export default MultiFactor
