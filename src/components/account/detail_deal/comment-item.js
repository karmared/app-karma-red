import React from 'react'

import {Link} from 'react-router-dom';
import {Avatar} from '../../helpers'

const DealCommentItem = ({name, comment}) => {
    return (
        <div className="comments__item">
            <div className='heading--sm comments__name'>
                <Link to={`/profile/${name}`} className='heading--sm comments__name'>
                    <Avatar img={null} className="avatar--sm" name={{first_name: name}}/>
                    {name}
                </Link>
            </div>
            <p className='text--main comments__text'>
                {comment}
            </p>
        </div>
    )
}

export default DealCommentItem
