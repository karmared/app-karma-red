import React from 'react'
import Translate from 'react-translate-component'

export const DetailCreditCompact = ({data}) => {
    return (
        <div className="info--compact">
            <div>
                <Translate component="h4" className="heading--sm heading--upper" content="deal_detail.title_detail"
                           with={{status: data.status}}/>
                <div className="credit__currency">
                    {data.sum}
                </div>
                <div className="props">
                    <Translate className="props__label" with={{month: <Translate content="months"/>}}
                               content="market.credit_terms"/>
                    <span className="props__value">{data.term}</span>
                </div>
                <div className="props">
                    <Translate className="props__label" content="market.interest_rate"/>
                    <span className="props__value">{data.interest}%</span>
                </div>
                <div className="props">
                    <Translate className="props__label" content="market.collateral"/>
                    <span className="props__value">{data.collateral}</span>
                </div>
                <Translate className="props__label" content="market.purpose"/>
                <p className="text--main">{data.purpose}</p>
            </div>
        </div>
    )
}
