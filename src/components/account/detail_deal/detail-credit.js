import React from 'react'
import Translate from 'react-translate-component'

export const DetailCredit = ({data}) => {

    return (
        <div className='detail__credit'>
            <Translate component="h3" className="heading--md" content="deal_detail.title_detail" with={{ status: data.status }}/>
            <div className="credit__currency">
                {data.sum}
            </div>
            <div className="credit__info">
                <div className="credit__props">
                    <Translate component="p" className="credit__label text--secondary" with={{month: <Translate content="months" />}} content="market.credit_terms" />
                    <span>{data.term}</span>
                </div>
                <div className="credit__props">
                    <Translate component="p" className="credit__label text--secondary" content="market.interest_rate"/>
                    <span>{data.interest}%</span>
                </div>
                <div className="credit__props">
                    <Translate component="p" className="credit__label text--secondary" content="market.collateral"/>
                    {data.collateral}
                </div>
            </div>
            <Translate component="label" className="credit__label text--secondary" content="market.purpose"/>
            <p className='text--main'>{data.purpose}</p>
        </div>
    )
};
