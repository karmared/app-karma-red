import React from 'react'
import {Icon} from 'semantic-ui-react'
import DetailContactsCompact from './detail-contacts-compact'
import { DetailCreditCompact } from './detail-credit-compact'
import Translate from 'react-translate-component'

import {Link } from 'react-router-dom'
import { Avatar, Spinner } from '../../helpers/'

export const DetailUser = ({ data, fullData, dataCredit, path }) => {
    if (!data) {
        return (
            <div className="personal__wrapper detail__wrapper">
                <Spinner className='spinner--absolute'/>
            </div>
        )
    }
    return(
        <div className="personal__wrapper">
            <Link to={`/profile/${data.name}`} className="personal__avatar">
                <Avatar
                    img={data ? data.avatar : null}
                    className="avatar--lg"
                    name={{ first_name: data.first_name || data.name, last_name: data.last_name }}
                />
            </Link>
            <div className="personal__info info--main">
                <Link to={`/profile/${data.name}`} className="personal__name heading--sm">
                    {`${data.first_name ? data.first_name : data.name }${data.last_name ? ` ${data.last_name}` : ''}`}
                    {data.confirmed && <Icon name='shield' className='text--secondary personal__verify' />}
                    <span className='personal__rating'>{parseFloat(data.rating).toFixed(2)}</span>
                </Link>
                <p className="personal__login">
                    {data.first_name && data.name}
                    <span className="personal__id">
                            {data.id}
                        </span>
                </p>
                { data.facebook &&
                <a href={data.facebook} target="_blank" className="btn--link">Facebook</a>
                }
            </div>
            { path === 'market'
                ? <DetailContactsCompact data={fullData} />
                : <DetailCreditCompact data={dataCredit}/>
            }
        </div>
    )
};
