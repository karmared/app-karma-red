import React, {Component} from 'react'
import Translate from 'react-translate-component'
import {ApiURI, Karma} from '../../../libs'
import {getUserData, getUserKeys} from '../../../reducers/karma'
import {clearPrvKey} from '../../../actions'
import {connect} from 'react-redux'

class DetailContactsCompact extends Component {
    constructor(props) {
        super(props)
        this.state = {
            self: false,
        }
    }

    componentDidMount() {
        const {userData, data} = this.props
        if (userData.name === data.name) {
            this.setState({self: true})
        }
    }

    render() {
        const {data} = this.props
        const {self} = this.state
        return (
            <div className="info--compact">
                {self &&
                [
                    <div key='contacts'>
                        <Translate component='h4' key='title' className='heading--sm heading--upper'
                                   content="contacts.title"/>

                        <div key='phone' className="props">
                            <Translate className="props__label" content="contacts.phone"/>
                            <span className="props__value">{data.extra && data.extra.phone ? data.extra.phone : 'n/a'}</span>
                        </div>

                        <div key='tax' className="props">
                            <Translate className="props__label" content="contacts.tax_residence"/>
                            <span className="props__value">{data.extra && data.extra.tax_residence ? data.extra.tax_residence : 'n/a'}</span>
                        </div>

                        <div key='passport_number' className="props">
                            <Translate className="props__label" content="contacts.passport"/>
                            <span className="props__value">{data.extra && data.extra.passport_number ? data.extra.passport_number : 'n/a'}</span>
                        </div>
                    </div>,
                    <div key='bank'>
                        <Translate component='h4' key='titleBank' className='heading--sm heading--upper'
                                   content="contacts.title_bank"/>

                        <div key='name' className="props">
                            <Translate className="props__label" content="contacts.bank_name"/>
                            <span className="props__value">{data.extra && data.extra.bank_name ? data.extra.bank_name : 'n/a'}</span>
                        </div>

                        <div key='swift' className="props">
                            <Translate className="props__label" content="contacts.bank_swift"/>
                            <span className="props__value">{data.extra && data.extra.bank_swift ? data.extra.bank_swift : 'n/a'}</span>
                        </div>

                        <div key='accnum' className="props">
                            <Translate className="props__label" content="contacts.bank_account"/>
                            <span className="props__value">{data.extra && data.extra.bank_account ? data.extra.bank_account : 'n/a'}</span>
                        </div>

                        <div key='beneficiary' className="props">
                            <Translate className="props__label" content="contacts.bank_name_of_beneficiary"/>
                            <span className="props__value">
                                {data.extra && data.extra.bank_name_of_beneficiary ? data.extra.bank_name_of_beneficiary : 'n/a'}
                            </span>
                        </div>
                    </div>
                ]
                }
            </div>
        )

    }

}

const mapStateToProps = state => ({
    userData: getUserData(state),
    userKeys: getUserKeys(state),
    path: state.routing.location.pathname.split('/')[2],
})

const mapDispatchToProps = dispatch => ({
    clearKey: () => dispatch(clearPrvKey()),
})

export default connect(mapStateToProps, mapDispatchToProps)(DetailContactsCompact)
