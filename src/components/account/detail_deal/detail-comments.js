import React, {Component} from 'react'
import Translate from 'react-translate-component'
import { Icon } from 'semantic-ui-react';
import DealCommentItem from './comment-item'
import { SvgDown} from '../../../svg/'
import {Karma, CoreAsset} from '../../../libs/'
import {Spinner} from '../../helpers'
import {getCurrencies, getGlobal, getUserData} from '../../../reducers/karma'
import {connect} from 'react-redux'
import counterpart from "counterpart"

import {loadGlobal, sendComment} from '../../../actions'

const getUsers = async (usersId) => await Karma.dbApi.exec('get_accounts', [usersId]).then(e => e);



const CommentTextArea = ({ value, onChange }) => {
  return (
    <Translate
      component="textarea"
      className="karma__input"
      value={value}
      onChange={onChange}
      attributes={{ placeholder: "deal_detail.comment_placeholder" }}
    />
  )
}


class DetailComments extends Component {
    state = {
        commentList: [],
        commentText: '',
        limit: 10,
        loading: false,
        fee: 0
    }

    showMore = () => {
        this.setState({limit: this.state.limit + 10})
    };

    componentDidMount(){
        const {data, global, currencies } = this.props;
        let users = data ? _.uniqBy(data.list.map(e => e[1])) : [];
        getUsers(users).then(e => {
            const precision = currencies.filter(e => e.symbol === CoreAsset)[0].precision;
            let fee = global.parameters.current_fees.parameters.filter((item, index) => index === 50)[0][1]['fee']/10**precision;
            let commentList = data.list.map((item, index) => (
                <DealCommentItem key={index} name={e.filter(i => i.id === item[1])[0].name} comment={item[0]} />))
            this.setState({commentList, fee});
        })
    }

    handleInput = (e) => {
        this.setState({commentText: e.target.value})
    }

    handleSubmit = () => {
        const {uuid, user} = this.props;
        const {commentList, commentText} = this.state;
        this.setState({loading: true});
        this.props.sendComment({
            memo: commentText,
            id: user.id,
            uuid
        })
            .then(() => {
                this.setState({
                    alert: null,
                    success: true,
                    loading: false,
                    commentText: '',
                    commentList: [...commentList, <DealCommentItem key={commentList.length + 1} name={user.name} comment={commentText} />]
                })
            })
            .catch(error => this.setState({
                alert: `${error.message}`,
                loading: false,
            }))
    }

    render() {
        const {commentList, limit, commentText, loading, fee} = this.state;
        const btnMore = limit < commentList.length
            ? <button type="button" onClick={this.showMore} className="btn btn--more">
                <Translate content="btn.show_more" />
                <SvgDown width={14} height={6} className='ico'/>
            </button>
            : '';
        return (
            <div>
                <Translate component="h3" className="heading--md" content="deal_detail.title_comments" />
                {this.props.status === 'wating_for_acceptance' &&
                    [
                        <Translate key='fee' component='p' className="text--secondary" content='withdraw.fee' fee={fee} currency={CoreAsset}/>,
                        <form key='form' className='comments__form' onSubmit={this.handleSubmit}>
                            <CommentTextArea
                              value={commentText}
                              onChange={this.handleInput}
                            />

                            <button key='btn' type="button" disabled={loading || !commentText.length}
                                    className={`btn btn--ghost${loading ? ' loading' : ''}`}
                                    onClick={this.handleSubmit}>
                                {loading
                                    ? <Spinner className='spinner--inside'/>
                                    : <Icon name='send'/>
                                }
                            </button>

                        </form>
                    ]

                }
                {commentList.length === 0 && this.props.status === 'wating_for_acceptance' &&
                    <div className='comments__mess-ico'>
                        <Icon name='comments' className='ico'/>
                        <Translate component="p" className="text--main" content="deal_detail.comment_first" />
                    </div>
                }
                {commentList.length === 0 && this.props.status !== 'wating_for_acceptance' &&
                    <Translate component="p" className="text--main" content="deal_detail.comment_close" />
                }
                {commentList.filter((item, index) => index < limit)}
                {btnMore}
            </div>
        )
    }

}

const mapDispatchToProps = dispatch => ({
    sendComment: data => dispatch(sendComment(data)),
    loadGlobal: () => dispatch(loadGlobal())
});

const mapStateToProps = state => ({
    user: getUserData(state),
    global: getGlobal(state),
    currencies: getCurrencies(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailComments)
