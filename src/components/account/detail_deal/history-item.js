import React from 'react'
import PropTypes from 'prop-types'

import {Link} from 'react-router-dom';
import Translate from 'react-translate-component'

const DealHistoryItem = ({date, data}) => {
    let text = '';
    switch (data.type) {

        // Monthly payment #xx - xx.xx USDT. Deposit balance - xx.xx BTC
        //
        // Insufficient funds for monthly payments #xx.
        //
        // Insufficient funds for monthly payments #xx. Payment by deposit - xx.xx USDT. Deposit balance - xx.xx BTC
        //
        // Loan paid in full.
        //
        // The loan is closed due to lack of collateral

        case 'request_creation':
            text = <Translate
                component='p'
                content="operation.request_creation"
                className="detail__item-desc"
                with={{
                    borrower: <Link to={`/profile/${data.borrower}`} className="text--accient">{data.borrower}</Link>,
                    id_credit: <span className='text--success'>{data.id}</span>
                }} />;
            break;
        case 'request_approved':
            text = <Translate
                component='p'
                className="detail__item-desc"
                content="operation.request_approved"
                with={{
                    creditor: <Link to={`/profile/${data.creditor}`} className="text--accient">{data.creditor}</Link>,
                    amount: <span className="text--red">{data.loan_amount}</span>,
                    months: <span className="text--red">{data.loan_period_in_month}</span>,
                    payment: <span className="text--red">{data.monthly_payment}</span>
                }}/>;
            break;
        case 'credit_complete_forced':
            text = <Translate
                component='p'
                className="detail__item-desc"
                content="operation.credit_complete_forced" />
            break;
        case 'monthly_settlement':
            text = <Translate
                component='p'
                className="detail__item-desc"
                content="operation.monthly_settlement"
                with={{
                    month_elapsed: <span>{`#${data.month_elapsed}`}</span>,
                    payment: <span className="text--red">{`${data.sum_settled} ${data.asset_settled}`}</span>,
                    balance: <span className="text--success">{`${data.deposit_left_sum} ${data.deposit_asset}`}</span>
                }} />;
            break;
        case 'credit_complete':
            text = <Translate component='p' className="detail__item-desc" content="operation.credit_complete" />
            break;
        case 'stop-loss_order':
            text = <Translate component='p' className="detail__item-desc" content="operation.stop-loss_order" />
            break;
    }

    return (
        <div className="detail__item">
            <span className="detail__item-date">{date}</span>
            {text}
        </div>
    )
}

DealHistoryItem.propTypes = {
    data: PropTypes.object.isRequired,
}

export default DealHistoryItem
