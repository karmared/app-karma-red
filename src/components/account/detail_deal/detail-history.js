import React, {Component} from 'react'
import Translate from 'react-translate-component'
import DealHistoryItem from './history-item'
import { SvgDown} from '../../../svg/'

class DetailHistory extends Component {
    state = {
        renderList: [],
        limit: 5
    }

    showMore = () => {
        this.setState({limit: this.state.limit + 5})
    };

    componentWillMount(){
        const historyJSON = this.props.data.historyJSON;
        let renderList = this.state.renderList,
            date, text;
        for (let i in historyJSON){
            date = new Date(i).toLocaleString('ru-RU', {
                hour12: false, day: '2-digit', month: '2-digit', year: 'numeric', hour: '2-digit', minute: '2-digit',
            });
            text = historyJSON[i];
            renderList.push(<DealHistoryItem key={i} date={date} data={text}/>)
        }
        this.setState({ renderList });
    }

    render() {
        const {renderList, limit} = this.state;
        const btnMore = limit < renderList.length ?
            <button type="button" onClick={this.showMore} className="btn btn--more">
                <Translate content="btn.show_more" />
                <SvgDown width={14} height={6} className='ico'/>
            </button> : '';

        return (
            <div>
                <Translate component="h3" className="heading--md" content="deal_detail.title_history" />
                {renderList.filter((item, index) => index < limit)}
                {btnMore}
            </div>
        )
    }

}

export default DetailHistory;
