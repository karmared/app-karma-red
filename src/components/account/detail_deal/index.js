import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Link, Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import 'whatwg-fetch'
import Translate from 'react-translate-component'
import ReactGA from 'react-ga'

import {redeemAsk, cancelAsk} from '../../../actions/'
import {getQuotations, getUserBalance, getUserData, getGlobal, getCurrencies} from '../../../reducers/karma'
import {Karma, KarmaMultiplier, ApiURI, CoreAsset} from '../../../libs/'
import {Spinner, nFormatter, ModalMessage, ModalConfirm} from '../../helpers/'
import ModalComment from '../../helpers/modal-comment'

import {DetailUser} from './detail-user'
import {DetailCredit} from './detail-credit'
import DetailHistory from './detail-history'
import DetailComments from './detail-comments'

import {Grid, Message} from 'semantic-ui-react';
import {giveCredit} from '../../../actions/market'

const getData = async (uuid) => {
    const offer = await Karma.dbApi.exec('list_credit_request_by_uuid', [uuid]).then(e => e[0]);

    if (!offer) {
        return { alert: <Translate content="deal_detail.no_data" /> }
    }

    const borrower = await Karma.dbApi.exec('get_accounts', [[offer.borrower.borrower]]).then(e => e[0]);

    if (!borrower) {
        return { alert: <Translate content="deal_detail.no_data_borrower" /> }
    }

    const extra = await fetch(`${ApiURI.extraApi}/users/${borrower.name}`).then(e => e.json());

    if (!extra) {
        return {
            offer,
            borrower,
            alert: null,
        }
    }

    return {
        offer,
        borrower: {...borrower, extra},
        alert: null,
    }
}

class DealDetails extends Component {
    state = {
        offer: null,
        borrower: null,
        alert: null,
        alertFee: null,
        redirect: false,
        historyLimit: 5,
        loading: false,
        showModal: '',
    };

    componentDidMount(){
        ReactGA.pageview(window.location.pathname + window.location.search);
        getData(this.props.match.params.uuid).then(e => this.setState(e))
    }

    componentWillReceiveProps(nextProps) {
        getData(nextProps.match.params.uuid).then(e => this.setState(e))
    }

    handleRedeem = () => {
        this.setState({loading: true});

        this.props.redeemAsk(this.state.offer.object_uuid)
            .then(() => this.setState({
                showModal: 'success',
                loading: false,
            }))
            .catch((error) => {
                this.setState({ loading: false, });
                throw Error(error.message);
            })
    };

    handleGiveCredit = () => {
        this.setState({ loading: true });
        this.props.giveCredit(this.state.offer.object_uuid)
            .then(() => this.setState({ showModal: 'success', loading: false }))
            .catch(() => this.setState({ loading: false }))
    };

    handleCancel = () => {
        this.setState({loading: true});
        this.props.cancelAsk(this.state.offer.object_uuid)
            .then(() => {
                this.setState({
                    alert: <Translate component="p" content='deal_detail.ask_canceled'/>,
                    loading: false,
                    redirect: true
                })
            })
            .catch(() => this.setState({
                loading: false,
            }))
    };

    toggleModal = (val) => {
        this.setState({showModal: val})
    };

    renderModal = (data) => {
        const { showModal, loading } = this.state;
        let modalProps = this.state.modalProps;

        switch (showModal){
            case 'redeem':
                return <ModalMessage message = {<Translate component='p' content="deal_detail.thank_for_redeem" />} toggle = {() => this.toggleModal('')} />;
            case 'success':
                return <ModalMessage message = {<Translate component='p' content="modal_thanks_for_interest" />} toggle = {() => this.toggleModal('')} />;
            case 'cancel':
                return (
                    <ModalConfirm
                        title={<Translate className="heading--sm" content='auth.modal_title' />}
                        toggle={() => this.toggleModal('')}
                        message={<Translate component='p' className="text--main" content={`modal_confirm.a_u_sure_cancel`} />}
                        data={data}
                        footer={[
                            loading
                                ? <span key='cancel' className='btn btn--red loading btn--disabled'><Spinner className='spinner--inside'/></span>
                                : <Translate key='cancel' component='button' type="button" className="btn btn--red"
                                             onClick={ this.handleCancel }
                                             content={`btn.cancel_ask`} />
                        ]}
                    />
                );
            case 'give_credit':
                return (
                    <ModalConfirm
                        title={<Translate className="heading--sm" content='auth.modal_title' />}
                        toggle={() => this.toggleModal('')}
                        message={<Translate component='p' className="text--main" content={`modal_confirm.a_u_sure_give`} />}
                        data={data}
                        footer={[
                            loading
                                ? <span key='cancel' className='btn btn--red loading btn--disabled'><Spinner className='spinner--inside'/></span>
                                : <Translate key='cancel' component='button' type="button" className="btn btn--red"
                                             onClick={ this.handleGiveCredit }
                                             content={`btn.give_credit`} />
                        ]}
                    />
                );
            case 'confirm':
                return (
                    <ModalConfirm
                        title={<Translate className="heading--sm" content='auth.modal_title' />}
                        toggle={() => this.toggleModal('')}
                        message={<Translate component='p' className="text--main" content={`modal_confirm.a_u_sure_redeem`} />}
                        data={data}
                        footer={[
                            loading
                                ? <span key='cancel' className='btn btn--red loading btn--disabled'><Spinner className='spinner--inside'/></span>
                                : <Translate key='cancel' component='button' type="button" className="btn btn--red"
                                             onClick={ this.handleRedeem }
                                             content={`btn.redeem`} />
                        ]}
                    />
                );
            case 'comment':
                return <ModalComment toggle={() => this.toggleModal('')} borrower={data.borrower} uuid={data.uuid}/>
        }

        this.setState({ modalProps })
    }

    renderButton = () => {
        const {offer, loading, alertFee} = this.state;
        const {balance, currencies, user, global} = this.props;

        const asset = currencies.filter(e => e.id === offer.borrower.loan_asset.asset_id)[0];

        const precision = 10**asset.precision;
        const amount = offer.borrower.loan_asset.amount / precision;
        const sufficientFunds = offer.borrower.borrower !== user.id && this.props.match.path.split('/')[1] === 'market'
            ? balance.filter(item => item.symbol === asset.symbol && item.amount >= amount).length
            : true;

        if(alertFee){
            return alertFee;
        }

        if (!user.extra.email && offer.borrower.borrower !== user.id) {
            return (
                <Translate component="b" className="text--red heading--sm" content='deal_detail.message_email'
                           profile_link={<Link to="/profile/edit" className='text--accient text--margin'><Translate content='profile.profile'/></Link>}/>
            );
        }

        if (!user.extra.confirmed && offer.borrower.borrower !== user.id) {
            return (
                <Translate component="b" className="text--red heading--sm" content='deal_detail.message_verify'/>
            )
        }

        if (offer.status === 'in_progress' && this.props.match.path.split('/')[1] !== 'market' && offer.borrower.borrower === user.id) {

            let coreAsset = currencies.filter(e => e.symbol === CoreAsset)[0];
            let fee = global.operations.filter(e => e.operation === 'settle_credit_operation')[0].fee / 10**coreAsset.precision;
            let coreAmount = balance.filter(e => e.symbol === CoreAsset)[0].amount;

            if (fee > coreAmount) {
                return this.setState({
                    loading: false,
                    showModal: '',
                    alertFee: <Translate component="p"
                                         className='text--main text--red'
                                         content="deal_detail.need_asset"
                                         fee={fee}
                                         link={<Link to='/help/deposit_KRM'><Translate content='btn.link'/></Link>}/>,
                })
            }

            return (
                <Translate  component="button" type="button" className='btn btn--red' key="redeem"
                            onClick={() => this.toggleModal('confirm')} content="btn.redeem" />
            )
        }

        if (offer.status !== 'wating_for_acceptance') {
            let displayedStatus = offer.status;

            if (offer.status === 'complete_normal')
                displayedStatus = <Translate component="b" className="text--success text--margin" content="deal_detail.status_complete" />;
            else if (offer.status === 'in_progress')
                displayedStatus = <Translate component="b" className="text--warning text--margin" content="deal_detail.status_processing" />;

            return <Translate className="text--secondary heading--sm" content="deal_detail.status" status={displayedStatus} />
        }

        if (!sufficientFunds) {
            return <Translate component="b" key="alertNoMoney" className="btn btn--link text--red" content='deal_detail.message_money'/>
        }

        return (
            offer.borrower.borrower === user.id
                ? loading
                ? <span className='btn btn--red loading btn--disabled'><Spinner className='spinner--inside'/></span>
                : <Translate component="button" type="button" className='btn btn--red' key="cancel_ask"
                             onClick={() => this.toggleModal('cancel')} content="btn.cancel_ask" />
                : loading
                ? <span className='btn btn--red loading btn--disabled'><Spinner className='spinner--inside'/></span>
                : <Translate  component="button" type="button" className='btn btn--red' key="give_credit"
                              onClick={() => this.toggleModal('give_credit')} content="btn.give_credit" />
        )
    };

    render() {
        const {user, quotations, currencies} = this.props;
        const {offer, borrower, alert, historyLimit, loading, redirect, showModal } = this.state;

        if (redirect) {
            return (
                <Redirect to={this.props.match.path.split('/')[1] !== 'market' ? '/deals' : '/market'}/>
            )
        }

        if (alert) {
            return (
                <section id="detail_page" className='content'>
                    <Grid celled className='karma__grid'>
                        <Grid.Row className='karma__wrapper'>
                            <Grid.Column computer={4} tablet={16} className='karma__column'>
                                <div>
                                    <Message className='message--warning'>
                                        {alert}
                                    </Message>
                                    <Link to="/deals" className="btn btn--red"><Translate content="deal_detail.see_all_deals" /></Link>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </section>
            )
        }

        if (!borrower) {
            return <Spinner className='spinner--absolute'/>
        }

        if (false && user.id !== borrower.id && user.id !== offer.creditor.creditor && this.props.match.path.split('/')[1] !== 'market') {
            return (
                <section id="detail_page" className='content'>
                    <Grid celled className='karma__grid'>
                        <Grid.Row className='karma__wrapper'>
                            <Grid.Column computer={4} tablet={16} className='karma__column'>
                                <Message key="dealsSection" warning className='message--warning'>
                                    <Translate content="deal_detail.warning_only_your" />
                                    <Link to="/deals" className="btn btn--red"><Translate content="deal_detail.see_all_deals" /></Link>
                                </Message>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </section>
            )
        }

        let asset = currencies.filter(e => e.id === offer.borrower.loan_asset.asset_id)[0];
        let assetDeposit = currencies.filter(e => e.id === offer.borrower.deposit_asset.asset_id)[0];

        let symbolL = asset.symbol !== CoreAsset ? asset.symbol.substr(1) : asset.symbol;
        let symbolD = assetDeposit.symbol !== CoreAsset ? assetDeposit.symbol.substr(1) : assetDeposit.symbol;

        if(quotations.length > 0){
            let coincidenceL = quotations.filter(e => e.symbol === symbolL),
                coincidenceD = quotations.filter(e => e.symbol === symbolD);
            asset = {
                ...asset,
                price_usd: coincidenceL.length > 0
                    ? coincidenceL[0].price_usd
                    : 1
            }
            assetDeposit = {
                ...assetDeposit,
                price_usd: coincidenceD.length > 0
                    ? coincidenceD[0].price_usd
                    : 1
            }
        }

        const precisionDeposit = 10**assetDeposit.precision;
        const precisionLoan = 10**asset.precision;
        const amountL = offer.borrower.loan_asset.amount / precisionLoan;
        const amountD = offer.borrower.deposit_asset.amount / precisionDeposit;

        let collateral = quotations.length > 0 ? amountD / (amountL / assetDeposit.price_usd * asset.price_usd ) * 100 : amountD / amountL * 100;

        collateral = Math.round(collateral * 100) / 100;
        if (collateral <= 100) collateral = <Translate content="collateral_free" />;
        else collateral += '%';

        let displayedStatus = offer.status;

        if (offer.status === 'wating_for_acceptance') displayedStatus = <Translate component="b" className="status--new" content="deal_detail.status_new" />;
        else if (offer.status === 'complete_normal') displayedStatus = <Translate component="b" className="status--success" content="deal_detail.status_complete" />;
        else if (offer.status === 'in_progress') displayedStatus = <Translate component="b" className="status--warning" content="deal_detail.status_processing" />;

        const dataUser = {
            avatar: borrower.extra ? borrower.extra.avatar.medium.url : null,
            first_name: borrower.extra.first_name,
            last_name: borrower.extra.last_name,
            name: borrower.name,
            id: borrower.id,
            rating: borrower.karma,
            confirmed: borrower.extra.confirmed,
            facebook: borrower.extra.facebook,
            facebookLink: `https://www.facebook.com/${borrower.extra.facebook}`
        };

        const dataCredit = {
            status: displayedStatus,
            sum: `${amountL.toLocaleString('ru-RU')} ${symbolL}`,
            term: offer.borrower.loan_period,
            interest: parseFloat(offer.borrower.loan_persent),
            collateral: collateral,
            purpose: offer.borrower.loan_memo || 'n/a',
        };

        const dataHistory = {
            limit: historyLimit,
            list: offer.history,
            historyJSON: JSON.parse(offer.history_json)
        };

        const dataComments = {
            list: offer.comments,
        };

        return (
            <section id="detail_page" className='content'>
                <Grid celled className='karma__grid vertical'>
                    <Grid.Row className='karma__wrapper fullspace'>
                        <Grid.Column computer={4} tablet={16} className='karma__column detail__column personal karma__column--scroll'>
                            <DetailUser data={dataUser} fullData={borrower} dataCredit={dataCredit} path={this.props.match.path.split('/')[1]}/>
                        </Grid.Column>
                        <Grid.Column computer={12} tablet={16} className='karma__inner profile__inner'>
                            <Grid celled className='karma__grid'>
                                <Grid.Row className='karma__wrapper deal__detail'>
                                    <Grid.Column computer={8} tablet={8} mobile={16}
                                                 className='karma__column'>
                                        {this.props.match.path.split('/')[1] === 'market'
                                            ? <DetailCredit data={dataCredit}/>
                                            : <DetailHistory data={dataHistory}/>
                                        }
                                    </Grid.Column>

                                    {offer.history.length > 0 &&
                                        <Grid.Column computer={8} tablet={8} mobile={16} className='karma__column karma__column--scroll'>
                                            <DetailComments uuid={offer.object_uuid} data={dataComments} status={offer.status} />
                                        </Grid.Column>
                                    }

                                </Grid.Row>
                            </Grid>
                        </Grid.Column>
                    </Grid.Row>
                    <div className='btn_panel'>
                        {this.renderButton()}
                    </div>
                    {showModal.length > 0 &&
                        this.renderModal( showModal !== 'comment' ? dataCredit : {borrower: borrower, uuid: offer.object_uuid})
                    }
                </Grid>
            </section>
        )
    }

    static propTypes = {
        match: PropTypes.objectOf(PropTypes.any).isRequired,
        currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
        user: PropTypes.objectOf(PropTypes.any).isRequired,
        redeemAsk: PropTypes.func.isRequired,
        cancelAsk: PropTypes.func.isRequired,
    }
}

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    user: getUserData(state),
    global: getGlobal(state),
    quotations: getQuotations(state),
    balance: getUserBalance(state)
});

const mapDispatchToProps = dispatch => ({
    redeemAsk: data => dispatch(redeemAsk(data)),
    cancelAsk: data => dispatch(cancelAsk(data)),
    giveCredit: data => dispatch(giveCredit(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(DealDetails)
