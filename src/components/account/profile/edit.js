import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'
import FacebookLogin from 'react-facebook-login'
import {PhoneNumberUtil, ShortNumberInfo} from 'google-libphonenumber'
import _ from 'lodash'
import {Grid, Dropdown, Message, Checkbox } from 'semantic-ui-react'
import Translate from 'react-translate-component'

import {getUserData, getUserKeys} from '../../../reducers/karma/'
import {userEdit, multiFactor} from '../../../actions/user'
import {ApiURI} from '../../../libs/'
import Countries from '../../../libs/countries.json'

import {SvgProfile} from '../../../svg/'
import {Spinner, Modal, Karma_input, ModalQR, Avatar} from '../../helpers/'

import EmailVerification from './email-verification'

const phoneUtil = PhoneNumberUtil.getInstance();
const FacebookId = '1755375728090614'
const countriesList = _.sortBy(Countries, country => country.name)


import { ProfileForm } from "/components"


class ProfileFormContainer extends Component {
    constructor(props) {
        super(props)
        const {user} = this.props

        for (let prop in user.extra) {
            if (user.extra[prop] === null) {
                user.extra[prop] = ''
            }
        }

        this.state = {
            ...user.extra,
            tempAvatar: null,
            loading: false,
            success: false,
            alert: '',
            alertToken: '',
            check2FA: false,
            multiFA: 'ga',
            qr: '',
            secret: '',
            token: '',
            modal2FA: false,
            request2FA: '',
            modalConfirmPhone: false,
            modalConfirmEmail: false,
        }
    }

    componentDidMount() {
        const {user, keys, multiFactor} = this.props
        multiFactor({operation:'GET_STATUS', username: user.name, keys}).then(e => {this.setState({ check2FA: e.data.isSecondFactorActive })})
    }

    toggleModal2FA = () => {
        this.setState({ modal2FA: !this.state.modal2FA, qr: '' })
    };

    handleAvatar = (e) => {
        if (e.target.files && e.target.files[0]) {
            const FR = new FileReader()

            FR.addEventListener('load', (img) => {
                this.setState({tempAvatar: img.target.result, alert: '', success: false})
            })

            const file = e.target.files[0]

            if (file.size > 3000000) {
                return this.setState({alert: 'Your image is too big. Max file size allowed is < 3mb', success: false})
            }

            FR.readAsDataURL(file)
        }

        return true
    }

    handleInput = (e) => {
        if (e.target.name) {
            this.setState({[e.target.name]: e.target.value, success: false})
        }
    }
    handleChange = (name, value) => {
        this.setState({[name]: value, success: false})
    }

    handleFbResponse = (data) => {
        if (data.id) {
            this.setState({facebook: data.id, success: false})
        }
    }

    handle2FA = () => {
        const {user, keys, multiFactor} = this.props
        const {check2FA} = this.state
        if(!check2FA){
            multiFactor({operation:'PUT_ENABLE', username: user.name, keys})
                .then(e => this.setState({ qr: e.data.uri, secret: e.data.secret, modal2FA: true }))
        } else {
            this.setState({modal2FA: true, request2FA: 'disable'})
        }
    }

    activate2FA = () => {
        const { user, keys, multiFactor } = this.props
        const { token } = this.state
        multiFactor({operation:'PUT_ACTIVATE_TOKEN', username: user.name, keys, token})
            .then(e => {
                if(!e) return this.setState({alertToken: <Translate className="text--red text--main" content='modal_qr.invalid_token' />})
                this.setState({qr: '', modal2FA: false, check2FA: true, secret: '', alertToken: ''})
            })
            .catch(() => this.setState({alertToken: <Translate className="text--red text--main" content='modal_qr.invalid_token' />}))
    }

    disable2FA = () => {
        const { user, keys, multiFactor } = this.props
        const { token } = this.state
        multiFactor({operation:'PUT_DISABLE', username: user.name, keys, token})
            .then(e => {
                if(!e) return this.setState({alertToken: <Translate className="text--red text--main" content='modal_qr.invalid_token' />})
                this.setState({modal2FA: false, request2FA: '', check2FA: false})
            })
            .catch(() => this.setState({alertToken: <Translate className="text--red text--main" content='modal_qr.invalid_token' />}))
    }

    handleFbRemove = () => {
        this.setState({facebook: '', success: false})
    }

    handleSubmit = (e) => {
        e.preventDefault()

        this.setState({success: false, alert: null})

        const {user, keys} = this.props
        const {phone, tax_residence, company_website, company_youtube, company_pdf_presentation, email} = this.state

        // try {
        //     phoneUtil.parse(phone)
        // }
        // catch (error) {
        //     return this.setState({alert: <p className='heading--sm text--red'>{error.message}</p>})
        // }
        //
        // const tx = Countries.filter(item => item.name === tax_residence)[0].code;
        //
        // const number = phoneUtil.parseAndKeepRawInput(phone, tx);
        //
        // if (!phoneUtil.isValidNumber(number)) {
        //     return this.setState({alert: <Translate component="p" className='heading--sm text--red' content="edit_profile.valid_phone"/>})
        // }

        const regLink = /^(ftp|http|https):\/\/[^ "]+$/,
            regEmail = /^[\w-_\.]+@[a-zA-Z_]+?\.[a-zA-Z]{2,5}$/

        if(company_website.length > 0 && !regLink.test(company_website)){
            return this.setState({alert: <Translate component="p" className='heading--sm text--red' content="edit_profile.valid_website"/>})
        }

        // console.log(!regEmail.test(email));

        if(email.length > 0 && !regEmail.test(email)){
            return this.setState({alert: <Translate component="p" className='heading--sm text--red' content="edit_profile.valid_email"/>})
        }

        if(company_pdf_presentation.length > 0 && !regLink.test(company_pdf_presentation)){
            return this.setState({alert: <Translate component="p" className='heading--sm text--red' content="edit_profile.valid_pdf"/>})
        }

        this.setState({loading: true})

        this.props.save(this.state)
            .then(() => {
                this.setState({loading: false, alert: null, success: true})
            })
            .catch((error) => {
                const msg = `${error}`
                this.setState({loading: false, alert: <p className='heading--sm text--red'>{msg.split('bitshares-crypto')[0]}</p>})
            })

        return true
    }

    verifyPhone = () => {
        this.setState({modalConfirmPhone: !this.state.modalConfirmPhone})
    }

    verifyEmail = () => {
        this.setState({modalConfirmEmail: !this.state.modalConfirmEmail})
    }

    render__() {
      return (
        <ProfileForm
          {...this.props}
          values={this.state}
          onChange={{
            otp: this.handle2FA,
            input: this.handleInput,
            avatar: this.handleAvatar,
          }}
        />
      )
    }

    render() {
        const {user, fillRequest, match} = this.props

        const {
            alert, success, loading, modalConfirmPhone, modalConfirmEmail, passport_number, modal2FA, request2FA,
            tempAvatar, first_name, last_name, email, check2FA, multiFA, qr, token, secret, alertToken,
            facebook, phone, tax_residence,
            bank_name, bank_swift, bank_account, bank_name_of_beneficiary,
            about_me, company_name, company_activity, company_vat_id, company_website, company_youtube, company_pdf_presentation,
        } = this.state

        if (match.params.login !== user.name) {
            return (
                <section id='settings_page' className='content'>
                    <Grid celled className='karma__grid'>
                        <Grid.Row className='karma__wrapper message__wrapper'>
                            <Grid.Column>
                                <Message className='message--danger'>
                                    <Translate component="p" content="edit_profile.edit_foreign"/>
                                </Message>
                                <Link to={`/profile/${user.name}/edit`} className="btn btn--red">
                                    <Translate content="btn.edit_profile"/></Link>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </section>
            )

        }
        let avatar = user.extra ? user.extra.avatar.medium.url : null
        // if (avatar) avatar = ApiURI.extraBase + avatar

        const notAllFilled = !(first_name.length > 0 && last_name.length > 0
            && email.length > 0 //&& phone.length > 0
            // && passport_number.length > 0
            // && tax_residence.length > 0 && bank_name.length > 0
            // && bank_swift.length > 0 && bank_account.length > 0
            // && bank_name_of_beneficiary.length > 0
        )

        return (
            <section id='settings_page' className='content'>
                <Grid celled className='karma__grid vertical'>
                    <Grid.Row className='karma__wrapper fullspace'>
                        <Grid.Column
                            computer={8}
                            tablet={16}
                            className={`karma__column karma__column--scroll ${loading ? 'box-loading' : ''}`}>
                            <div>
                                <div className="settings__block">
                                    <Translate component="h4" className='heading--md' with={{name: user.name}}
                                               content={`edit_profile.${avatar ? 'title_edit' : 'title' }`}/>
                                    <Translate component="p" className='heading--sm'
                                               content="edit_profile.complete_all_fields"/>

                                    <div className="avatar__upload">
                                        <Avatar
                                            img={tempAvatar || avatar ? tempAvatar || avatar : null}
                                            className="avatar--lg personal__avatar"
                                            edit={tempAvatar}
                                            name={{
                                                first_name: user.extra
                                                    ? user.extra.first_name
                                                    : user.name,
                                                last_name: user.extra
                                                    ? user.extra.last_name
                                                    : ''
                                            }}
                                        />
                                        <div className="btn btn--link btn--red_text karma__file-input">
                                            <span>
                                                {avatar ? <Translate content="btn.change_photo"/> :
                                                    <Translate content="btn.upload_photo"/>}
                                            </span>
                                            <input type="file" onChange={this.handleAvatar}
                                                   accept="image/x-png,image/gif,image/jpeg"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="settings__block">
                                    <Translate component="h4" className='heading--md' content="edit_profile.title_personal"/>
                                    <div className="karma__field">
                                        <Translate className='karma__label required' content="contacts.first_name"/>
                                        <Karma_input required name="first_name"
                                                     value={first_name}
                                                     onChange={this.handleInput} validate="string"/>
                                    </div>

                                    <div className="karma__field">
                                        <Translate className='karma__label required' content="contacts.last_name"/>
                                        <Karma_input required name="last_name"
                                                     value={user.extra ? last_name : ''}
                                                     onChange={this.handleInput} validate="string"/>
                                    </div>

                                    <div className="karma__field">
                                        <div className="karma__label required">Email</div>
                                        <Karma_input type="email" required name="email" value={email} onChange={this.handleInput} validate="email"/>
                                        {(user.extra.confirmed === false && email === user.extra.email) &&
                                            <Translate component='button' content="btn.verify_email" type="button" onClick={this.verifyEmail} className="btn btn--link" />
                                        }
                                        {modalConfirmEmail &&
                                            <EmailVerification login={user.name} toggleModal={this.verifyEmail} />
                                        }
                                    </div>

                                    <div className="karma__field">
                                        <Translate className='karma__label'
                                                   content="edit_profile.multi_factor"/>
                                        <Checkbox className='karma__toggle text--main'
                                                  label={<Translate component='label' content="edit_profile.multi_factor"/>}
                                                  name='check2FA'
                                                  toggle
                                                  checked={check2FA}
                                                  onChange={this.handle2FA}/>

                                    </div>

                                    {modal2FA  &&
                                        <ModalQR
                                            qr={qr}
                                            token={token}
                                            secret={secret}
                                            request={request2FA}
                                            toggleModal={this.toggleModal2FA}
                                            activeBtn={check2FA ? this.disable2FA : this.activate2FA}
                                            handleInput={this.handleInput}
                                            alert={alertToken}
                                        />
                                    }

                                </div>


                                <div className="settings__block">
                                    <Translate component='h4' className='heading--md' content="contacts.title"/>
                                    <div className="karma__field settings__input facebook_connect">
                                        <div className="karma__label">Facebook</div>
                                        <span>{facebook ? 'Connected' : 'Not connected'}</span>
                                        {facebook
                                            ? <button type="button" onClick={this.handleFbRemove}
                                                      className="btn btn--link btn--red_text">
                                                <Translate content="btn.facebook_disconnect"/></button>
                                            : <FacebookLogin
                                                appId={FacebookId}
                                                fields="name,email,picture"
                                                callback={this.handleFbResponse}
                                                cssClass="btn btn--link btn--red_text"
                                                textButton={<Translate content="btn.facebook_connect"/>}
                                                icon={null}
                                            />
                                        }
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.phone"/>
                                        <Karma_input type="tel" name="phone" placeholder="+"
                                                     value={phone} onChange={this.handleInput} validate="phone"/>

                                        {modalConfirmPhone &&
                                        <Modal title="Phone Number Verification" toggleModal={this.verifyPhone}>
                                            <div>
                                                <div className="text-center mt-3">
                                                    Please enter the code we sent<br/>
                                                    to your phone number {phone}
                                                </div>
                                                <div className="verification-input my-4">
                                                    <Karma_input required maxLength="1" validate="digits"/>
                                                    <Karma_input required maxLength="1" validate="digits"/>
                                                    <Karma_input required maxLength="1" validate="digits"/>
                                                    <Karma_input required maxLength="1" validate="digits"/>
                                                    <Karma_input required maxLength="1" validate="digits"/>
                                                </div>
                                                <div className="text-center">
                                                    <button type="button" className="btn btn-primary px-4"><Translate
                                                        content="btn.verify_phone"/>
                                                    </button>
                                                    <br/>
                                                    <button type="button" className="btn btn-link mt-1 font-weight-500">Re-send code</button>
                                                </div>
                                            </div>
                                        </Modal>
                                        }
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.tax_residence"/>
                                        <Dropdown
                                            fluid
                                            search
                                            selection
                                            name="tax_residence"
                                            defaultValue={tax_residence}
                                            className='karma__input settings__input'
                                            onChange={(e, {value}) => this.handleChange('tax_residence', value)}
                                            options={
                                                countriesList.map(country => {
                                                    return {key: country.name, value: country.name, text: country.name}
                                                })
                                            }
                                        />
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.passport"/>
                                        <Karma_input name="passport_number" value={passport_number || ''}
                                                     onChange={this.handleInput}/>
                                    </div>
                                </div>

                                <div className="settings__block">
                                    <Translate component="h4" className="heading--md" content="contacts.title_about"/>
                                    <textarea rows="4" name="about_me" value={about_me} className="karma__input"
                                              onChange={this.handleInput}/>

                                </div>

                            </div>
                        </Grid.Column>

                        <Grid.Column
                            computer={8}
                            tablet={16}
                            className={`karma__column karma__column--scroll ${loading ? 'box-loading' : ''}`}>
                            <div>
                                <div className="settings__block">
                                    <Translate component="h4" className="heading--md" content="contacts.title_bank"/>
                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.bank_name"/>
                                        <Karma_input name="bank_name" value={bank_name}
                                                     onChange={this.handleInput}/>
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.bank_swift"/>
                                        <Karma_input name="bank_swift" value={bank_swift}
                                                     onChange={this.handleInput}/>
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.bank_account"/>
                                        <Karma_input name="bank_account" value={bank_account}
                                                     onChange={this.handleInput} validate="digits"/>
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label"
                                                   content="contacts.bank_name_of_beneficiary"/>
                                        <Karma_input name="bank_name_of_beneficiary"
                                                     value={bank_name_of_beneficiary}
                                                     onChange={this.handleInput}/>
                                    </div>
                                </div>
                                <div className="settings__block">
                                    <Translate component="h4" className="heading--md" content="contacts.title_company"/>
                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.company_name"/>
                                        <Karma_input name="company_name" value={company_name}
                                                     onChange={this.handleInput}/>
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.company_activity"/>
                                        <Karma_input name="company_activity" value={company_activity}
                                                     onChange={this.handleInput}/>
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.company_VAT"/>
                                        <Karma_input name="company_vat_id" value={company_vat_id} validate="link"
                                                     onChange={this.handleInput}/>
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label" content="contacts.company_website"/>
                                        <Karma_input type="url" placeholder="https://" name="company_website"
                                                     value={company_website} onChange={this.handleInput}/>
                                    </div>

                                    <div className="karma__field">
                                        <Translate className="karma__label"
                                                   content="contacts.company_pdf_presentation"/>
                                        <Karma_input type="url" placeholder="https://" name="company_pdf_presentation"
                                                     value={company_pdf_presentation} onChange={this.handleInput}/>
                                    </div>
                                </div>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                    <div className='btn_panel'>

                        { alert }
                        { success && <Translate component="p" className="text--success heading--sm" content="edit_profile.data_saved"/> }
                        { fillRequest && <Translate component="p" className="text--red heading--sm" content="edit_profile.need_complete_all_fields"/> }

                        <Translate component='button' content="btn.edit_save" type="submit" disabled={notAllFilled || success === true}
                                   className="btn btn--red" onClick={this.handleSubmit}/>
                    </div>
                </Grid>
            </section>
        )
    }

    static propTypes = {
        user: PropTypes.objectOf(PropTypes.any).isRequired,
        match: PropTypes.objectOf(PropTypes.any).isRequired,
        save: PropTypes.func.isRequired,
        fillRequest: PropTypes.bool,
    }

    static defaultProps = {
        fillRequest: false,
    }
}

const mapStateToProps = state => ({
    user: getUserData(state),
    keys: getUserKeys(state),
})

const mapDispatchToProps = dispatch => ({
    save: data => dispatch(userEdit(data)),
    multiFactor: data=> dispatch(multiFactor(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileFormContainer)
