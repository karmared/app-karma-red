import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import 'whatwg-fetch'
import ReactGA from 'react-ga'
import { Grid, Message } from 'semantic-ui-react'

import {clearPrvKey} from '../../../actions/'
import {getUserData, getUserKeys} from '../../../reducers/karma/'
import {Karma, ApiURI} from '../../../libs/'

import { Spinner } from '../../helpers/'


import Profile_info from './profile_info'
import Contacts from './contacts'
import Statistic from './statistic'


const getUserByLogin = async (login) => {
    const user = await Karma.dbApi.exec('get_account_by_name', [login]).then(e => e);
    if (!user) {
        const alert = <p>User with login <b>{login}</b> not found.</p>;

        return {
            alert,
        }
    }

    const extra = await fetch(`${ApiURI.extraApi}/users/${user.name}`).then(e => e.json());

    if (!extra) {
        return {
            user,
            alert: null,
            self: false,
        }
    }

    return {
        user: { ...user, extra },
        alert: null,
        self: false,
    }
};

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            alert: null,
            self: false,
        }
    }

    componentDidMount() {
        const { userData, path } = this.props;

        if (path && path !== 'edit' && path !== userData.name) {
            getUserByLogin(path).then(e => this.setState(e))
        } else {
            this.setState({
                user: userData,
                alert: null,
                self: true,
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        const { userData, path } = nextProps;
        if (path && path !== userData.name) {
            getUserByLogin(path).then(e => this.setState(e))
        }
        else {
            this.setState({
                user: userData,
                alert: null,
                self: true,
            })
        }
    }

    render() {
        const {user, alert, self} = this.state;
        const { clearKey } = this.props;
        if (alert) {
            return (
                <section id="profile_page" className='content'>
                    <Grid celled className='karma__grid'>
                        <Grid.Row className='karma__wrapper profile__wrapper'>
                            <Grid.Column>
                                <Message warning className='message--warning'>
                                    {alert}
                                </Message>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </section>
            )
        }

        if (!user) {
            return (
                <section id="profile_page" className='content'>
                    <Spinner className='spinner--absolute'/>
                </section>
            )
        }

        let fillIn = false;
        if(user.extra){
            fillIn = user.extra.bank_account;
        }

        return (

            <section id="profile_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper profile__wrapper'>
                        <Grid.Column computer={4} tablet={16} className='karma__column profile__column personal'>
                            <Profile_info user={user} self={self} fillIn={fillIn} clearKey={clearKey}/>
                        </Grid.Column>
                        <Grid.Column computer={12} tablet={16} className='karma__inner profile__inner'>
                            <Grid celled className='karma__grid'>
                                <Grid.Row className='karma__wrapper'>
                                    <Grid.Column computer={8} mobile={16} className='karma__column profile__column karma__column--scroll'>
                                        <Contacts data={user} self={self}/>
                                    </Grid.Column>
                                    <Grid.Column computer={8} mobile={16} className='karma__column profile__column karma__column--scroll'>
                                        <Statistic data={user} self={self}/>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )
    }
}

Profile.propTypes = {
    userData: PropTypes.objectOf(PropTypes.any).isRequired,
    path: PropTypes.string,
    clearKey: PropTypes.func.isRequired,
};

Profile.defaultProps = {
    path: null,
};

const mapStateToProps = state => ({
    userData: getUserData(state),
    userKeys: getUserKeys(state),
    path: state.routing.location.pathname.split('/')[2],
});

const mapDispatchToProps = dispatch => ({
    clearKey: () => dispatch(clearPrvKey()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
