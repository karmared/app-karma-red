import React from 'react'
import Translate from 'react-translate-component'

class Contacts extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {}
        }
    }

    componentDidMount() {

    }

    render() {
        const {data, self} = this.props

        return (
            <div className="info--contacts">
                {self &&
                [<Translate component='h4' key='title' className='heading--md' content="contacts.title" />,

                    data.extra && data.extra.facebook &&
                    <div key='facebook' className="props">
                        <div className="props__label">Facebook</div>
                        <a href={`https://www.facebook.com/${data.extra.facebook}`} target="_blank"
                           className="props__value text--red">View</a>
                    </div>,

                    <div key='phone' className="props">
                        <Translate className="props__label" content="contacts.phone" />
                        <span
                            className="props__value">{data.extra && data.extra.phone ? data.extra.phone : 'n/a'}</span>
                    </div>,

                    <div key='tax' className="props">
                        <Translate className="props__label" content="contacts.tax_residence" />
                        <span
                            className="props__value">{data.extra && data.extra.tax_residence ? data.extra.tax_residence : 'n/a'}</span>
                    </div>,

                    <div key='passport_number' className="props">
                        <Translate className="props__label" content="contacts.passport" />
                        <span
                            className="props__value">{data.extra && data.extra.passport_number ? data.extra.passport_number : 'n/a'}</span>
                    </div>,

                    <Translate component='h4' key='titleBank' className='heading--md' content="contacts.title_bank" />,

                    <div key='name' className="props">
                        <Translate className="props__label" content="contacts.bank_name" />
                        <span
                            className="props__value">{data.extra && data.extra.bank_name ? data.extra.bank_name : 'n/a'}</span>
                    </div>,

                    <div key='swift' className="props">
                        <Translate className="props__label" content="contacts.bank_swift" />
                        <span
                            className="props__value">{data.extra && data.extra.bank_swift ? data.extra.bank_swift : 'n/a'}</span>
                    </div>,

                    <div key='accnum' className="props">
                        <Translate className="props__label" content="contacts.bank_account" />
                        <span
                            className="props__value">{data.extra && data.extra.bank_account ? data.extra.bank_account : 'n/a'}</span>
                    </div>,

                    <div key='beneficiary' className="props">
                        <Translate className="props__label" content="contacts.bank_name_of_beneficiary" />
                        <span className="props__value">
                                {data.extra && data.extra.bank_name_of_beneficiary ? data.extra.bank_name_of_beneficiary : 'n/a'}
                            </span>
                    </div>,
                ]
                }

                {data.extra && data.extra.about_me &&
                [
                    <Translate component='h4' key='title' className='heading--md' content="contacts.title_about" />,
                    <div key='about' className="props props--about">
                        <span className="props__value">{data.extra.about_me}</span>
                    </div>
                ]
                }

                {data.extra && (data.extra.company_name
                    || data.extra.company_activity
                    || data.extra.company_vat_id
                    || data.extra.company_website
                    || data.extra.company_youtube
                    || data.extra.company_pdf_presentation) &&
                [
                    <Translate component='h4' key='title' className='heading--md' content="contacts.title_company" />,

                    <div key='comname' className="props">
                        <Translate className="props__label" content="contacts.company_name" />
                        <span className="props__value">{data.extra.company_name || 'n/a'}</span>
                    </div>,

                    <div key='comactiv' className="props">
                        <Translate className="props__label" content="contacts.company_activity" />
                        <span className="props__value">{data.extra.company_activity || 'n/a'}</span>
                    </div>,

                    self &&
                    <div key='comvat' className="props">
                        <Translate className="props__label" content="contacts.company_VAT" />
                        <span className="props__value">{data.extra.company_vat_id || 'n/a'}</span>
                    </div>,

                    data.extra.company_website &&
                    <div key='comweb' className="props">
                        <Translate className="props__label" content="contacts.company_website" />
                        <a href={data.extra.company_website} target="_blank"
                           className="props__value text--red">{data.extra.company_website}</a>
                    </div>,

                    data.extra.company_youtube &&
                    <div key='comyoutube' className="props">
                        <span className="props__label">YouTube</span>
                        <a href={data.extra.company_youtube} target="_blank"
                           className="props__value text--red">{data.extra.company_youtube}</a>
                    </div>,

                    data.extra.company_pdf_presentation &&
                    <div key='compdf' className="props">
                        <Translate className="props__label" content="contacts.company_pdf_presentation" />
                        <a href={data.extra.company_pdf_presentation} target="_blank"
                           className="props__value text--red">{data.extra.company_pdf_presentation}</a>
                    </div>
                ]
                }
            </div>
        )

    }

}

export default Contacts
