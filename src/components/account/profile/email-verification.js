import React, {Component} from 'react'
import PropTypes from 'prop-types'
import 'whatwg-fetch'
import {Message} from 'semantic-ui-react'
import Translate from 'react-translate-component'

import {SvgClose} from '../../../svg/'
import {ApiURI} from '../../../libs/'
import {Spinner, Modal} from '../../helpers/'


class ModalEmailVerification extends Component {
    state = {
        loading: true,
        alert: null,
    };

    componentDidMount() {
        this.sentLink()
    }

    sentLink = () => {
        fetch(`${ApiURI.extraApi}/users/${this.props.login}/send_confirmation`)
            .then(() => {
                this.setState({loading: false})
            })
            .catch(error => this.setState({loading: false, alert: error.message}))
    };

    render() {
        const {loading, alert} = this.state;

        if (loading) {
            return <Spinner className="spinner--absolute"/>
        }

        if (alert) {
            return (
                <Modal toggleModal={this.props.toggleModal} className='modal--message'>
                    <Message className='message--warning one-line-block'>
                        <p>{alert}</p>
                        <button type="button" className="btn btn--link btn--red_text"
                                onClick={this.props.toggleModal}>
                            <SvgClose width={24} height={24}/>
                        </button>
                    </Message>
                </Modal>
            )
        }

        return (

            <Modal toggleModal={this.props.toggleModal} className='modal--message'>
                <Message className='message--success one-line-block'>
                    <Translate component='p' content="message_verify_email"/>
                    <button type="button" className="btn btn--link btn--red_text"
                            onClick={this.props.toggleModal}>
                        <SvgClose width={24} height={24}/>
                    </button>
                </Message>
            </Modal>
        )
    }

    static propTypes = {
        login: PropTypes.string.isRequired,
    }
}

export default ModalEmailVerification
