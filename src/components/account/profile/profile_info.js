import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import Translate from 'react-translate-component'
import { Avatar, Modal } from './../../helpers'
import ModalKeys from './modal-keys'
import {Icon} from 'semantic-ui-react';
import {CopyToClipboard} from 'react-copy-to-clipboard'

class Profile_info extends Component {

    constructor(props){
        super(props);
        this.state = {
            showModal: false,
        };
    }

    toggleModal = () => {
        if (this.state.showModal) {
            this.props.clearKey()
        }

        this.setState({ showModal: !this.state.showModal })
    };

    render() {
        const { user, self, fillIn } = this.props,
            showModal = this.state.showModal;

        return (
            <div className="personal__wrapper">
                <Avatar
                    img={user.extra ? user.extra.avatar.medium.url : null}
                    className="avatar--lg personal__avatar"
                    name={{
                        first_name: user.extra
                            ? user.extra.first_name
                            : user.name,
                        last_name: user.extra
                            ? user.extra.last_name
                            : ''
                    }}
                />

                <div className="personal__info">
                    <h5 className="personal__name heading--sm">
                        {user.extra
                            ? `${user.extra.first_name
                                    ? user.extra.first_name
                                    : user.name }
                                ${user.extra.last_name
                                    ? ` ${user.extra.last_name}`
                                    : ''}`
                            : user.name
                        }
                        {user.extra ? user.extra.confirmed && <Icon className='text--secondary personal__verify' name='shield' /> : ''}
                        <span className='personal__rating'>{parseFloat(user.karma).toFixed(2)}</span>
                    </h5>

                    <p className="personal__login">
                        {(user.extra && user.extra.first_name) && user.name}
                        <span className="personal__id">
                            {user.id}
                        </span>
                        {user.lifetime_referrer === user.id &&
                            <span className="personal__ltm">LTM</span>
                        }
                    </p>

                    {(user.extra && user.extra.email && self) &&
                        <a className='personal__email' href={`mailto:${user.extra.email}`}>{user.extra.email}</a>
                    }

                    {self &&
                        <div className='personal__referral-link'>
                            <CopyToClipboard text={window.location.origin + '/?r=' + user.name} onCopy={() => this.setState({copied: true})}>
                                <Translate component="button" className="btn btn--link text--secondary" content='profile.referral_link'/>
                            </CopyToClipboard>
                            <a href="https://karmared.atlassian.net/wiki/spaces/EN/pages/151158805/Referral+program" target='_blank'>
                                <Icon className='text--secondary ico--help' name='help circle outline' />
                            </a>
                            {this.state.copied && <Translate className='text--red' content='profile.link_copied'/>}
                        </div>
                    }

                    {user.extra && user.extra.facebook &&
                        <a href={`https://www.facebook.com/${user.extra.facebook}`} target="_blank" className="btn--link">Facebook</a>
                    }

                    {self &&
                        <button type="button" className="btn btn--red btn--small" onClick={this.toggleModal}>
                            <Translate content="btn.permissions" /></button>
                    }

                </div>

                {self &&
                    <div className="btn_wrapper">
                        <Link to={`/profile/${user.name}/edit`} className="btn btn--ghost btn--red_text">
                            <Translate content="btn.edit_profile" /></Link>
                        {fillIn
                            ? <Translate className='personal__hint' content="profile.profile_warning_full" />
                            : <Translate className='personal__hint text--red' content="profile.profile_warning_empty" />
                        }
                    </div>
                }

                {showModal &&
                    <Modal title={<Translate className='heading--sm' content="modal_key.title" />}
                           lg
                           toggleModal={this.toggleModal}>
                        <ModalKeys />
                    </Modal>
                }
            </div>
        )
    }
}

export default Profile_info;
