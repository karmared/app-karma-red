import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Translate from 'react-translate-component'
import { getUserData, getUserGateways } from '../../../reducers/karma/'
import { getPrvKey } from '../../../reducers/authorization/'
import { toggleRequestPassword, clearPrvKey, setGateways } from '../../../actions/'


class ModalKeys extends Component {
  componentDidMount() {
    this.props.loadGateways()
  }

  componentWillUnmount() {
    this.props.clearKey()
  }

  render() {
    const { user, gateways, prvKey, toggleModal, clearKey } = this.props;

    return [
        <Translate component="h5" key="pub" className="heading--sm" content="modal_key.public_key" />,
        <p key='pubKey' className='text--long'>{user.pubKey}</p>,
        <Translate component="h5" key="eth" className="heading--sm" content="modal_key.ether_gw" />,
        <p key='etherGW' className='text--long'>{gateways.ethGW}</p>,
        <p><strong><Translate key='minAmount' className='text--long' content="modal_key.min_amount" min={gateways.fees.minKRM / 100000} currency="KRM" /></strong></p>,
        <p><Translate key='fee' className='text--long' content="withdraw.fee" fee={gateways.fees.feeToKRM / 100000} currency="KRM" /></p>,
        <Translate component="h5" key="prv" className="heading--sm" content="modal_key.private_key" />,
        prvKey
          ? [
            <p key="prvKey" className='text--long'>{prvKey}</p>,
            <Translate component="button" key="hide" type="button" className="btn btn--small btn--red" onClick={clearKey} content="btn.hide" />,
          ]
          : <Translate component="button" key="show" type="button" className="btn btn--small btn--red" onClick={toggleModal} content="btn.show" />
    ]
  }

  static propTypes = {
    user: PropTypes.objectOf(PropTypes.any).isRequired,
    gateways: PropTypes.objectOf(PropTypes.any).isRequired,
    prvKey: PropTypes.string,
    toggleModal: PropTypes.func.isRequired,
    clearKey: PropTypes.func.isRequired,
  }

  static defaultProps = {
    prvKey: '',
  }
}

const mapStateToProps = state => ({
  user: getUserData(state),
  gateways: getUserGateways(state),
  prvKey: getPrvKey(state),
})

const mapDispatchToProps = dispatch => ({
  toggleModal: () => dispatch(toggleRequestPassword()),
  clearKey: () => dispatch(clearPrvKey()),
  loadGateways: () => dispatch(setGateways()),
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalKeys)
