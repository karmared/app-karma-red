import React from 'react';
import Translate from 'react-translate-component'
import PropTypes from "prop-types"
import {ChainTypes} from 'karmajs'
import {Karma, ApiURI} from '../../../libs/'
import {Spinner} from '../../helpers/'

const loadData = async (login) => {
    const queryParams = [
        0, // from_index
        100, // elements_count
        0, // loan_persent_from
        10000000, // loan_persent_to
        0, // deposit_persent_from
        10000000, // deposit_persent_to
        0, // loan_volume_from
        1000000000, // loan_volume_to
        '', // cyrrency_symbol
        login, // user login
        6, // status
    ];
    let deals = await Karma.dbApi.exec('fetch_credit_requests_stack', queryParams).then(e => e);
    const cred = await Karma.dbApi.exec('fetch_credit_requests_stack_by_creditor', queryParams).then(e => e);
    return deals.concat(cred)
};

class Deals_statistic extends React.Component {
    state = {
        statistic: {
            all: 0,
            success: 0,
            defaults: 0,
            borrower: 0,
            investor: 0
        },
        loading: false,
    };

    componentDidMount() {
        this.getData(this.props.data.name);
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.data.name !== nextProps.data.name){
            this.getData(nextProps.data.name);
        }
    }

    getData = (name) => {
        this.setState({loading: true});
        loadData(name).then(e => {
            this.statisticGenerate(this.props.data, e).then(e => {
                this.setState({
                    statistic: e,
                    loading: false
                })
            })
        });
    };

    statisticGenerate = async ( data, deals) => {
        let statistic = {
            all: 0,
            success: 0,
            defaults: 0,
            borrower: 0,
            investor: 0
        };

        deals.map(deal => {
            switch(deal.status){
                case "complete_normal":
                    statistic.success++;
                    break;
                case 'complete_ubnormal':
                    statistic.defaults++;
                    break;
            }
            if(deal.borrower.borrower === data.id){
                statistic.borrower++
            }
            if(deal.creditor.creditor === data.id){
                statistic.investor++
            }
        });
        statistic.all = deals.length;
        return statistic
    };

    render() {
        const {loading, statistic} = this.state;

        if(loading){
            return (
                <div className="statistic">
                    <Translate component='h4' className="heading--md" content="statistic.title" />
                    <Spinner className='spinner--absolute'/>
                </div>
            )
        }

        return (
            <div className="statistic">
                <Translate component='h4' className="heading--md" content="statistic.title" />
                <div className="statistic__item">
                    <Translate className="statistic__label heading--sm" content="statistic.all_deals" />
                    <span className="statistic__value">{ statistic.all }</span></div>
                <div className="statistic__item">
                    <Translate className="statistic__label heading--sm" content="statistic.an_investor" />
                    <span className="statistic__value">{ statistic.investor }</span></div>
                <div className="statistic__item">
                    <Translate className="statistic__label heading--sm" content="statistic.a_borrower" />
                    <span className="statistic__value">{ statistic.borrower }</span></div>
                <div className="statistic__item">
                    <Translate className="statistic__label heading--sm" content="statistic.successfully_returned" />
                    <span className="statistic__value">{ statistic.success }</span></div>
                <div className="statistic__item">
                    <Translate className="statistic__label heading--sm" content="statistic.defaults" />
                    <span className="statistic__value">{ statistic.defaults }</span></div>
                <div className="statistic__total">
                    <Translate className="statistic__label heading--sm" content="statistic.volume" />
                    <span className="statistic__value">
                        <span className='usd'>$</span> 0
                </span>
                </div>
            </div>
        )
    }

    static propTypes = {
        statistic: PropTypes.objectOf(PropTypes.any)
    }
}

export default Deals_statistic;
