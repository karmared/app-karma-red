import React, {Component} from 'react'
import PropTypes from 'prop-types'
import request from 'request-promise-native'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'

import Translate from 'react-translate-component'

import {setUser} from '../../../actions/'
import {getUserData} from '../../../reducers/karma/'
import {ApiURI} from '../../../libs/'

import {Spinner} from '../../helpers/'

import {Grid, Message} from 'semantic-ui-react';

const checker = async ({user, pathHash, setUserData}) => {
    const result = await request({
        method: 'POST',
        uri: `${ApiURI.extraApi}/users/${user.name}/${pathHash}`,
        json: {},
    })

    if (!result) {
        return {
            alert: 'Cannot check your url key. Please, try again.',
        }
    }

    if (!result.confirmed) {
        return {
            alert: 'Cannot verify this email. Please, check the confirmation link.',
        }
    }

    const {extra, ...restUserData} = user

    setUserData({...restUserData, extra: result})

    return {
        alert: null,
        success: true,
    }
}

class Confirm extends Component {
    state = {
        success: false,
        alert: null,
    }

    componentDidMount() {
        const {user, pathLogin, pathHash, setUserData} = this.props

        if (user) {
            if (user.name === pathLogin) {
                checker({user, pathHash, setUserData}).then(e => this.setState(e))
            }
        }
    }

    render() {
        const {user, pathLogin} = this.props
        const {success, alert} = this.state

        if (!user) {
            return (
                <section id="deals_page" className='content'>
                    <Grid celled className='karma__grid'>
                        <Grid.Row className='karma__wrapper'>
                            <Grid.Column className='karma__column'>
                                <div>
                                    <h3 className="heading--md">Checking your email...</h3>
                                    <Spinner className='spinner--absolute'/>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </section>
            )
        }

        if (user.name !== pathLogin) {
            return (
                <section id="deals_page" className='content'>
                    <Grid celled className='karma__grid'>
                        <Grid.Row className='karma__wrapper'>
                            <Grid.Column className='karma__column'>
                                <div>
                                    <h3 className="heading--md">Email confirmation</h3>
                                    <Message warning className='message--warning'>
                                        You are trying to confirm an email not related to your login.
                                    </Message>
                                    <Link to="/profile" className="btn btn--red"><Translate content="btn.back_to_profile" /></Link>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </section>
            )
        }

        return (
            <section id="deals_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper'>
                        <Grid.Column computer={4} tablet={16} className='karma__column'>
                            <div>
                                <h3 className="heading--md">Email confirmation</h3>
                                {alert &&
                                    <Message warning className='message--danger'>
                                        {alert}
                                    </Message>
                                }
                                {success &&
                                    <Message warning className='message--success'>
                                        Your email address has been successfully verified!
                                    </Message>
                                }
                                <Link to="/profile" className="btn btn--red"><Translate content="btn.back_to_profile" /></Link>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )
    }
}

Confirm.propTypes = {
    user: PropTypes.objectOf(PropTypes.any),
    pathHash: PropTypes.string,
    pathLogin: PropTypes.string,
    setUserData: PropTypes.func.isRequired,
}

Confirm.defaultProps = {
    user: null,
    pathHash: null,
    pathLogin: null,
}

const mapStateToProps = state => ({
    user: getUserData(state),
    pathLogin: state.routing.location.pathname.split('/')[2],
    pathHash: state.routing.location.pathname.split('/')[3],
})

const mapDispatchToProps = dispatch => ({
    setUserData: data => dispatch(setUser(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Confirm)
