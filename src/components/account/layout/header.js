import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import MediaQuery from 'react-responsive'
import Translate from 'react-translate-component'

import {getUserData, getUserBalance, getQuotations} from '../../../reducers/karma/'
import {logOut, loadBalance, loadQuotations} from '../../../actions/'

import {Logo} from '../../../svg/'
import {SvgWallet, SvgExit} from '../../../svg/'
import {Avatar, nFormatter} from '../../helpers/'

import {Icon} from 'semantic-ui-react'
import {CoreAsset} from '../../../libs'

class Header extends Component {
    qetQuotationsData = null;
    balanceUpdateTick = null;

    componentDidMount() {
        this.qetQuotationsData = setInterval(() => this.props.loadQuotations(), 5000);
        this.balanceUpdateTick = setInterval(() => this.props.loadBalance(), 5000);
    }

    componentWillUnmount() {
        clearInterval(this.balanceUpdateTick);
        clearInterval(this.qetQuotationsData);
    }

    render() {
        const {showNav, user, balance, quotations, actionLogOut, toggleNav} = this.props;
        const temp = quotations.map(item=> ({ ...item, crypto: item.symbol === CoreAsset || item.symbol === "BTC" || item.symbol === "ETH" }));
        const cryptoCur = temp.filter(item => item.crypto === true);
        const mainCur = temp.filter(item => item.crypto !== true);
        return (
            <header className='header'>
                <ul className="header__list">
                    <MediaQuery minWidth={768}>
                        {(matches) => {
                            if (matches) {
                                if (balance.length === 0) {
                                    return null
                                }

                                return [
                                    <li key='quotations' className='header__item--quotations'>
                                        <div className='quotations__wrapper'>
                                        {cryptoCur.map(item => (
                                                <p className='quotations__item' key={item.symbol}>
                                                    <span className="quotations__cur">{item.symbol}</span>
                                                    <span className="quotations__val">${parseFloat(item.price_usd).toFixed(3)}</span>
                                                    {/*<span className="quotations__val">${parseFloat(item.price_usd).toLocaleString('ru-RU').replace(',', '.')}</span>*/}
                                                    <Icon
                                                        className={`quotations__ico ${item.percent_change_24h > 0 ? 'plus' : 'minus'}`}
                                                        name={`caret ${item.percent_change_24h > 0 ? 'up' : 'down'}`}/>
                                                </p>
                                            )
                                        )}
                                        </div>
                                        <div className='quotations__wrapper'>
                                        {mainCur.map(item => (
                                                <p className='quotations__item' key={item.symbol}>
                                                    <span className="quotations__cur">{item.symbol}</span>
                                                    <span className="quotations__val">${parseFloat(item.price_usd).toFixed(3)}</span>
                                                    {/*<span className="quotations__val">${parseFloat(item.price_usd).toLocaleString('ru-RU').replace(',', '.')}</span>*/}
                                                    <Icon
                                                        className={`quotations__ico ${item.percent_change_24h > 0 ? 'plus' : 'minus'}`}
                                                        name={`caret ${item.percent_change_24h > 0 ? 'up' : 'down'}`}/>
                                                </p>
                                            )
                                        )}
                                        </div>
                                    </li>,
                                    <li key='ico' className={`header__item header__item--ico`}>
                                        <Link to="/wallet">
                                            <SvgWallet width={18} height={15} className='header__ico'/>
                                        </Link>
                                    </li>,
                                    balance.map(item => [
                                        <li key={item.id}
                                            className='header__item'>
                                            <Link to="/wallet" className='header__btn'>
                                                {nFormatter(item.amount)}&nbsp;{item.symbolPublic}
                                            </Link>
                                        </li>
                                    ])
                                ]
                            }
                            return null
                        }}
                    </MediaQuery>
                    <MediaQuery maxWidth={575}>
                        <li className="logo">
                            <Link to="/">
                                <Logo width={40} height={40} alt=""/>
                            </Link>
                        </li>
                    </MediaQuery>
                    <li className="header__item">
                        <Link to="/profile" className='header__link'>
                            <Avatar
                                img={user.extra ? user.extra.avatar.thumb.url : null}
                                className="avatar--sm"
                                name={{first_name: user.extra.first_name || user.name, last_name: user.extra.last_name}}
                            />
                            <MediaQuery minWidth={576}>
                                {`${user.extra.first_name ? user.extra.first_name : user.name }${user.extra.last_name ? ` ${user.extra.last_name}` : ''}`}
                            </MediaQuery>
                        </Link>
                    </li>
                    <MediaQuery minWidth={576}>
                        {(matches) => {
                            if (matches) {
                                return [
                                    <li className="header__item header__item--exit" key="exit">
                                        <button type="button" className='header__btn' onClick={actionLogOut}>
                                            <Translate content="btn.exit" />
                                            <SvgExit width={16} height={17} className="ico"/>
                                        </button>
                                    </li>,
                                ]
                            }

                            return (
                                <li className="header__item header__item--ico">
                                    <button type="button" onClick={toggleNav}
                                            className={`toggler btn--link ${showNav}`}>
                                        <span className="icon"/>
                                    </button>
                                </li>
                            )
                        }}
                    </MediaQuery>
                </ul>
            </header>
        )
    }

    static propTypes = {
        user: PropTypes.objectOf(PropTypes.any).isRequired,
        balance: PropTypes.arrayOf(PropTypes.any).isRequired,
        quotations: PropTypes.arrayOf(PropTypes.any).isRequired,
        actionLogOut: PropTypes.func.isRequired,
        loadBalance: PropTypes.func.isRequired,
        loadQuotations: PropTypes.func.isRequired
    }
}

const mapStateToProps = state => ({
    user: getUserData(state),
    balance: getUserBalance(state),
    quotations: getQuotations(state),
});

const mapDispatchToProps = dispatch => ({
    actionLogOut: () => dispatch(logOut()),
    loadBalance: () => dispatch(loadBalance()),
    loadQuotations: () => dispatch(loadQuotations())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header)
