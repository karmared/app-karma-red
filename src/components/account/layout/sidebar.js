import React, {Component} from 'react'
import MediaQuery from 'react-responsive'
import PropTypes from 'prop-types'
import { Link, NavLink } from 'react-router-dom'

import Translate from 'react-translate-component'

import {Logo} from '../../../svg/'
import {SvgDeals, SvgMarket, SvgProfile, SvgWallet, SvgExit, SvgVoting, SvgHelp} from '../../../svg/'
import { LanguageSelect } from '../../helpers'
import {logOut} from '../../../actions'
import {connect} from 'react-redux'

class Sidebar extends Component {

    render() {
        const {isOpen, actionLogOut, toggleNav } = this.props

        if (isOpen && window.innerWidth < 576) {
            const scrollWidth = window.innerWidth - document.body.clientWidth;

            document.body.setAttribute('style', `overflow: hidden; padding-right: ${scrollWidth}px`)
        }
        else {
            document.body.removeAttribute('style')
        }
        return (
            <ul className={`sidebar__menu ${isOpen}`}>
                <MediaQuery minWidth={576}>
                    <li className="sidebar__item sidebar__item--logo">
                        <Link to="/">
                            <Logo width={40} height={40} alt=""/>
                        </Link>
                    </li>
                </MediaQuery>
                <li className='sidebar__item'>
                    <NavLink to="/profile" className="sidebar__link" activeClassName="active" onClick={toggleNav}>
                        <SvgProfile width={15} height={17} className='sidebar__ico'/>
                        <Translate content="sidebar.profile"/>
                    </NavLink>
                </li>
                <li className='sidebar__item'>
                    <NavLink to="/deals" className="sidebar__link" activeClassName="active" onClick={toggleNav}>
                        <SvgDeals width={17} height={17} className='sidebar__ico'/>
                        <Translate content="sidebar.deals"/>
                    </NavLink>
                </li>
                <li className='sidebar__item'>
                    <NavLink to="/market" className="sidebar__link" activeClassName="active" onClick={toggleNav}>
                        <SvgMarket width={15} height={17} className='sidebar__ico'/>
                        <Translate content="sidebar.market"/>
                    </NavLink>
                </li>
                <li className='sidebar__item'>
                    <NavLink to="/wallet" className="sidebar__link" activeClassName="active" onClick={toggleNav}>
                        <SvgWallet width={18} height={15} className='sidebar__ico'/>
                        <Translate content="sidebar.wallet"/>
                    </NavLink>
                </li>
                <li className='sidebar__item'>
                    <NavLink to="/voting" className="sidebar__link" activeClassName="active" onClick={toggleNav}>
                        <SvgVoting width={20} height={20} className='sidebar__ico'/>
                        <Translate content="sidebar.voting"/>
                    </NavLink>
                </li>
                <li className='sidebar__item'>
                    <NavLink to="/help" className="sidebar__link" activeClassName="active" onClick={toggleNav}>
                        <SvgHelp width={22} height={22} className='sidebar__ico'/>
                        <Translate content="sidebar.help"/>
                    </NavLink>
                </li>
                <li className='sidebar__item  sidebar__item--lang'>
                    <LanguageSelect/>
                </li>
                <MediaQuery maxWidth={575}>
                    <li className='sidebar__item'>
                        <button type="button" className="sidebar__item--exit btn--link" onClick={actionLogOut}>
                            <Translate content="btn.exit"/>
                            <SvgExit width={16} height={17} className="exit-ico"/>
                        </button>
                    </li>
                </MediaQuery>
            </ul>
        )
    }
    static propTypes = {
        isOpen: PropTypes.bool,
        handleLogout: PropTypes.func,
        toggleNav: PropTypes.func,
    }

    static defaultProps = {
        isOpen: true,
        handleLogout: null,
        toggleNav: null,
    }
}

const mapDispatchToProps = dispatch => ({
    actionLogOut: () => dispatch(logOut())
});


export default connect(null, mapDispatchToProps)(Sidebar)
