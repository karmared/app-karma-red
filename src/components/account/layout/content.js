import React from 'react';
import ReactDOM from 'react-dom';

class Content extends React.Component {
    render() {
        return (
            <section className='content'>
                {this.props.children}
            </section>
        )
    }
}

export default Content;
