import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import {getGlobal} from '../../../reducers/karma/'
import {loadGlobal} from '../../../actions/'

import Header from './header';
import Sidebar from './sidebar';

class Layout extends Component {
    constructor(props){
        super(props);
        this.state = {
            showNav: false
        };
    }

    componentDidMount() {
        this.props.loadGlobal();
    }

    toggleNav = () => {
        this.setState({ showNav: !this.state.showNav })
    }

    render(){
        const { showNav } = this.state;
        const {children} = this.props;
        return (
            <main id="main_container">
                <Sidebar isOpen={showNav} toggleNav={this.toggleNav}/>
                <Header toggleNav={this.toggleNav} showNav={showNav}/>
                {children}
            </main>
        )
    }

    static propTypes = {
        loadGlobal: PropTypes.func.isRequired
    }
}

const mapStateToProps = state => ({
    global: getGlobal(state)
});

const mapDispatchToProps = dispatch => ({
    loadGlobal: () => dispatch(loadGlobal())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout)
