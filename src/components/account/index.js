import React from 'react'
import {Route, Switch} from 'react-router-dom'

import Page404 from '../page404'

import Layout from './layout/layout'

import Profile from './profile/'
import ProfileConfirm from './profile/confirm'
import ProfileEdit from './profile/edit'

import Deals from './deals/'
import DealDetails from './detail_deal/'

import Market from './market/'
import NewAsk from './market/new-ask'
import NewContract from './market/new-contract'

import Help from './help/'
import Voting from './voting/'
import Wallet from './wallet/'
import WalletAdd from './wallet/add'
import WalletWithdraw from './wallet/withdraw'
import WalletTransfer from './wallet/transfer'


const Account = () => (
    <Layout>
        <Switch>
            <Route exact path='/' component={Profile}/>

            <Route path='/deals/:uuid' component={DealDetails}/>
            <Route path='/deals' component={Deals}/>

            <Route exact path='/profile/:login/edit' component={ProfileEdit}/>
            <Route path='/profile/:login/:hash' component={ProfileConfirm}/>
            <Route path='/profile/:login' component={Profile}/>
            <Route path='/profile' component={Profile}/>

            <Route exact path='/market/new' component={NewAsk} />
            <Route exact path='/market/new_contract' component={NewContract} />
            <Route path='/market/:uuid' component={DealDetails}/>
            <Route path='/market' component={Market}/>

            <Route exact path='/voting' component={Voting}/>
            <Route path='/help' component={Help}/>

            <Route exact path='/wallet' component={Wallet}/>
            <Route path='/wallet/add/:currency' component={WalletAdd}/>
            <Route path='/wallet/withdraw/:currency' component={WalletWithdraw}/>
            <Route path='/wallet/transfer/:currency' component={WalletTransfer}/>

            <Route component={Page404}/>
        </Switch>
    </Layout>
);

export default Account
