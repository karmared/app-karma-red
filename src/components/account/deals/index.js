import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Translate from 'react-translate-component'
import ReactGA from 'react-ga'

import {Karma} from '../../../libs/karma'
import {getUserData} from '../../../reducers/karma/'
import {Spinner} from '../../helpers/'
import Item from './list-item'

import {Grid, Message, Dropdown} from 'semantic-ui-react'


const loadData = async (user) => {
    const queryParams = [
        0, // from_index
        100, // elements_count
        0, // loan_persent_from
        10000000, // loan_persent_to
        0, // deposit_persent_from
        10000000, // deposit_persent_to
        0, // loan_volume_from
        1000000000, // loan_volume_to
        '', // cyrrency_symbol
        user.name, // user login
        6, // status
    ];

    const allDeals = await Karma.dbApi.exec('fetch_credit_requests_stack', queryParams).then(e => e.map(item => {item._own = true; return item;}));
    const allDealsByCreditor = await Karma.dbApi.exec('fetch_credit_requests_stack_by_creditor', queryParams).then(e => e.map(item => {item._own = true; return item;}));

    const queryParams_all = [
        0, // from_index
        100, // elements_count
        0, // loan_persent_from
        10000000, // loan_persent_to
        0, // deposit_persent_from
        10000000, // deposit_persent_to
        0, // loan_volume_from
        1000000000, // loan_volume_to
        '', // cyrrency_symbol
        '', // user login
        6, // status
    ];

    const allDeals_2 = await Karma.dbApi.exec('fetch_credit_requests_stack', queryParams_all)
        .then( e => e.filter( item =>
                item.borrower.borrower !== user.id && 
                item.status !== 'wating_for_acceptance' &&
                item.creditor !== null && item.creditor !== (void 0) &&
                item.creditor.creditor !== user.id
        ).map(item => {item._own = false; return item;}));

    return {
        allDeals: allDeals.concat(allDealsByCreditor).concat(allDeals_2),
        loading: false,
    }
};

class Deals extends Component {
    state = {
        allDeals: [],
        filterAsk: 'all_my_asks',
        loading: true,
    };

    componentDidMount(){
        ReactGA.pageview(window.location.pathname + window.location.search);
        this.getData()
    }

    handleInput = (state, value) => {
        this.setState({
            [state]: value,
        })
    };

    getData = () => {
        loadData(this.props.user).then(e => this.setState(e))
    };

    render() {
        const {allDeals, loading, filterAsk} = this.state;

        let filterOptions = ['', ...new Set(allDeals.map(item => item.status))];

        filterOptions.push('all_my_asks');
        filterOptions.push('my_asks');
        filterOptions.push('approved_asks');

        console.log(filterOptions);

        filterOptions = filterOptions.map(item => {
            switch (item) {
                case 'wating_for_acceptance':   return {key: 1, value: item, text: <Translate content="deals.status_opened" />};
                case 'in_progress':             return {key: 2, value: item, text: <Translate content="deals.status_processed" />};
                case 'cancalled':               return {key: 3, value: item, text: <Translate content="deals.status_cancelled" />};
                case 'complete_normal':         return {key: 4, value: item, text: <Translate content="deals.status_completed" />};
                case 'complete_ubnormal':       return {key: 5, value: item, text: <Translate content="deals.status_defaults" />};
                case 'all_my_asks':             return {key: 6, value: item, text: <Translate content="deals.status_all_self" />};
                case 'my_asks':                 return {key: 7, value: item, text: <Translate content="deals.status_self" />};
                case 'approved_asks':           return {key: 8, value: item, text: <Translate content="deals.status_approved" />};
                default:                        return {key: 0, value: item, text: <Translate content="deals.status_all" />};
            }
        });

        let renderData = allDeals.map(item => <Item key={item.object_uuid} offer={item} ref={item.status} className={item.status}/>);

        if (filterAsk !== '' && filterAsk !== 'all_my_asks' && filterAsk !== 'my_asks' && filterAsk !== 'approved_asks' ) {
            renderData = renderData.filter(item => item.props.className === filterAsk);
        }
        else if (filterAsk === 'all_my_asks') {
            renderData = renderData.filter(item => item.props.offer._own);
        }
        else if (filterAsk === 'my_asks') {
            renderData = renderData.filter(item => item.props.offer._own && item.props.offer.borrower.borrower === this.props.user.id);
        }
        else if (filterAsk === 'approved_asks') {
            renderData = renderData.filter(item => item.props.offer._own && item.props.offer.borrower.borrower !== this.props.user.id);
        }

        if (loading) {
            return (
                <section id="deals_page" className='content'>
                    <Spinner className='spinner--absolute'/>
                </section>
            )
        } else {
            if (allDeals.length === 0) {
                return (
                    <section id="deals_page" className='content'>
                        <Grid celled className='karma__grid'>
                            <Grid.Row className='karma__wrapper'>
                                <Grid.Column className='karma__column'>
                                    <div>
                                        <Message key="dealsSection" warning className='message--warning'>
                                            <Translate component="p" content="deals.empty_list_deals" />
                                        </Message>
                                    </div>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </section>
                )
            } else {
                return (
                    <section id="deals_page" className='content'>
                        <Grid celled className='karma__grid'>
                            <Grid.Row className='karma__wrapper'>
                                <Grid.Column className='karma__column karma__column--scroll'>
                                    <div className="deals">
                                        <div className="deal__filter">
                                            <Translate component="h3" className="heading--md deal__title" content="deals.title" />
                                            <Dropdown
                                                fluid search selection required
                                                name="ask filter"
                                                placeholder={<Translate content="deals.status_all" />}
                                                value={filterAsk}
                                                onChange={(e, {value}) => this.handleInput('filterAsk', value)}
                                                className='karma__input deal__input'
                                                options={filterOptions}
                                            />
                                        </div>
                                        <div className='deal__container'>
                                            {renderData}
                                        </div>
                                    </div>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </section>
                )
            }
        }
    }

    static propTypes = {
        user: PropTypes.objectOf(PropTypes.any).isRequired,
    }
}

const mapStateToProps = state => ({
    user: getUserData(state),
})

export default connect(mapStateToProps)(Deals)
