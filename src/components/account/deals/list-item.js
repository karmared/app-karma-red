import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import 'whatwg-fetch'
import Translate from 'react-translate-component'
import { Card, Icon } from 'semantic-ui-react'

import {getCurrencies, getQuotations, getUserData} from '../../../reducers/karma/'
import {Karma, KarmaMultiplier, ApiURI, CoreAsset} from '../../../libs/'
import {nFormatter, Spinner, Avatar} from '../../helpers/'

const getData = async (id) => {
    const borrower = await Karma.dbApi.exec('get_accounts', [[id]]).then(e => e[0]);

    if (!borrower) {
        return {
            alert: "Cannot get borrower's information",
        }
    }

    const extra = await fetch(`${ApiURI.extraApi}/users/${borrower.name}`).then(e => e.json());

    if (!extra) {
        return {
            alert: "Cannot get borrower's extra data",
            borrower
        }
    }

    return {
        borrower: {...borrower, extra},
        alert: null,
    }
};

class DealsItem extends Component {
    state = {
        borrower: null,
        alert: '',
    };

    componentDidMount() {
        const {user, offer} = this.props
        user.id === offer.borrower.borrower
            ? this.setState({borrower: user})
            : getData(offer.borrower.borrower).then(e => this.setState(e))
    }

    render() {
        const { borrower, alert } = this.state;
        const { offer, className, currencies, quotations, user } = this.props;

        if (alert) {
            return (
                <div className='deal__wrapper'>
                    <div className='deal__name text--red'>{alert}</div>
                </div>
            )
        }

        if (!borrower) {
            return (
                <div className='deal__wrapper'>
                    <div className="deal__card">
                        <Spinner className='spinner--absolute'/>
                    </div>
                </div>
            )
        }

        let ico = '';
        switch (className) {
            case 'wating_for_acceptance':
                ico = <Icon className='ico' name='time' />;
                break;
            case 'in_progress':
                ico = <Icon className='ico' name='dollar'/>;
                break;
            case 'cancalled':
                ico = <Icon className='ico' name='remove circle outline'/>;
                break;
            case 'complete_normal':
                ico = <Icon className='ico' name='check circle outline'/>;
                break;
            case 'complete_ubnormal':
                ico = <Icon className='ico' name='close'/>;
                break;
        }

        let asset = currencies.filter(e => e.id === offer.borrower.loan_asset.asset_id)[0];
        let assetDeposit = currencies.filter(e => e.id === offer.borrower.deposit_asset.asset_id)[0];

        let symbolL = asset.symbol !== CoreAsset ? asset.symbol.substr(1) : asset.symbol;
        let symbolD = assetDeposit.symbol !== CoreAsset ? assetDeposit.symbol.substr(1) : assetDeposit.symbol;

        if(quotations.length > 0){
            let coincidenceL = quotations.filter(e => e.symbol === symbolL),
                coincidenceD = quotations.filter(e => e.symbol === symbolD);
            asset = {
                ...asset,
                price_usd: coincidenceL.length > 0
                    ? coincidenceL[0].price_usd
                    : 1
            }
            assetDeposit = {
                ...assetDeposit,
                price_usd: coincidenceD.length > 0
                    ? coincidenceD[0].price_usd
                    : 1
            }
        }

        const precisionDeposit = 10**assetDeposit.precision;
        const precisionLoan = 10**asset.precision;
        const amountL = offer.borrower.loan_asset.amount / precisionLoan;
        const amountD = offer.borrower.deposit_asset.amount / precisionDeposit;

        let collateral = quotations.length > 0 ? amountD / (amountL / assetDeposit.price_usd * asset.price_usd ) * 100 : amountD / amountL * 100;

        collateral = Math.round(collateral * 100) / 100;
        if (collateral <= 100) collateral = <Translate content="CF" />;
        else collateral += '%';

        const date = (new Date(offer.request_creation_time)).toLocaleString('en-US', {
            hour12: false, month: '2-digit', day: '2-digit', year: '2-digit',
        });

        return (
            <Link className='deal__wrapper' to={`/deals/${offer.object_uuid}`}>
                <Card className={`deal__card ${className ? className : ''}`}>
                    <Card.Content className='deal__content'>
                        {ico}
                        <Avatar
                            img={borrower.extra ? borrower.extra.avatar.url : null}
                            className="avatar--sm"
                            name={ {first_name: borrower.name} }
                        />
                        <div className="deal__info">
                            <h5 className='deal__name heading--sm'>{borrower.name}</h5>
                            <p className="deal__text text--secondary">
                                {date}
                                <br/>
                                {offer.borrower.loan_period}&nbsp;<Translate content="mth" /> |&nbsp;
                                {parseFloat(offer.borrower.loan_persent)}% |&nbsp;
                                {collateral}
                            </p>
                        </div>
                        <div className="deal__sum">
                            {nFormatter(amountL)}
                            &nbsp;
                            {symbolL}
                        </div>
                    </Card.Content>
                </Card>
            </Link>
        )
    }

    static propTypes = {
        offer: PropTypes.objectOf(PropTypes.any).isRequired,
        currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
    }
}

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    quotations: getQuotations(state),
    user: getUserData(state),
});

export default connect(mapStateToProps)(DealsItem)
