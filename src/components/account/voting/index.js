import React, {Component} from 'react'
import {connect} from 'react-redux'
import Translate from 'react-translate-component'
import {Dropdown, Checkbox} from 'semantic-ui-react'
import {Spinner, Karma_input, Modal} from '../../helpers/'
import {Karma} from '../../../libs/'
import VotingList from './voting_list'
import {getCurrencies, getGlobal, getUserBalance, getUserData} from '../../../reducers/karma'
import {voteMembers, getCommittee, getWitnesses} from '../../../actions'
import {CoreAsset} from '../../../libs'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import VotingLink from "./voting_link"

const loadData = async (filter, votes = [], global) => {
    /*requests*/
    const witness = await getWitnesses(votes, global.active_witnesses).then(e => e)
    const committee = await getCommittee(votes, global.active_committee_members).then(e => e)

    return {
        list: filter === '' ? witness.allWitness : committee.allCommittee,
        witness: witness.witness,
        committee: committee.committee,
        loading: false,
    }
}

const getProxyAccount = async (id) => {
    const proxyAccount = await Karma.dbApi.exec('get_accounts', [[id]]).then(e => e)
    return {
        id: proxyAccount[0].id,
        name: proxyAccount[0].name,
        votes: proxyAccount[0].options.votes
    }
}

const getProxyAccountByName = async (name) => {
    const proxyAccount = await Karma.dbApi.exec('get_account_by_name', [name]).then(e => e)
    return {
        id: proxyAccount.id,
        name: proxyAccount.name,
        votes: proxyAccount.options.votes
    }
}

const sortData = (array, prop, floatPoint) => array
    .sort((prev, next) => {
        const prevVal = prev[prop]/floatPoint,
            nextVal = next[prop]/floatPoint;
        return (prevVal > nextVal) ? -1 : (nextVal > prevVal) ? 1 : 0 })
    .map(item => ({...item, weight: item[prop]/floatPoint}));

const filterOptions = [
    {key: 1, value: '', text: <Translate content='voting.witnesses' />},
    {key: 2, value: 'committee', text: <Translate content='voting.committee' />},
    // {key: 3, value: 'workers', text: <Translate content='voting.workers' />},
]

class Voting extends Component {
    state = {
        list: [],
        loading: false,
        empty: false,
        alert: false,
        filter: '',
        witness: [],
        committee: [],
        proxy: {},
        tempProxyLogin: '',
        proxyCheck: false,
        showModal: false
    }

    componentDidMount() {
        this.getData('')
    }

    getData = (filter) => {
        const { userData, global, currencies } = this.props;

        const floatPoint = 10**currencies.filter(cur =>  cur.symbol === CoreAsset )[0].precision;

        if (userData.options.voting_account === userData.id) {
            loadData(filter, userData.options.votes, global).then(e => {
                e.list = sortData(e.list, 'total_votes', floatPoint)
                this.setState(e)
            })
        }
        else {
            getProxyAccount(userData.options.voting_account).then(e => {
                this.setState({
                    proxy: {
                        id: e.id,
                        name: e.name,
                        votes: e.votes
                    },
                    tempProxyLogin: e.name,
                    proxyCheck: true
                })
                loadData(filter, e.votes, global).then(e => {
                    e.list = sortData(e.list, 'total_votes', floatPoint)
                    this.setState(e)
                })
            })
        }
    }

    handleFilter = (value) => {
        const { currencies } = this.props;
        const floatPoint = 10**currencies.filter(cur =>  cur.symbol === CoreAsset )[0].precision;
        this.setState({filter: value})
        if (value === 'committee') {
            const votes = this.state.committee.map(com => com.vote_id)
            getCommittee(votes, this.props.global.active_committee_members)
                .then(e => this.setState({
                    committee: e.committee,
                    list: sortData(e.allCommittee, 'total_votes', floatPoint)
                }))
        }
        else {
            const votes = this.state.witness.map(com => com.vote_id)
            getWitnesses(votes, this.props.global.active_witnesses)
                .then(e => this.setState({
                    witness: e.witness,
                    list: sortData(e.allWitness, 'total_votes', floatPoint)
                }))
        }
    }

    handleProxy = () => {
        this.setState({proxyCheck: !this.state.proxyCheck})
    }

    handleVoting = (vote_id, member, approve) => {
        const {filter} = this.state
        let witness = this.state.witness
        let committee = this.state.committee
        switch (filter) {
            case '':
                if (approve) witness.push({vote_id, member, approve})
                else witness = witness.filter(item => item.vote_id !== vote_id)
                break;
            case 'committee':
                if (approve) committee.push({vote_id, member, approve})
                else committee = committee.filter(item => item.vote_id !== vote_id)
                break;
        }
        this.setState({witness, committee})
    }

    changeProxy = (e) => {
        this.setState({tempProxyLogin: e.target.value})
    }

    toggleModal = () => {
        const {currencies, global, balance} = this.props

        let coreAsset = currencies.filter(e => e.symbol === CoreAsset)[0];
        let fee = global.operations.filter(e => e.operation === 'account_update')[0].fee / 10**coreAsset.precision;

        let coreAmount = balance.filter(e => e.symbol === CoreAsset)[0].amount;
        if (fee > coreAmount) {
            return this.setState({
                loading: false,
                alert: <Translate component="p" className='heading--sm text--red' content="voting.need_asset"/>,
            })

        }

        this.setState({showModal: !this.state.showModal, alert: false})
    }

    saveChanges = () => {
        this.setState({loading: true})

        const {userData} = this.props

        const {witness, committee, proxyCheck, tempProxyLogin, filter} = this.state
        const members = [...witness, ...committee]

        const votes = members.map(e => e.vote_id)

        let proxy = this.state.proxy

        if (proxyCheck) {

            if (tempProxyLogin !== proxy.name) {
                getProxyAccountByName(tempProxyLogin)
                    .then(e => {
                        proxy = {
                            id: e.id,
                            name: e.name,
                            votes: e.votes
                        }
                        loadData(filter, proxy.votes)
                            .then(e => this.setState(e))
                        this.props.voteMembers({votes: proxy.votes, id: proxy.id})
                            .then(() => {
                                this.setState({showModal: false, loading: false, proxy})
                            })
                    })
            }
            else {
                this.props.voteMembers({votes: proxy.votes, id: proxy.id})
                    .then(() => {
                        this.setState({showModal: false, loading: false})
                    })
            }

        }
        else {
            this.props.voteMembers({votes, id: userData.id})
                .then(() => {
                    this.setState({showModal: false, loading: false})
                })
        }


    }

    render() {
        const {loading, filter, witness, committee, list, showModal, proxyCheck, tempProxyLogin, alert} = this.state;

        const { global, currencies } = this.props;

        const floatPoint = 10**currencies.filter(cur =>  cur.symbol === CoreAsset )[0].precision;

        return (
            <section id="voting_page" className='content'>
                <div className="voting__header">
                    <Translate component="h4" className="heading--md" content='voting.title' />
                    <VotingLink />

                    <div className="voting__proxy">
                        <Checkbox className='karma__checkbox voting__checkbox text--main'
                                  label={<Translate component='label' content='voting.label_proxy' />}
                                  checked={proxyCheck} onChange={this.handleProxy}/>
                        {proxyCheck &&
                        <Karma_input className='karma__input voting__input'
                                     classWrapper='voting__field'
                                     placeHolder={<Translate className='placeholder' content='auth.placeholder_login' />}
                                     onChange={this.changeProxy}
                                     defaultValue={tempProxyLogin}
                                     value={tempProxyLogin}/>
                        }
                    </div>
                    <div className="voting__filter">
                        <Dropdown fluid search selection required name="ask filter"
                            placeholder={<Translate content='voting.witnesses' />}
                            value={filter}
                            onChange={(e, {value}) => this.handleFilter(value)}
                            className='karma__input voting__input'
                            options={filterOptions}
                        />
                    </div>
                </div>
                <VotingList proxy={proxyCheck} data={list} filter={filter} members={filter === '' ? witness : committee}
                    votes={filter === '' ? witness.map(user => user.vote_id) : committee.map(user => user.vote_id)}
                    handleVoting={this.handleVoting}/>
                <div className="btn-panel">
                    {alert}
                    {loading
                        ? <span className={`btn btn--red btn--disabled loading`}><Spinner className='spinner--inside'/></span>
                        : <Translate component='button' type="button" onClick={this.toggleModal} className={`btn btn--red`} content='btn.save_changes' />
                    }
                </div>
                {showModal &&
                    <Modal
                        title={<Translate className="heading--sm" content='modal_voting.title' />}
                        className='voting__modal'
                        toggleModal={this.toggleModal}
                        footerActions={[
                            <Translate key='cancel' component='button' type="button" disabled={loading} onClick={this.toggleModal} className={`btn btn--ghost ${loading ? 'loading' : ''}`} content='btn.cancel' />,
                            loading
                                ? <span key='save' className={`btn btn--red loading btn--disabled`}><Spinner className='spinner--inside'/></span>
                                : <Translate key='save' component='button' type="button" className={`btn btn--red`} onClick={this.saveChanges} content={`btn.${proxyCheck ? 'agree' : 'save_changes'}`} />
                        ]}>
                        <div>
                            {proxyCheck
                                ? <div>
                                    <Translate component="p" content='modal_voting.sure_proxy' with={{ proxy: <b>{tempProxyLogin}</b> }}/>
                                    <Translate component="p" content='modal_voting.change_proxy_content'/>
                                </div>
                                : <div>
                                    {witness.length === 0 && committee.length === 0
                                        ? <Translate component="p" content='modal_voting.no_change_votes_content'/>
                                        : [
                                            <Translate component="p" content='modal_voting.change_votes_content'/>,
                                            witness.length > 0 &&
                                            <div>
                                                <Translate content='voting.witnesses'/>:
                                                <ul> {witness.map(item => <li
                                                    key={item.vote_id}>{item.member}</li>)} </ul>
                                            </div>,
                                            committee.length > 0 &&
                                            <div>
                                                <Translate content='voting.committee' />:
                                                <ul> {committee.map(item => <li key={item.vote_id}>{item.member}</li>)} </ul>
                                            </div>
                                        ]
                                    }
                                </div>
                            }
                            <Translate component='p' className='text--main' content='transfer.fee' sum={global.parameters.current_fees ? global.parameters.current_fees.parameters[6][1].fee/floatPoint : '0.1'} memo={CoreAsset} />
                        </div>
                    </Modal>
                }
            </section>
        )
    }

    static propTypes = {
        global: PropTypes.objectOf(PropTypes.any).isRequired,
    }
}

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    userData: getUserData(state),
    global: getGlobal(state),
    balance: getUserBalance(state)
})

const mapDispatchToProps = dispatch => ({
    voteMembers: data => dispatch(voteMembers(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Voting)
