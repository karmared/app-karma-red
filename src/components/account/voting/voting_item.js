import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import MediaQuery from 'react-responsive'
import 'whatwg-fetch'

import {nFormatter, Spinner, Avatar} from '../../helpers/'

import {Table, Checkbox, Icon} from 'semantic-ui-react'
import Translate from 'react-translate-component'

class VotingItem extends Component {
    state = {
        vote: false
    };

    componentDidMount() {
        this.setState({vote: this.props.defaultChecked})
    }

    handleVote = () => {
        this.setState({vote: !this.state.vote});
        this.props.changeVote(this.props.data.vote_id, this.props.data.name, !this.state.vote)
    };

    render() {
        const {data, proxy, index, defaultChecked} = this.props;

        const urlCheck = /^(ftp|http|https):\/\/[^ "]+$/.test(data.url);

        return (
            <MediaQuery minWidth={810}>
                {(matches) => {
                    if (matches) {
                        return (
                            <div className="voting__table-item">
                                <span className='voting__table-cell num'>{index}</span>
                                <span className='voting__table-cell name'>
                                    <Link to={`/profile/${data.name}`}>
                                        <Avatar img={null} className="avatar--sm" name={{first_name: data.name}}/>
                                            {data.name}
                                    </Link>
                                </span>
                                <span className='voting__table-cell about'>
                                    {data.url !== '' && urlCheck
                                        ? <Translate component='a' href={data.url} className='btn btn--red btn--small' content="btn.link" />
                                        : <Translate className='btn btn--red btn--small btn--disabled' content="btn.link" />}
                                </span>
                                {/*<span className='voting__table-cell votes'>{data.weight}</span>*/}
                                <span className='voting__table-cell votes'>{Math.floor(data.weight).toLocaleString('ru-RU')}</span>
                                <span className='voting__table-cell status'>
                                    <Translate content={`voting.status_${data.status ? 'active' : 'backup'}`} />
                                </span>
                                <span className='voting__table-cell support'>
                                    <Translate component={defaultChecked ? 'b' : 'span'} content={`voting.supported_${defaultChecked ? 'yes' : 'no'}`} />
                                </span>
                                <span className='voting__table-cell toggle_vote'>
                                    {proxy
                                        ? <Icon name='lock' className='proxy__lock'/>
                                        : <Checkbox className='karma__checkbox voting__checkbox'
                                                    defaultChecked={defaultChecked}
                                                    onChange={this.handleVote}/>
                                    }
                                </span>
                            </div>
                        )
                    }


                    return (
                        <div className="item item--market">
                            <Link to={`/profile/${data.name}`} className="item__prop item__prop--name">
                                <Avatar img={null} className="avatar--sm" name={{first_name: data.name}}/>
                                <span className="heading--sm">{data.name}</span>
                            </Link>
                            <div className="item__prop">
                                <Translate className="item__label" content='voting.table_about' />
                                <span><a href={data.url} className='btn btn--red btn--small'>Link</a></span>
                            </div>
                            <div className="item__prop">
                                <Translate className="item__label" content='voting.table_votes' />
                                <span className="item__value">{data.weight}</span>
                            </div>
                            <div className="item__prop">
                                <Translate className="item__label" content='voting.table_status' />
                                <Translate className="item__value" content={`voting.status_${data.status ? 'active' : 'backup'}`} />
                            </div>
                            <div className="item__prop">
                                <Translate className="item__label" content='voting.table_supported' />
                                <Translate className="item__value" content={`voting.supported_${this.state.vote ? 'yes' : 'no'}`} />
                            </div>
                            <div className="item__prop">
                                {proxy
                                    ? <Icon name='lock' className='proxy__lock'/>
                                    : <Translate component="button" className='btn btn--visit' onChange={this.handleVote} content='voting.btn_voting' />
                                }
                            </div>
                        </div>
                    )
                }}
            </MediaQuery>

        )
    }
}

export default VotingItem
