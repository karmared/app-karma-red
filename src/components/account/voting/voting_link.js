import React from "react"
import Translate from 'react-translate-component'


const render = props => {
  return (
    <div className="text--main" style={{ marginLeft: "1em" }}>
      (
        <a href="https://karma.consider.it" target="_blank" className="personal__email">
          <Translate content='voting.public_voting_link' />
        </a>
      )
    </div>
  )
}


export default render
