import React from 'react'
import MediaQuery from 'react-responsive'
import Translate from 'react-translate-component'
import VotingItem from './voting_item'
import {Spinner} from '../../helpers/'

class VotingList extends React.Component {

    changeVote = (id, name, vote) => {
        this.props.handleVoting(id, name, vote);
        this.props.data.map(item => {
           if(id === item.vote_id){
               item.toggleVote = !vote;
           }
        });
    };

    render() {
        const { proxy, votes, data } = this.props;
        if(!data){
            return (
                <div className="voting__list">
                    <Spinner className="spinner--absolute"/>
                </div>
            )
        }

        return (
            <MediaQuery minWidth={810}>
                {(matches) => {
                    if (matches) {
                        return (

                            <div className='voting__table'>
                                <div className="voting__table-header">
                                    <span className='voting__table-cell num'>#</span>
                                    <span className='voting__table-cell name'>
                                        <Translate content='voting.table_name' />
                                    </span>
                                    <span className='voting__table-cell about'>
                                        <Translate content='voting.table_about' />
                                    </span>
                                    <span className='voting__table-cell votes'>
                                        <Translate content='voting.table_votes' />
                                    </span>
                                    <span className='voting__table-cell status'>
                                        <Translate content='voting.table_status' />
                                    </span>
                                    <span className='voting__table-cell support'>
                                        <Translate content='voting.table_supported' />
                                    </span>
                                    <span className='voting__table-cell toggle_vote'>
                                        <Translate content='voting.table_toggle' />
                                    </span>
                                </div>
                                <div className="voting__table-body">
                                    {data.map((item, index) => <VotingItem
                                        key={item.id}
                                        index={index}
                                        data={item}
                                        proxy={proxy}
                                        defaultChecked={votes.filter(e => e === item.vote_id ).length > 0}
                                        changeVote={this.changeVote}/>)}
                                </div>
                            </div>
                        )
                    }

                    return (
                        <div key='marketList' className="voting__list">
                            {data.map((item, index) => <VotingItem
                                key={item.id}
                                index={index}
                                data={item}
                                proxy={proxy}
                                changeVote={this.changeVote}/>)}
                        </div>
                    )
                }}
            </MediaQuery>

        )
    }
}

export default VotingList
