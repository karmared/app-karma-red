import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link, Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
import Translate from 'react-translate-component'
import ReactGA from 'react-ga'

import {isTestnet, CoreAsset} from '../../../libs/'
import {getPrvKey} from '../../../reducers/authorization/'
import {getUserBalance, getCurrencies, getUserData} from '../../../reducers/karma/'
import {createAsk, toggleRequestPassword, clearPrvKey} from '../../../actions/'
import {Grid, Message, Dropdown, Radio} from 'semantic-ui-react'
import {Spinner, SliderSingle, Modal, Karma_input} from '../../helpers/'
import {getGlobal, getQuotations} from '../../../reducers/karma'
import {Karma} from '../../../libs'

class NewAsk extends Component {
    state = {
        loanAmount: '',
        loanAssetId: isTestnet ? '1.3.1' : '1.3.4',
        interest: 1,
        month: 1,
        hasCollateral: false,
        depositAmount: '',
        depositAssetId: '1.3.0',
        memo: '',
        alert: null,
        loading: false,
        success: false,
        modalConfirm: false,
        feeSize: 0
    };

    componentWillReceiveProps(nextProps) {
        if (!nextProps.prvKey) return;

        if (this.props.prvKey !== nextProps.prvKey) {
            this.submitData({prvKey: nextProps.prvKey})
        }
    }

    componentDidMount(){
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    componentWillUnmount() {
        this.props.clearKey()
    }

    handleInput = (state, value) => {
        this.setState({
            [state]: value,
        })
    };

    submitData = ({prvKey}) => {
        const { loanAmount, loanAssetId, interest, month, depositAmount, depositAssetId, memo, hasCollateral } = this.state;

        const params = {
            loan: [
                parseFloat(loanAmount),
                loanAssetId,
            ],
            interest,
            month,
            deposit: [
                hasCollateral ? parseFloat(depositAmount) : 0,
                hasCollateral ? depositAssetId : '1.3.0',
            ],
            memo,
            prvKey,
        };

        this.props.createAsk(params)
            .then(() => {
                this.setState({loading: false, success: true, modalConfirm: false});
            })
            .catch(error => (
                this.setState({
                    alert: (`${error}`).split('\n')[0],
                    loading: false,
                    showModal: false,
                })
            ))
    };

    handleSubmit = () => {

        const { loanAmount, depositAmount, hasCollateral, loanAssetId, depositAssetId } = this.state;
        const { quotations, currencies, global, balance } = this.props

        this.setState({alert: null, success: false, loading: true})

        let coreAsset = currencies.filter(e => e.symbol === CoreAsset)[0];
        let fee = `${global.operations.filter(e => e.operation === 'credit_request_operation')[0].fee / 10**coreAsset.precision} ${CoreAsset}`;
        let coreAmount = balance.filter(e => e.symbol === CoreAsset)[0].amount;

        if (coreAmount === 0) {
            return this.setState({
                loading: false,
                alert: <Translate component="p" content="deal_detail.message_wallet" fee={fee}/>,
            })
        }

        if (coreAmount < fee) {
            return this.setState({
                loading: false,
                alert: <Translate component="p" content="deal_detail.error_empty_asset" fee={fee}/>,
            })
        }

        let asset = currencies.filter(e => e.id === loanAssetId)[0];
        let assetDeposit = currencies.filter(e => e.id === depositAssetId)[0];

        let symbolL = asset.symbol !== CoreAsset ? asset.symbol.substr(1) : asset.symbol;
        let symbolD = assetDeposit.symbol !== CoreAsset ? assetDeposit.symbol.substr(1) : assetDeposit.symbol;

        if(quotations.length > 0){
            let coincidenceL = quotations.filter(e => e.symbol === symbolL),
                coincidenceD = quotations.filter(e => e.symbol === symbolD);
            asset = {
                ...asset,
                price_usd: coincidenceL.length > 0
                    ? coincidenceL[0].price_usd
                    : 1
            }
            assetDeposit = {
                ...assetDeposit,
                price_usd: coincidenceD.length > 0
                    ? coincidenceD[0].price_usd
                    : 1
            }
        }

        const temp = quotations.length > 0
            ? depositAmount / (loanAmount / assetDeposit.price_usd * asset.price_usd )
            : depositAmount / loanAmount;

        if (hasCollateral && temp <= 3) {
            return this.setState({
                loading: false,
                alert: <Translate component="p" content="deal_detail.error_small_deposit" />,
            })
        }

        if (!(parseFloat(loanAmount) > 0)) {
            return this.setState({
                loading: false,
                alert: <Translate component="p" content="deal_detail.error_amount" />,
            })
        }

        if (hasCollateral && !(parseFloat(depositAmount) > 0)) {
            return this.setState({
                loading: false,
                alert: <Translate component="p" content="deal_detail.error_empty_deposit" />,
            })
        }

        if (hasCollateral && parseFloat(depositAmount) > balance.filter(e => e.id === depositAssetId)[0].amount) {
            return this.setState({
                loading: false,
                alert: <Translate component="p" content="deal_detail.error_deposit_insufficient_funds" />,
            })
        }

        this.toggleConfirm()
    }

    toggleCollateral = () => {
        this.setState({hasCollateral: !this.state.hasCollateral})
    }

    toggleConfirm = () => {
        const {modalConfirm} = this.state;

        if (modalConfirm){
            this.setState({modalConfirm: false, loading: false})
        } else {
            this.setState({modalConfirm: true})
        }
    }

    handlePass = () => {
        const {toggleModalPass} = this.props;
        const {prvKey} = this.state;

        if (prvKey) {
            return this.submitData({prvKey})
        }
        return toggleModalPass()
    }

    renderMessage() {
        const {balance, user, global, currencies} = this.props;

        let message = '',
            feeSize = 0;

        if(global.operations && currencies.length > 0){
            feeSize = global.operations.filter(item => item.operation === 'credit_request_operation')[0].fee/10**currencies.filter(item => item.symbolPublic === CoreAsset)[0].precision;
        }

        if (balance.length === 0 || balance.filter(item => item.symbolPublic === CoreAsset)[0].amount < feeSize) {
            message = <Message key="dealsSection" warning className='message--warning'>
                <Translate component="p" content="deal_detail.message_wallet" fee={`${feeSize} ${CoreAsset}`}/>
            </Message>;
        }

        if (!user.extra.email) {
            message = <Message key="dealsSection" warning className='message--warning'>
                <Translate component="p" content="deal_detail.message_email" profile_link={<Link to={`/profile/${user.extra.username}/edit`}><Translate content='profile.profile'/></Link>}/>
            </Message>;
        }

        return (
            <section id="new-ask_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper currency__wrapper'>
                        <Grid.Column className='currency__column'>
                            {message}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )

    }

    render() {

        const {currencies, balance, user, global} = this.props;

        const { loanAmount, loanAssetId, interest, month,
            hasCollateral, depositAmount, depositAssetId, memo,
            alert, success, loading, modalConfirm } = this.state;

        if (success) {
            // return <Redirect to="/market/new_contract"/>
            return <Redirect to="/market"/>
        }

        if (!user.extra.email || /*!isTestnet ||*/ balance.length === 0) {
            return this.renderMessage()
        }

        const symbol = currencies.filter(item => item.id === loanAssetId)[0].symbol
        const symbolD = currencies.filter(item => item.id === depositAssetId)[0].symbol

        return (
            <section id="new-ask_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper currency__wrapper'>
                        <Grid.Column computer={7} tablet={16} className='karma__column karma__column--scroll'>
                            <div>
                                { alert && <Message key="dealsSection" warning className='message--warning'>{alert}</Message> }

                                <Translate component="h4" className="heading--sm heading--upper" content="deal_detail.credit_info" />

                                <div className="karma__inputs-group">
                                    <Karma_input type="number" min="0" required
                                                 label={<Translate component="label" className="karma__label" content="market.amount" />}
                                                 placeholder='0'
                                                 value={loanAmount}
                                                 onChange={e => this.handleInput('loanAmount', e.target.value)}
                                                 validate="sum"/>
                                    <div className="karma__field">
                                        <Translate component="label" className="karma__label" content="market.currency" />
                                        <Dropdown fluid search selection required name="taxResidence"
                                            value={loanAssetId}
                                            onChange={(e, {value}) => this.handleInput('loanAssetId', value)}
                                            className='karma__input settings__input'
                                            options={
                                                currencies.filter(item => !item.crypto).map(item => {
                                                    return {
                                                        key: item.id,
                                                        value: item.id,
                                                        text: item.symbol !== CoreAsset ? item.symbol.substr(1) : item.symbol
                                                    }
                                                })
                                            }
                                        />
                                    </div>
                                </div>

                                <div className="karma__inputs-group">
                                    <div className='karma__slider'>
                                        <Translate component="label" className="karma__label" content="market.interest_rate" />
                                        <SliderSingle affix="%" min={1} max={100} defaultValue={interest}
                                                      onAfterChange={e => this.handleInput('interest', e)}/>
                                    </div>
                                    <div className='karma__slider'>
                                        <label className='karma__label'><Translate content="time" />, <Translate content="months" /></label>
                                        <SliderSingle min={1} max={120} defaultValue={month}
                                                      onAfterChange={e => this.handleInput('month', e)}/>
                                    </div>
                                </div>

                                <Translate component="h4" className="heading--sm heading--upper" content="market.collateral" />

                                <div className="karma__inputs-group">
                                    <Radio
                                        label={<Translate component="label" className="karma__label" content="collateral_free" />}
                                        id={`${hasCollateral}1`}
                                        name='radioGroup'
                                        className='karma__radio karma__field'
                                        value='Сollateral-free'
                                        checked={!hasCollateral}
                                        onChange={this.toggleCollateral}
                                    />
                                    <Radio
                                        label={<Translate component="label" className="karma__label" content="collateral_crypto" />}
                                        id={`${hasCollateral}2`}
                                        name='radioGroup'
                                        className='karma__radio karma__field'
                                        value='Crypto-collateral'
                                        checked={hasCollateral}
                                        onChange={this.toggleCollateral}
                                    />
                                </div>

                                {hasCollateral &&
                                    <div className="karma__inputs-group">
                                        <Karma_input
                                            type="number" min="0" required value={depositAmount}
                                            label={<Translate component="label" className="karma__label" content="market.amount" />}
                                            placeholder='0'
                                            onChange={e => this.handleInput('depositAmount', e.target.value)}
                                            validate="sum"/>
                                        <div className='karma__field'>
                                            <Translate component="label" className="karma__label" content="market.currency" />
                                            <Dropdown fluid search selection required name="taxResidence"
                                                defaultValue={depositAssetId}
                                                disabled={currencies.filter(item => item.crypto).length < 1}
                                                className='karma__input settings__input'
                                                onChange={(e, {value}) => this.handleInput('depositAssetId', value)}
                                                options={
                                                    currencies.filter(item => item.crypto).map(item => {
                                                        return {
                                                            key: item.id,
                                                            value: item.id,
                                                            text: item.symbol !== CoreAsset ? item.symbol.substr(1) : item.symbol
                                                        }
                                                    })
                                                }
                                            />
                                        </div>
                                        <Translate component="p" className="text--secondary karma__hint" content="deal_detail.hint_deposit" />
                                    </div>
                                }

                                <Translate
                                  component="textarea"
                                    rows="4"
                                    name="purpose"
                                    value={memo}
                                    onChange={e => this.handleInput('memo', e.target.value)}
                                    className="karma__input"
                                    attributes={{placeholder: "deal_detail.loan_purpose_placeholder"}}
                                />

                                {loading
                                    ? <button className="btn btn--red btn--disabled"><Spinner className='spinner--inside'/></button>
                                    : <Translate component='button' content="btn.create" className="btn btn--red" onClick={this.handleSubmit} />
                                }

                                {modalConfirm &&
                                    <Modal
                                        title={<Translate className="heading--sm" content='auth.modal_title' />}
                                        className='modal--many_actions'
                                        toggleModal={this.toggleConfirm}
                                        footerActions={[
                                            <Translate
                                                key='cancel'
                                                component='button'
                                                type="button"
                                                className="btn btn--ghost"
                                                onClick={this.toggleConfirm}
                                                content='btn.cancel' />,
                                            <Translate
                                                key='accept'
                                                component='button'
                                                type="button"
                                                className="btn btn--red"
                                                onClick={this.handlePass}
                                                content='btn.agree' />
                                        ]}
                                    >
                                        <div className='message--confirm'>
                                            <Translate component='p' className="text--main" content='modal_confirm.a_u_sure_create' />
                                            <Translate component='p' className="text--main" content='modal_confirm.deal_data' />
                                            <Translate
                                              with={{
                                                amount: loanAmount,
                                                asset: symbol === CoreAsset ? symbol : symbol.substr(1)
                                              }}
                                              content="modal_confirm.loan_amount"
                                              component="p"
                                            />
                                            <Translate
                                              with={{ interest }}
                                              content="modal_confirm.loan_interest_rate"
                                              component="p"
                                            />
                                            <Translate
                                              with={{ count: month }}
                                              content="modal_confirm.loan_duration"
                                              component="p"
                                            />
                                            {
                                              hasCollateral &&
                                              <Translate
                                                with={{
                                                  amount: depositAmount,
                                                  asset: symbolD === CoreAsset ? symbolD : symbolD.substr(1),
                                                }}
                                                content="modal_confirm.loan_collateral"
                                                component="p"
                                              />
                                            }
                                            {
                                              memo &&
                                              <Translate
                                                with={{ memo }}
                                                content="modal_confirm.loan_memo"
                                                component="p"
                                              />
                                            }
                                            <Translate
                                              with={{
                                                amount: 10,
                                                asset: CoreAsset,
                                              }}
                                              content="modal_confirm.loan_fee"
                                              component="p"
                                            />
                                        </div>
                                    </Modal>
                                }


                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )
    }

    static propTypes = {
        currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
        balance: PropTypes.arrayOf(PropTypes.any).isRequired,
        user: PropTypes.objectOf(PropTypes.any).isRequired,
        createAsk: PropTypes.func.isRequired,
        toggleModalPass: PropTypes.func.isRequired,
        clearKey: PropTypes.func.isRequired,
        prvKey: PropTypes.string,
    };

    static defaultProps = {
        prvKey: '',
    }

}

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    quotations: getQuotations(state),
    balance: getUserBalance(state),
    user: getUserData(state),
    global: getGlobal(state),
    prvKey: getPrvKey(state),
});

const mapDispatchToProps = dispatch => ({
    createAsk: data => dispatch(createAsk(data)),
    toggleModalPass: () => dispatch(toggleRequestPassword()),
    clearKey: () => dispatch(clearPrvKey()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewAsk)
