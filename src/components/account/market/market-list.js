import React from 'react'
import MediaQuery from 'react-responsive'
import Translate from 'react-translate-component'

import { SvgDown } from '../../../svg/'
import {Spinner} from '../../helpers/'
import ListItem from './list-item'

const MarketList = ({btnMoreClick, showMore, loading, data}) => {
    return (
        <MediaQuery minWidth={810}>
            {(matches) => {
                if (matches) {
                    return (
                        <div className='karma__table--market'>
                            <div className="karma__table-header">
                                <Translate className='karma__table-cell w20 name' content="market.table_name" />
                                <Translate className='karma__table-cell w15' content="market.currency_amount" />
                                <Translate className='karma__table-cell w15' content="market.interest_rate" />
                                <Translate className='karma__table-cell w15' content="market.collateral_rate" />
                                <Translate className='karma__table-cell w15' content="market.credit_terms" />
                                <span className='karma__table-cell w20' />
                            </div>
                            <div className="karma__table-body">
                                {data.map(item => <ListItem key={item.object_uuid} offer={item} />)}
                            </div>
                            {showMore &&
                            <div className="karma__table-footer">
                                {loading
                                    ? <Spinner />
                                    : <button type="button" onClick={btnMoreClick} className="btn btn--more">
                                        <Translate content="btn.show_more" />
                                        <SvgDown width={14} height={6} className='ico'/>
                                    </button>}
                            </div>
                            }
                        </div>
                    )
                }

                return [
                    <div key='marketList' className="market__list">
                        {data.map(item => <ListItem key={item.object_uuid} offer={item} />)}
                    </div>,
                    <div key='actionPanel' className="btn_wrapper">
                        {(showMore && loading) &&
                        <Spinner className='spinner--inside'/>
                        }
                        {(showMore && !loading) &&
                        <button type="button" onClick={btnMoreClick} className="btn btn--more">
                            <Translate content="btn.show_more" />
                            <SvgDown width={14} height={6} className='ico'/>
                        </button>
                        }
                    </div>
                ]
            }}
        </MediaQuery>
    )
}

export default MarketList;
