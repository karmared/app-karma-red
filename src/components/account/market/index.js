import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import _ from 'lodash'
import Translate from 'react-translate-component'
import ReactGA from 'react-ga'
import {Spinner} from '../../helpers/'
import {Karma} from '../../../libs/karma'

import Filters from './filters'
import MarketList from './market-list'

import {Grid, Message} from 'semantic-ui-react'
import {getUserData} from '../../../reducers/karma'

const getOffers = async ({items, filters, isClear}) => {
    let from = 0;

    if (!isClear) {
        from = items.length > 0 ? items.length - 1 : 0
    }
    const queryParams = [
        from, // from_index
        10, // elements_count
        parseInt(filters.rate[0], 10), // loan_persent_from
        parseInt(filters.rate[1], 10), // loan_persent_to
        parseInt(filters.collateral[0], 10), // deposit_persent_from
        parseInt(filters.collateral[1], 10), // deposit_persent_to
        parseInt(filters.amount[0], 10), // loan_volume_from
        parseInt(filters.amount[1], 10), // loan_volume_to
        filters.currency, // cyrrency_symbol
        '', // user login
        1, // status
    ];

    const result = await Karma.dbApi.exec('fetch_credit_requests_stack', queryParams).then(e => e);

    const data = isClear ? result : [...items, ...result];

    return {
        items: _.uniqBy(data, 'id'),
        showMore: result.length >= 10,
        loading: false,
        empty: !data.length,
    }
};

class Market extends Component {
    state = {
        items: [],
        showMore: true,
        loading: false,
        empty: false,
    };

    componentDidMount() {
        this.loadMore(this.props.filters, true)
    }

    componentWillReceiveProps(nextProps) {
        const needClearList = JSON.stringify(this.props.filters) !== JSON.stringify(nextProps.filters);
        if(needClearList){
            this.loadMore(nextProps.filters, needClearList)
        }
    }

    loadMore = (filters, isClear) => {
        const {items} = this.state;
        this.setState({loading: true});
        getOffers({items, filters, isClear}).then(e => this.setState(e))
    };

    render() {
        const {filters, user} = this.props;
        const {items, showMore, loading, empty} = this.state;
        return (
            <section id="market_page" className='content'>
                <div className="market__header">
                    <div className="market__title">
                        <Translate component='h4' className='heading--md' content="market.title" />
                        { !user.extra.confirmed
                            ? <Translate content="btn.create_ask" className='btn btn--red btn--disabled' />
                            : <Link to='/market/new' className='btn btn--red'><Translate content="btn.create_ask" /></Link>
                        }

                    </div>
                    <div className="market__filters-wrapper">
                        <Filters/>
                    </div>
                    {!user.extra.confirmed &&
                       <Link to={`/profile/${user.name}/edit`} className='text--red text--lowercase'>
                           <Translate content="btn.verify_email_long_mes" />
                       </Link>
                    }
                </div>
                {empty &&
                <Message warning className='message--warning'>
                    <Translate component='p' content="market.no_data" />
                </Message>
                }

                {(!empty && items.length === 0) && <Spinner/>}

                {items.length > 0 &&
                <MarketList data={items} showMore={showMore} loading={loading} btnMoreClick={() => this.loadMore(filters, false)}/>
                }
            </section>
        )
    }

    static propTypes = {
        filters: PropTypes.objectOf(PropTypes.any).isRequired,
    }
}

const mapStateToProps = state => ({
    user: getUserData(state),
    filters: state.market.filters,
});

export default connect(mapStateToProps)(Market)
