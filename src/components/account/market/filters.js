import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import Translate from 'react-translate-component'

import {getCurrencies} from '../../../reducers/karma/index'
import {setMarketFilters} from '../../../actions/market/filters'
import {SliderRange} from '../../helpers/'

import { Label, Dropdown } from 'semantic-ui-react'
import {CoreAsset} from '../../../libs'

class Filters extends Component {
    constructor(props) {
        super(props)
        const {filters} = this.props

        this.state = {
            ...filters,
        }
    }

    componentDidUpdate() {
        this.props.setMarketFilters(this.state)
    }

    handleCurrency = (e, { value }) => {
        this.setState({ currency: value })
    }

    handleSlide = (name, e) => {
        this.setState({[name]: e})
    }

    render() {
        const {currencies} = this.props;
        const {rate, collateral, amount, currency} = this.state;

        let curDropdown =
            currencies
                .filter(item => !item.crypto)
                .map((item) => {
                    const symbol = item.symbol !== CoreAsset ? item.symbol.substr(1) : item.symbol;
                    return {key: symbol.name, value: item.symbol, text: symbol}
                });

        curDropdown.push({key: 'all', value: null, text: <Translate content="market.all_currencies" />});
        return [
            <div key='interest' className='karma__slider karma__slider--market'>
                <Translate component='label' className='karma__label' content="market.interest_rate" />
                <SliderRange
                    affix="%"
                    min={0}
                    max={100}
                    defaultValue={rate}
                    onAfterChange={e => this.handleSlide('rate', e)}
                />
            </div>,
            <div key='collateral' className='karma__slider karma__slider--market'>
                <Translate component='label' className='karma__label' content="market.collateral_size" />
                <SliderRange
                    min={0}
                    max={300000}
                    defaultValue={collateral}
                    onAfterChange={e => this.handleSlide('collateral', e)}
                />
            </div>,
            <div key='loan_size' className='karma__slider karma__slider--market'>
                <Translate component='label' className='karma__label' content="market.loan_size" />
                <SliderRange
                    min={0}
                    max={10000000}
                    defaultValue={amount}
                    onAfterChange={e => this.handleSlide('amount', e)}
                />
            </div>,
            <div key='cur' className='karma__field karma__slider karma__slider--market karma__slider--dropdown'>
                <Dropdown
                    fluid
                    selection
                    className='karma__input'
                    options={curDropdown}
                    placeholder={<Translate content="market.all_currencies" />}
                    value={currency}
                    onChange={this.handleCurrency}
                />
            </div>
        ]
    }

    static propTypes = {
        currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
        filters: PropTypes.objectOf(PropTypes.any).isRequired,
        setMarketFilters: PropTypes.func.isRequired,
    }
    }

    const mapStateToProps = state => ({
        currencies: getCurrencies(state),
        filters: state.market.filters,
    })

    const mapDispatchToProps = dispatch => ({
        setMarketFilters: data => dispatch(setMarketFilters(data)),
    })

    export default connect(mapStateToProps, mapDispatchToProps)(Filters)
