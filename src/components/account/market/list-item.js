import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import MediaQuery from 'react-responsive'
import 'whatwg-fetch'
import Translate from 'react-translate-component'

import {getCurrencies, getQuotations, getUserData} from '../../../reducers/karma'
import {cancelAsk} from '../../../actions'
import {Karma, ApiURI} from '../../../libs/'
import {nFormatter, Spinner, Avatar, Modal} from '../../helpers/'
import {CoreAsset} from '../../../libs'


const getData = async (id) => {
    const user = await Karma.dbApi.exec('get_accounts', [[id]]).then(e => e[0])

    if (!user) {
        return {
            alert: "Cannot get borrower's information",
        }
    }

    const extra = await fetch(`${ApiURI.extraApi}/users/${user.name}`)
        .then(e => e.json())
        .catch(error => ({
            user: null,
            alert: error.message,
        }));

    if (!extra) {
        return {
            user,
            alert: null,
        }
    }

    return {
        user: {...user, extra},
        alert: null,
    }
};

class ListItem extends Component {
    state = {
        user: null,
        alert: '',
        loading: false,
        cancel: false,
        showCancel: false
    };

    componentDidMount() {
        getData(this.props.offer.borrower.borrower).then(e => this.setState(e))
    }

    componentWillReceiveProps(nextProps) {
        getData(nextProps.offer.borrower.borrower).then(e => this.setState(e));
    }

    handleCancel = () => {
        this.setState({ loading: true })
        this.props.cancelAsk(this.props.offer.object_uuid).then(() => this.setState({ cancel: true, loading: false }))
    };

    toggleModal = () => {
        this.setState({ showCancel: !this.state.showCancel })
    }

    render() {
        const {user, alert, cancel, showCancel, loading} = this.state;
        const {offer, quotations, currencies} = this.props;
        const me = this.props.user

        if( cancel ){
            return (
                <MediaQuery minWidth={810}>
                    {(matches) => {
                        return (
                            <div className={matches ? 'karma__table-item' : 'item item--market'}>
                                <Translate component='p' className='text--red text--main' content="deal_detail.ask_canceled" />
                            </div>
                        )
                    }}
                </MediaQuery>
            )
        }

        if (alert) {
            return (
                <div className="karma__table-item">
                    <p className="text--main text-red">{alert}</p>
                </div>
            )
        }

        if (!user) {
            return (
                <div className="karma__table-item">
                    <Spinner className='spinner--absolute'/>
                </div>
            )
        }

        let asset = currencies.filter(e => e.id === offer.borrower.loan_asset.asset_id)[0];
        let assetDeposit = currencies.filter(e => e.id === offer.borrower.deposit_asset.asset_id)[0];

        let symbolL = asset.symbol !== CoreAsset ? asset.symbol.substr(1) : asset.symbol;
        let symbolD = assetDeposit.symbol !== CoreAsset ? assetDeposit.symbol.substr(1) : assetDeposit.symbol;

        if(quotations.length > 0){
            let coincidenceL = quotations.filter(e => e.symbol === symbolL),
                coincidenceD = quotations.filter(e => e.symbol === symbolD);
            asset = {
                ...asset,
                price_usd: coincidenceL.length > 0
                    ? coincidenceL[0].price_usd
                    : 1
            }
            assetDeposit = {
                ...assetDeposit,
                price_usd: coincidenceD.length > 0
                    ? coincidenceD[0].price_usd
                    : 1
            }
        }

        const precisionDeposit = 10**assetDeposit.precision;
        const precisionLoan = 10**asset.precision;

        const amountL = offer.borrower.loan_asset.amount / precisionLoan;
        const amountD = offer.borrower.deposit_asset.amount / precisionDeposit;

        let collateral = quotations.length > 0 ? amountD / (amountL / assetDeposit.price_usd * asset.price_usd ) * 100 : amountD / amountL * 100;

        collateral = Math.round(collateral * 100) / 100;
        if (collateral <= 100) collateral = <Translate content="collateral_free" />;
        else collateral += '%';

        return [
            <MediaQuery minWidth={810} key='main'>
                {(matches) => {
                    if (matches) {
                        return (

                            <div className="karma__table-item">
                                <span className='karma__table-cell w20 name'>
                                    <Link to={`/profile/${user.name}`} >
                                        <Avatar
                                            img={user.extra ? user.extra.avatar.thumb.url : null}
                                            className="avatar--sm"
                                            name={{first_name: user.name}}
                                        />
                                        <span>
                                            {!user.extra
                                                ? user.name
                                                : `${user.extra.first_name
                                                    ? user.extra.first_name
                                                    : user.name }
                                                    ${user.extra.last_name
                                                    ? ` ${user.extra.last_name}`
                                                    : ''}
                                                `}
                                        </span>
                                    </Link>
                                </span>
                                <span className='karma__table-cell w15'><span className='sum'>{nFormatter(amountL)}&nbsp;{symbolL}</span></span>
                                <span className='karma__table-cell w15'>{parseFloat(offer.borrower.loan_persent)}%</span>
                                <span className='karma__table-cell w15'>{collateral}</span>
                                <span className='karma__table-cell w15'>{offer.borrower.loan_period}</span>
                                <span className='karma__table-cell w10'>
                                    { me.id === offer.borrower.borrower
                                        ? <Translate component='button' content="btn.cancel" className='btn btn--small btn--red' onClick={this.toggleModal} />
                                        : ''}
                                </span>
                                <span className='karma__table-cell w10'>
                                    <Link to={`/market/${offer.object_uuid}`}
                                          className="btn btn--small btn--visit"><Translate content="btn.more" /></Link>
                                </span>
                            </div>
                        )
                    }

                    return (
                        <div className="item item--market">
                            <Link to={`/profile/${user.name}`} className="item__prop item__prop--name">
                                <Avatar
                                    img={user.extra ? user.extra.avatar.thumb.url : null}
                                    className="avatar--sm"
                                    name={{first_name: user.name}}
                                />
                                <span className="heading--sm">
                                    {!user.extra ? user.name : `${user.extra.first_name ? user.extra.first_name : user.name }${user.extra.last_name ? ` ${user.extra.last_name}` : ''}`}
                                </span>
                            </Link>
                            <div className="item__prop">
                                <Translate className="item__label" content="market.currency_amount" />
                                <span className="sum">{nFormatter(amountL)}&nbsp;{symbolL}</span>
                            </div>
                            <div className="item__prop">
                                <Translate className="item__label" content="market.interest_rate" />
                                <span className="item__value">{parseFloat(offer.borrower.loan_persent)}%</span>
                            </div>
                            <div className="item__prop">
                                <Translate className="item__label" content="market.collateral_rate" />
                                <span className="item__value">{collateral}</span>
                            </div>
                            <div className="item__prop">
                                <Translate className="item__label" with={{month: <Translate content="months" />}} content="market.credit_terms" />
                                <span className="item__value">{offer.borrower.loan_period}</span>
                            </div>
                            <div className="item__prop">
                                <Link to={`/market/${offer.object_uuid}`} className="btn btn--small btn--visit"><Translate content="btn.more" /></Link>
                            </div>
                        </div>
                    )
                }}
            </MediaQuery>,
            showCancel &&
            <Modal
                title={<Translate className="heading--sm" content='auth.modal_title' />}
                className='modal--many_actions' key='modal'
                toggleModal={this.toggleModal}
                footerActions={[
                    loading
                        ? <span key='cancel' className='btn btn--red loading btn--disabled'><Spinner className='spinner--inside'/></span>
                        : <Translate key='cancel' component='button' type="button" className="btn btn--red" onClick={this.handleCancel} content={`btn.cancel_ask`} />
                ]}
            >
                <div className='message--confirm'>
                    <Translate component='p' className="text--main" content={`modal_confirm.a_u_sure_cancel`} />
                    <Translate component='p' className="text--main" content='modal_confirm.deal_data' />
                    <p> {`Сумма займа ${nFormatter(amountL)} ${symbolL}`}</p>
                    <p>{`Кредитная ставка ${parseFloat(offer.borrower.loan_persent)}%`}</p>
                    <p>{`Срок кредита ${offer.borrower.loan_period} месяцев`}</p>
                    {collateral}
                    <p>{`Размер комиссии 1 KRM`}</p>
                </div>
            </Modal>
        ]
    }

    static propTypes = {
        offer: PropTypes.objectOf(PropTypes.any).isRequired,
        currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
    }
}

const mapStateToProps = state => ({
    user: getUserData(state),
    currencies: getCurrencies(state),
    quotations: getQuotations(state)
})

const mapDispatchToProps = dispatch => ({
    cancelAsk: data => dispatch(cancelAsk(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ListItem)
