import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import Translate from 'react-translate-component'
import ReactGA from 'react-ga'

import {getUserBalance, getCurrencies, getUserData} from '../../../reducers/karma/'
import {createAsk, toggleRequestPassword} from '../../../actions/'
import {Grid, Dropdown, Radio, Checkbox} from 'semantic-ui-react'
import {SliderSingle, Karma_input} from '../../helpers/'
import { contract } from '../../../locales/en_contract'


const contracts = [
    {value: 'type1', text: <Translate content="contract.contract_type1"/>},
    {value: 'type2', text: <Translate content="contract.contract_type2"/>},
    {value: 'type3', text: <Translate content="contract.contract_type3"/>}
]

class NewContract extends Component {
    state = {
        amount: '',
        date: '',
        interest_rate: 1,
        day: 1,
        percent: 'monthly',
        prepayment: '',
        repayment: 'in_parts',
        contract_type: 'type1',
        before_the_appointed_time: false,
        advance_notice: false,
        advance_notice_day: 30,
        advance_notice_law: 30,
        collateral: false,
        collateral_amount: 0
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.prvKey) return

        if (this.props.prvKey !== nextProps.prvKey) {
            this.submitData({prvKey: nextProps.prvKey})
        }
    }

    componentDidMount(){
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    handleInput = (state, value) => {
        this.setState({[state]: value})
    }

    render() {
        const {
            amount,
            interest_rate,
            date,
            day,
            before_the_appointed_time,
            advance_notice,
            advance_notice_day,
            advance_notice_law,
            contract_type,
            collateral,
            collateral_amount,
            percent,
            repayment,
            prepayment
        } = this.state

        const contractHTML = contract.filled.split('\n');

        return (
            <section id="new-ask_page" className='content'>
                <Grid celled className='karma__grid'>
                    <Grid.Row className='karma__wrapper currency__wrapper'>
                        <Grid.Column computer={7} tablet={16} className='karma__column karma__column--scroll'>
                            <div>
                                <Translate component="h4" className="heading--sm heading--upper" content="contract.title"/>

                                <div className="karma__field">
                                    <Translate component="label" className="karma__label" content="contract.contract_type"/>
                                    <Dropdown
                                        fluid
                                        search
                                        selection
                                        name="contract_type"
                                        required
                                        value={contract_type}
                                        onChange={(e, {value}) => this.handleInput('contract_type', value)}
                                        className='karma__input settings__input'
                                        options={contracts}
                                    />
                                </div>

                                <Karma_input
                                    type="number"
                                    min="0" required
                                    label={<Translate component="label" className="karma__label" content="contract.loan_amount"/>}
                                    placeholder='0'
                                    value={amount}
                                    onChange={e => this.handleInput('amount', e.target.value)}
                                    validate="sum"/>

                                <Translate component="h4" className="heading--sm heading--upper" content="contract.payment_procedure"/>

                                <Karma_input
                                    type="date"
                                    label={<Translate component="label" className="karma__label" content="contract.date_repayment_of_loan"/>}
                                    value={date}
                                    onChange={e => this.handleInput('date', e.target.value)}/>

                                <div className="karma__inputs-group karma__inputs-group--vertical">
                                    <Translate component="label" className="karma__label" content="contract.loan_repayment"/>
                                    <Radio
                                        label={<Translate component="label" className="karma__label" content="contract.repayment_at_it_time"/>}
                                        name='Repayment'
                                        className='karma__radio karma__field contract__radio'
                                        value='at_it_time'
                                        checked={repayment === 'at_it_time'}
                                        onChange={() => this.handleInput('repayment', 'at_it_time')}
                                    />
                                    <Radio
                                        label={<Translate component="label" className="karma__label" content="contract.repayment_in_parts"/>}
                                        name='Repayment'
                                        className='karma__radio karma__field contract__radio'
                                        value='in_parts'
                                        checked={repayment === 'in_parts'}
                                        onChange={() => this.handleInput('repayment', 'in_parts')}
                                    />
                                </div>



                                <div className='karma__slider karma__slider--contract'>
                                    <Translate component="label" className="karma__label" content="market.interest_rate"/>
                                    <SliderSingle affix="%" min={1} max={100} defaultValue={interest_rate} onAfterChange={e => this.handleInput('interest_rate', e)}/>
                                </div>

                                <div className="karma__inputs-group karma__inputs-group--vertical">
                                    <Translate component="label" className="karma__label" content="contract.payment_of_interest"/>
                                    <Radio
                                        label={<Translate component="label" className="karma__label" content="contract.monthly"/>}
                                        name='Percent'
                                        className='karma__radio karma__field contract__radio'
                                        value='monthly'
                                        checked={percent === 'monthly'}
                                        onChange={() => this.handleInput('percent', 'monthly')}
                                    />
                                    <Radio
                                        label={<Translate component="label" className="karma__label" content="contract.quarterly"/>}
                                        name='Percent'
                                        className='karma__radio karma__field contract__radio'
                                        value='quarterly'
                                        checked={percent === 'quarterly'}
                                        onChange={() => this.handleInput('percent', 'quarterly')}
                                    />
                                </div>

                                <div className='karma__slider karma__slider--contract'>
                                    <Translate component="label" className="karma__label" content="contract.payment_day"/>
                                    <SliderSingle min={1} max={31} defaultValue={day}
                                                  onAfterChange={e => this.handleInput('day', e)}/>
                                </div>

                                <Translate component="h4" className="heading--sm heading--upper" content="contract.before_the_appointed_time"/>

                                <div className="karma__field">
                                    <Checkbox className='karma__toggle text--main'
                                              label={<Translate component="label" className="karma__label" content="contract.possibly"/>}
                                              name='before_the_appointed_time'
                                              toggle
                                              checked={before_the_appointed_time}
                                              onChange={() => this.handleInput('before_the_appointed_time', !before_the_appointed_time)}/>
                                </div>


                                {before_the_appointed_time &&
                                    [
                                    <div className="karma__inputs-group karma__inputs-group--vertical">
                                        <Translate component="label" className="karma__label" content="contract.amount_of_early_repaid_funds"/>
                                        <Radio
                                            label={<Translate component="label" className="karma__label" content="contract.prepayment_full"/>}
                                            name='Prepayment'
                                            className='karma__radio karma__field contract__radio'
                                            value='full'
                                            checked={prepayment === 'full'}
                                            onChange={() => this.handleInput('prepayment', 'full')}
                                        />
                                        <Radio
                                            label={<Translate component="label" className="karma__label" content="contract.prepayment_parts"/>}
                                            name='Prepayment'
                                            className='karma__radio karma__field contract__radio'
                                            value='parts'
                                            checked={prepayment === 'parts'}
                                            onChange={() => this.handleInput('prepayment', 'parts')}
                                        />
                                        <Radio
                                            label={<Translate component="label" className="karma__label" content="contract.prepayment_full_or_parts"/>}
                                            name='Prepayment'
                                            className='karma__radio karma__field contract__radio'
                                            value='full or parts'
                                            checked={prepayment === 'full or parts'}
                                            onChange={() => this.handleInput('prepayment', 'full or parts')}
                                        />
                                    </div>,

                                    <div className="karma__field">
                                        <Checkbox className='karma__toggle text--main'
                                                  label={<Translate component="label" className="karma__label" content="contract.early_repayment_warning"/>}
                                                  name='advance_notice'
                                                  toggle
                                                  checked={advance_notice}
                                                  onChange={() => this.handleInput('advance_notice', !advance_notice)}/>
                                    </div>
                                    ]
                                }

                                {advance_notice && before_the_appointed_time &&
                                [
                                    <div className="karma__inputs-group karma__inputs-group--vertical">
                                        <Translate component="label" className="karma__label" content="contract.warning_no_later_than"/>
                                        <Radio
                                            label={<Translate component="label" className="karma__label" content="contract.30day"/>}
                                            name='Notice_day'
                                            className='karma__radio karma__field contract__radio'
                                            value={30}
                                            checked={advance_notice_day === 30}
                                            onChange={() => this.handleInput('advance_notice_day', 30)}
                                        />
                                        <Radio
                                            label={<Translate component="label" className="karma__label" content="contract.60day"/>}
                                            name='Notice_day'
                                            className='karma__radio karma__field contract__radio'
                                            value={60}
                                            checked={advance_notice_day === 60}
                                            onChange={() => this.handleInput('advance_notice_day', 60)}
                                        />
                                    </div>,
                                    <div className="karma__inputs-group karma__inputs-group--vertical">
                                        <Translate component="label" className="karma__label" content="contract.getting_the_law_to_early_repayment"/>
                                        <Radio
                                            label={<Translate component="label" className="karma__label" content="contract.30day"/>}
                                            name='Notice_law'
                                            className='karma__radio karma__field contract__radio'
                                            value={30}
                                            checked={advance_notice_law === 30}
                                            onChange={() => this.handleInput('advance_notice_law', 30)}
                                        />
                                        <Radio
                                            label={<Translate component="label" className="karma__label" content="contract.180day"/>}
                                            name='Notice_law'
                                            className='karma__radio karma__field contract__radio'
                                            value={180}
                                            checked={advance_notice_law === 180}
                                            onChange={() => this.handleInput('advance_notice_law', 180)}
                                        />
                                    </div>
                                ]
                                }

                                <Translate component="h4" className="heading--sm heading--upper" content="contract.securing_commitments"/>

                                <div className="karma__field">
                                    <Checkbox className='karma__toggle text--main'
                                              label={<Translate component="label" className="karma__label" content="contract.provision_of_collateral"/>}
                                              name='Сollateral'
                                              toggle
                                              checked={collateral}
                                              onChange={() => this.handleInput('collateral', !collateral)}/>
                                </div>

                                {collateral &&
                                    <Karma_input
                                        type="number"
                                        min="0" required
                                        label={<Translate component="label" className="karma__label" content="contract.deposit_amount"/>}
                                        placeholder='0'
                                        value={collateral_amount}
                                        onChange={e => this.handleInput('collateral_amount', e.target.value)}
                                        validate="sum"/>
                                }

                                <div className="btn-wrapper">
                                    {/*<Translate component="button" type="button" className="btn btn--red" content="btn.create"/>*/}
                                    <Translate component="a" href='/market' className="btn btn--red" content="btn.create"/>
                                </div>
                            </div>
                        </Grid.Column>

                        <Grid.Column computer={9} tablet={16} className='karma__column karma__column--scroll'>
                            <div>
                                <Translate component="h4" className="heading--sm heading--upper" content="contract.preview"/>
                                {contractHTML.map((item, index) => <p key={index}>{item}</p>)}
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </section>
        )
    }

    static propTypes = {
        currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
        balance: PropTypes.arrayOf(PropTypes.any).isRequired,
        user: PropTypes.objectOf(PropTypes.any).isRequired,
        createAsk: PropTypes.func.isRequired,
        toggleModalPass: PropTypes.func.isRequired,
        clearKey: PropTypes.func.isRequired,
        prvKey: PropTypes.string,
    }

    static defaultProps = {
        prvKey: '',
    }

}

const mapStateToProps = state => ({
    currencies: getCurrencies(state),
    balance: getUserBalance(state),
    user: getUserData(state),
})

const mapDispatchToProps = dispatch => ({
    createAsk: data => dispatch(createAsk(data)),
    toggleModalPass: () => dispatch(toggleRequestPassword()),
})

export default connect(mapStateToProps, mapDispatchToProps)(NewContract)

