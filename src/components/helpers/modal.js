import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {SvgClose} from '../../svg/'


export class Modal extends Component {
    componentDidMount = () => {
        const scrollWidth = window.innerWidth - document.body.clientWidth;

        document.body.setAttribute('style', `overflow: hidden; padding-right: ${scrollWidth}px`)
    };

    componentWillUnmount = () => {
        document.body.removeAttribute('style')
    };

    render() {
        const {children, toggleModal, title, hasFooter, lg, xl, footerActions, className} = this.props;

        return (
            <div className="modal__wrapper">
                <div className={`modal${lg ? ' lg' : ''}${xl ? ' xl' : ''}${className ? ' '+className : ''}`}>

                    {title &&
                    <header className="modal__header">
                        {title}
                        <button type="button" className="btn btn--link btn--red_text" onClick={toggleModal}>
                            <SvgClose width={24} height={24}/>
                        </button>
                    </header>
                    }

                    <div className="modal__content">
                        {children}
                    </div>

                    { footerActions
                        ? <footer className="modal__footer">
                            { footerActions }
                        </footer>
                        : ''
                    }
                </div>
                <button className="bg" onClick={toggleModal}/>
            </div>
        )
    }

    static propTypes = {
        children: PropTypes.objectOf(PropTypes.any).isRequired,
        toggleModal: PropTypes.func.isRequired,
        hasFooter: PropTypes.bool,
        className: PropTypes.string,
        footerActions: PropTypes.array,
        lg: PropTypes.bool,
        xl: PropTypes.bool,
    };

    static defaultProps = {
        title: '',
        hasFooter: false,
        lg: false,
        xl: false,
    }
}
