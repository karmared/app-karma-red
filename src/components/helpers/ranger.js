import React from 'react'
import PropTypes from 'prop-types'
import Slider, { Range } from 'rc-slider'

import { nFormatter } from './'


const { Handle } = Slider

const handler = ({ affix, value, index, dragging, ...restProps }) => (
  <Handle key={`handler${index}`} value={value} {...restProps}>
    <span data-dragging={dragging}>
      {affix ? value : nFormatter(value)}{affix}</span>
  </Handle>
)

handler.propTypes = {
  affix: PropTypes.string,
  value: PropTypes.number.isRequired,
  dragging: PropTypes.bool.isRequired,
  index: PropTypes.number.isRequired,
}

handler.defaultProps = {
  affix: '',
}

export const SliderRange = ({ affix, min, max, step, defaultValue, onAfterChange }) => (
  <Range
    min={min}
    max={max}
    step={step}
    defaultValue={defaultValue}
    handle={props => handler({ affix, ...props })}
    onAfterChange={e => onAfterChange(e)}
  />
)

SliderRange.propTypes = {
  affix: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  defaultValue: PropTypes.arrayOf(PropTypes.number),
  onAfterChange: PropTypes.func,
}

SliderRange.defaultProps = {
  affix: '',
  min: 0,
  max: false,
  step: 1,
  defaultValue: [0, 0],
  onAfterChange: null,
}
