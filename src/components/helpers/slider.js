import React from 'react'
import PropTypes from 'prop-types'
import Slider from 'rc-slider'

import { nFormatter } from './'


const { Handle } = Slider

const handler = ({ affix, value, dragging, ...restProps }) => (
  <Handle value={value} {...restProps}>
    <span data-dragging={dragging}>
      {affix ? value : nFormatter(value)}{affix}</span>
  </Handle>
)

handler.propTypes = {
  affix: PropTypes.string,
  value: PropTypes.number.isRequired,
  dragging: PropTypes.bool.isRequired,
}

handler.defaultProps = {
  affix: '',
}

export const SliderSingle = ({ affix, min, max, step, defaultValue, onAfterChange }) => (
  <Slider
    min={min}
    max={max}
    step={step}
    defaultValue={defaultValue}
    handle={props => handler({ affix, ...props })}
    onAfterChange={e => onAfterChange(e)}
  />
)

SliderSingle.propTypes = {
  affix: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  defaultValue: PropTypes.number,
  onAfterChange: PropTypes.func,
}

SliderSingle.defaultProps = {
  affix: '',
  min: 0,
  max: false,
  step: 1,
  defaultValue: null,
  onAfterChange: null,
}
