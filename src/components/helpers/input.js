import React, {Component} from 'react'
import PropTypes from 'prop-types'
import ReactPasswordStrength from 'react-password-strength/dist/universal'

export class Karma_input extends Component {
    validate = (event) => {
        const {validate} = this.props

        if (!validate) return

        const keyCode = event.keyCode || event.which
        const element = event.target

        if (keyCode !== 13) {
            const keyValue = String.fromCharCode(keyCode)
            const classes = element.classList

            classes.remove('shake')

            let reg

            if (validate === 'string') {
                reg = /^[a-zA-Z\s\-]*$/
            }
            else if (validate === 'email') {
                reg = /^[A-Z\da-z@\-\._]*$/
            }
            else if (validate === 'phone') {
                reg = /^[\d\-\+\s]*$/
            }
            else if (validate === 'digits') {
                reg = /^\d*$/
            }
            else if (validate === 'sum') {
                reg = /^[\d\,]*$/
            }
            else if (validate === 'login') {
                reg = /^[\da-zA-Z\-\.]*$/
            }

            if (!(reg.test(keyValue))) {
                event.preventDefault()
                setTimeout(() => classes.add('shake'), 100)
            }
        }
    }

    render() {
        const {className, classWrapper, type, label, placeHolder, value, defaultValue, btn, passStrength, ...restProps} = this.props;

        if (passStrength) {
            return (
                <div className='karma__field'>
                    {label}
                    <ReactPasswordStrength {...restProps} />
                    {(value ==='' || defaultValue ==='') && placeHolder}
                </div>
            )
        }

        return (
            <div className={`karma__field ${classWrapper ? classWrapper : ''}`}>
                {label}
                <input className={`karma__input ${className ? className : ''}`} type={type} value={value} {...restProps} onKeyPress={this.validate}/>
                {(value ==='' || defaultValue ==='') && placeHolder}
                {btn}
            </div>
        )
    }

    static propTypes = {
        type: PropTypes.string,
        validate: PropTypes.string,
    }

    static defaultProps = {
        type: 'text',
        validate: '',
    }
}
