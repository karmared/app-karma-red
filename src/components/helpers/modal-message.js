import React from 'react'
import { Message } from 'semantic-ui-react';
import { Modal } from './'
import {SvgClose} from '../../svg/'

export const ModalMessage = ({toggle, message}) => (
    <Modal className='modal--message' toggleModal={toggle} >
        <Message className='message--success one-line-block'>
            {message}
            <button type="button" className="btn btn--link btn--red_text" onClick={toggle}>
                <SvgClose width={24} height={24}/>
            </button>
        </Message>
    </Modal>
)
