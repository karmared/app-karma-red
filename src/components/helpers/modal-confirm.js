import React from 'react'
import Translate from 'react-translate-component'
import { Modal } from './'

export const ModalConfirm = ({title, toggle, message, footer, data}) => (
    <Modal title={title} className='modal--many_actions' toggleModal={toggle} footerActions={footer}>
        <div className='message--confirm'>
            {message}
            <Translate component='p' className="text--main" content='modal_confirm.deal_data' />
            <Translate
              with={{
                amount: data.sum,
              }}
              content="modal_confirm.loan_amount"
              component="p"
            />
            <Translate
              with={{
                interest: data.interest
              }}
              content="modal_confirm.loan_interest_rate"
              component="p"
            />
            <Translate
              with={{
                count: data.term
              }}
              content="modal_confirm.loan_duration"
              component="p"
            />
            {
              data.collateral &&
              <Translate
                with={{
                  amount: data.collateral
                }}
                content="modal_confirm.loan_collateral"
                component="p"
              />
            }
            {
              data.purpose &&
              <Translate
                with={{
                  memo: data.purpose
                }}
                content="modal_confirm.loan_memo"
                component="p"
              />
            }
            <Translate
              with={{
                amount: 1,
                asset: "KRMT",
              }}
              content="modal_confirm.loan_fee"
              component="p"
            />
        </div>
    </Modal>
)
