import React from 'react'
import PropTypes from 'prop-types'


export const CircleCount = ({ percent, afterText }) => (
  <div className="percentage">
    <span>{percent}{afterText}</span>
    <svg width={100} height={100} viewBox="0 0 100 100">
      <circle cx="50" cy="50" r="45" />
      <path
        strokeDasharray={`${(percent * 282) / 100}, 282`}
        d="M50 5 a 45 45 0 0 1 0 90 a 40 40 0 0 1 0 -90"
      />
    </svg>
  </div>
)

CircleCount.propTypes = {
  percent: PropTypes.number.isRequired,
  afterText: PropTypes.string,
}

CircleCount.defaultProps = {
  afterText: '',
}
