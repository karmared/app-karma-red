import React from 'react'
import Translate from 'react-translate-component'
import MediaQuery from 'react-responsive'
import {QRCode} from 'react-qr-svg'
import {Modal, Karma_input, Spinner} from './'

export const ModalQR = ({qr, token, toggleModal, activeBtn, handleInput, request, alert, secret, loading}) => {
    switch (request) {
        case 'disable':
            return (
                <Modal title={<Translate component='h4' className="heading--sm" content="modal_qr.title"/>}
                       toggleModal={toggleModal} className='modal--qr'>
                    <div>
                        <Translate component='p' className="text--main" content="modal_qr.disable_text"/>
                        {alert}
                        <Karma_input name='token' type="number" value={token} onChange={handleInput} validate='digits'
                                     placeHolder={<Translate className="placeholder" content="modal_qr.placeholder"/>}/>
                        <Translate component='button' className="btn btn--red" content="btn.submit"
                                   onClick={activeBtn}/>
                    </div>
                </Modal>
            )
        case 'check':
            return (
                <Modal key='modal' title={<Translate component='h4' className="heading--sm" content="modal_qr.title"/>}
                       toggleModal={toggleModal}>
                    <div>
                        {alert}
                        <Translate component="p" className="text--main" content="modal_qr.enter_token"/>
                        <Karma_input name='token' type="number" value={token} onChange={handleInput} validate='digits'
                                     placeHolder={<Translate className="placeholder" content="modal_qr.placeholder"/>}/>
                        <Translate component='button' className="btn btn--red" content="btn.submit"
                                   onClick={activeBtn}/>
                    </div>
                </Modal>
            )
        default:
            return (
                <Modal title={<Translate component='h4' className="heading--sm" content="modal_qr.title"/>}
                       toggleModal={toggleModal} className='modal--qr'>
                    <div>
                        <MediaQuery minWidth={810}>
                            <Translate component='p' className="text--main" content="modal_qr.text"/>
                            <span className="qr__wrapper">
                                <QRCode
                                    bgColor="#FFFFFF"
                                    fgColor="#000000"
                                    level="Q"
                                    style={{width: 256}}
                                    value={qr}
                                />
                            </span>
                        </MediaQuery>
                        <Translate component='p' className="text--main" content="modal_qr.enter_key"/>
                        <b className="qr__wrapper text--main">{secret}</b>
                        <Translate component='p' className="text--main" content="modal_qr.save_key"/>
                        {alert}
                        <Karma_input name='token' type="number" value={token} onChange={handleInput} validate='digits'
                                     placeHolder={<Translate className="placeholder" content="modal_qr.placeholder"/>}/>
                        {loading
                            ? <span className="btn btn--red btn--disabled"><Spinner className='spinner--inside'/></span>
                            : <Translate component='button' className="btn btn--red" content="btn.submit" onClick={activeBtn}/>
                        }
                    </div>
                </Modal>
            )
    }

}
