import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Translate from 'react-translate-component'

import {SvgClose} from '../../svg/'
import {sendComment} from '../../actions'
import {Avatar, Spinner} from './'

import {Message} from 'semantic-ui-react'


class ModalComment extends Component {
    state = {
        alert: null,
        success: false,
        loading: false,
        text: '',
    }

    handleInput = (e) => {
        this.setState({text: e.target.value})
    }

    handleSubmit = () => {
        this.setState({loading: true});
        this.props.sendComment({memo: this.state.text, id: this.props.borrower.id, uuid: this.props.uuid})
            .then(() => this.setState({
                alert: null,
                success: true,
                loading: false,
            }))
            .catch(error => this.setState({
                alert: `${error.message}`,
                loading: false,
            }))
    }

    render() {
        const {toggle, borrower} = this.props;
        const {alert, loading, success, text} = this.state;

        return (
            <div className="modal__wrapper">
                <div className={`modal lg modal--comment ${success ? 'modal--message' : ''}`}>

                    {success
                        ? ''
                        : <header className="modal__header">
                            <Avatar
                                img={borrower.extra ? borrower.extra.avatar.thumb.url : null}
                                className="avatar--sm"
                                name={{
                                    first_name: borrower.extra.first_name || borrower.name,
                                    last_name: borrower.extra.last_name
                                }}
                            />,
                            <span className="heading--sm">
                                    {`${borrower.extra.first_name}${borrower.extra.last_name ? ` ${borrower.extra.last_name}` : ''}`}
                                </span>
                        </header>
                    }

                    <div className="modal__content">
                        {success
                            ? <Message className='message--success one-line-block'>
                                <p>Your message has been sent.</p>
                                <button type="button" className="btn btn--link btn--red_text btn--close"
                                        onClick={toggle}>
                                    <SvgClose width={24} height={24}/>
                                </button>
                            </Message>
                            : <textarea rows="5" className="karma__input karma__input--ghost" value={text}
                                        onChange={this.handleInput}
                                        placeholder="Your comment..."/>
                        }
                    </div>
                    {success
                        ? ''
                        : <footer className="modal__footer">
                            <p key='alert' className="text--red">{alert}</p>,
                            <button key='btn' type="button" disabled={loading || !text.length}
                                    className={`btn btn--red${loading ? ' loading' : ''}`}
                                    onClick={this.handleSubmit}>
                                {loading
                                    ? <Spinner className='spinner--inside'/>
                                    : <Translate content="btn.comment_send" />
                                }
                            </button>
                        </footer>
                    }
                </div>
                <button className="bg" onClick={toggle}/>
            </div>
        )
    }

    static propTypes = {
        toggle: PropTypes.func.isRequired,
        borrower: PropTypes.objectOf(PropTypes.any).isRequired,
        sendComment: PropTypes.func.isRequired,
    }
}

const mapDispatchToProps = dispatch => ({
    sendComment: data => dispatch(sendComment(data)),
});

export default connect(null, mapDispatchToProps)(ModalComment)
