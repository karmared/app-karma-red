import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {ApiURI} from '../../libs/'

export class Avatar extends Component {

    constructor(props) {
        super(props)

        this.state = {
            classAvatar: 'avatar--square'
        }
    }

    componentDidMount() {
        this.editClass(this.props.img)
    }
    componentWillReceiveProps(nextProps) {
        this.editClass(nextProps.img)
    }

    editClass(img) {
        if(img !== null) {
            let width,
                height;

            const temp = new Image();
            temp.src = (!this.props.edit ? ApiURI.extraBase : '') + img;
            width = temp.naturalWidth;
            height = temp.naturalHeight;

            this.setState({
                classAvatar: width < height
                    ? 'avatar--vertical'
                    : width > height
                        ? 'avatar--horizontal'
                        : 'avatar--square'
            })
        }
    }

    render() {
        const { img, className, name, edit } = this.props;
        const {classAvatar } = this.state;

        let letters = '';

        if (name && name.first_name !== null) {
            letters = name.first_name.substring(0, 2);
        }

        if (name.last_name) {
            letters = name.first_name.substring(0, 1) + name.last_name.substring(0, 1)
        }

        return <span className={`${className} ${classAvatar}`}>{img && img !== null ? <img src={(!edit ? ApiURI.extraBase : '') + img}/> : letters}</span>
    }

    static propTypes = {
        img: PropTypes.string,
        className: PropTypes.string.isRequired,
        name: PropTypes.objectOf(PropTypes.any).isRequired,
    };

    static defaultProps = {
        img: '',
    };
}
