import counterpart from 'counterpart'
import React, { Component } from 'react'
import {Dropdown, Flag, Icon} from 'semantic-ui-react'

import getCookie from '../../actions/getCookie'

const locales = [
    {
        text: <span className='lang-select__item'><Flag name='gb'/>English</span>,
        value: 'en',
    },
    {
        text: <span className='lang-select__item'><Flag name='ru'/>Русский</span>,
        value: 'ru',
    },
    {
        text: <span className='lang-select__item'><Flag name='kr'/>한국어</span>,
        value: 'ko',
    },
]

export class LanguageSelect extends Component {

    handleChange = (e, { value }) => {
        counterpart.setLocale(value);
        document.cookie = `lang=${value}`;
    }

    render() {
        const lang = getCookie('lang');

        if(window.location.origin.match(/crowdin/i) !== null && locales.filter(e => e.value === 'ach').length === 0){
            locales.push({
                text: <span className='lang-select__item'><Icon className='ico--translate' name='translate'/></span>,
                value: 'ach',
            })
        }

        return (
            <Dropdown
                placeholder='Select language'
                className='lang-select'
                defaultValue={lang ? lang : 'en'}
                onChange={this.handleChange}
                selection
                scrolling={false}
                pointing='bottom'
                options={locales}
            />
        )
    }
}
