import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Translate from 'react-translate-component'

import {setPrvKey} from '../../actions/'
import {Karma_input} from '../helpers/'


class ModalRequestPassword extends Component {
    state = {
        alert: null,
        password: '',
    }

    handleInput = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const {password} = this.state
        const {setKey} = this.props

        setKey({password})
            .catch(() => {
                this.setState({alert: <Translate className="text--red" content='modal_confirm.error_pass'/>})
            })
    }

    render() {
        const {alert, password} = this.state

        return (
            <form onSubmit={this.handleSubmit} className='form--request_pass'>
                {alert
                && <div className="text--main text--red">{alert}</div>
                }

                <Karma_input
                    placeHolder={<Translate className='placeholder' content='modal_confirm.placeholder_pass'/>}
                    type="password"
                    required name="password"
                    value={password}
                    onChange={this.handleInput}/>


                <Translate component='button' type="submit" className="btn btn--red" content='btn.confirm'/>
            </form>
        )
    }

    static propTypes = {
        setKey: PropTypes.func.isRequired,
    }
}

const mapDispatchToProps = dispatch => ({
    setKey: data => dispatch(setPrvKey(data)),
})

export default connect(null, mapDispatchToProps)(ModalRequestPassword)
