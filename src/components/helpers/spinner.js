import React from 'react'

export const Spinner = ({className}) => (
  <span className={`spinner ${className ? className: ''}`}>
    <span /><span /><span /><span /><span />
  </span>
)
