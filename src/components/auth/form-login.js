import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Translate from 'react-translate-component'
import ReactGA from 'react-ga'

import {multiFactor} from '../../actions/user'

import {logIn, checkUser} from '../../actions/'
import { Spinner, Karma_input, ModalQR } from '../helpers/'
import {Karma} from '../../libs'


class FormLogin extends Component {
    state = {
        login: '',
        password: '',
        alert: '',
        loading: false,
        modal: false,
        token: '',
        modalAlert: ''
    };

    handleInput = (e) => {
        this.setState({[e.target.name]: e.target.value})
    };

    handleToken = (e) => {
        this.setState({token: e.target.value})
        if(e.target.value.length === 6){
            this.setState({loading: true})
            this.checkFA(e.target.value);
        }
    };

    handleLogin = (e) => {
        this.setState({login: e.target.value.toLowerCase()})
    };

    handleError = (alert) => {
        this.setState({alert})
    };

    toggleModal = () => {
        this.setState({ modal: !this.state.modal })
    };

    checkFA = (token) => {
        const { login, password } = this.state;
        const {multiFactor} = this.props;
        const keys = Karma.generateKeys(login, password);

        multiFactor({ operation:'GET_CHECK', username: login, keys, token })
            .then(e => {
                if(e.data.isSecondFactorActive){
                    this.logIn()
                } else {
                    this.setState({modalAlert: 'Неверный токен', token: ''});
                }
            })
            .catch(() => {
                this.setState({
                    modalAlert: <Translate className="text--red heading--sm" content='modal_qr.invalid_token' />,
                    token: ''
                });
            })
    };

    logIn = () => {
        const { login, password } = this.state;
        const {logIn: action} = this.props;
        return action({login, password})
            .catch((error) => {
                this.setState({loading: false});
                this.handleError(<span className="text--red heading--sm">{error.message}</span>)
            })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { login, password } = this.state;
        const { handleError, multiFactor, checkUser} = this.props;

        if (!login || !password) {
            return handleError(<Translate className="text--red heading--sm" content='auth.error_sign_in' />)
        }

        this.setState({loading: true});

        const keys = Karma.generateKeys(login, password)

        checkUser({login, password})
            .then(() => {
                multiFactor({ operation:'GET_STATUS', username: login, keys })
                    .then(e => {
                        if(!e.data.isSecondFactorActive){
                            this.logIn()
                        } else {
                            this.setState({modal: true})
                        }
                    })
            })
            .catch(error => {
                this.setState({loading: false});
                this.handleError(<span className="text--red heading--sm">{error.message}</span>)
            })
    };

    render() {
        const {login, password, loading, alert, modal, token, modalAlert } = this.state;

        return [
            <form onSubmit={this.handleSubmit} key='form'>
                { alert }
                <Karma_input
                    className='auth__field'
                    placeHolder={<Translate className='placeholder' content='auth.placeholder_login' />}
                    required
                    name="login"
                    value={login}
                    autoFocus
                    onChange={this.handleLogin}
                    validate="login"/>
                <Karma_input
                    className='auth__field'
                    placeHolder={<Translate className='placeholder' content='auth.placeholder_pass' />}
                    type="password"
                    required
                    name="password"
                    value={password}
                    onChange={this.handleInput}/>
                <div className='btn_wrapper'>
                    <button type="submit"
                            disabled={loading}
                            className={`btn btn--red`}>
                        { !loading
                            ? <Translate content='btn.login' />
                            : <Spinner className='spinner--inside'/>
                        }
                    </button>
                </div>
            </form>,
            modal &&
            <ModalQR
                key='modalqr'
                token={token}
                loading={loading}
                request='check'
                alert={modalAlert}
                toggleModal={this.toggleModal}
                activeBtn={() => this.checkFA(token)}
                handleInput={this.handleToken}
            />
        ]
    }

    static propTypes = {
        logIn: PropTypes.func.isRequired,
    }
}

const mapDispatchToProps = dispatch => ({
    logIn: data => dispatch(logIn(data)),
    checkUser: data => dispatch(checkUser(data)),
    multiFactor: data=> dispatch(multiFactor(data))
});

export default connect(null, mapDispatchToProps)(FormLogin)
