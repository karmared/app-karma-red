import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import _ from 'lodash'
import ReactGA from 'react-ga'
import {Redirect} from 'react-router-dom'
import {Karma} from '../../libs/'

import Translate from 'react-translate-component'

import {Checkbox} from 'semantic-ui-react'

import {register} from '../../actions/'
import {Modal, Spinner, Karma_input} from '../helpers/'

import Terms from './terms'

const checkLogin = async (login) => await Karma.dbApi.exec('get_account_by_name', [login]).then(e => e);

class FormRegister extends Component {
    state = {
        login: '',
        password: '',
        password2: '',
        loginValid: false,
        passwordValid: false,
        agreement: [false, false, false],
        loading: false,
        showModal: false,
        showConfirm: false,
        disabled: true,
        alert: '',
        enter: false
    };

    toggleConfirm = () => {
        this.setState({showConfirm: !this.state.showConfirm});
        if (this.state.showConfirm) {
            this.setState({loading: false})
        }
    };

    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };


    handleError = (alert) => {
        this.setState({alert, disabled: true})
    };

    handleLogin = (e) => {
        const login = e.target.value;
        let states = this.state,
            error = null,
            loginValid = false;

        // Huge check list for allowed login
        if (/^[\d\-]/.test(login)) {
            error = <Translate className="text--red heading--sm" content='auth.error_login_start_letter' />
        }
        else if (login.length < 3) {
            error = <Translate className="text--red heading--sm" content='auth.error_login_longer' />
        }
        // contain dot, not contain digit, not contain dash
        else if (/\./.test(login) || (!(/\d/.test(login)) && !(/\-/.test(login)) && /[aeyuio]/.test(login))) {
            // error = 'This is a premium name which is not supported now. Please enter a regular name containing at least one dash, a number or no vowels.'
            error = <Translate className="text--red text--main heading--sm" content='auth.error_login_dash_number' />
        }
        else if (_.endsWith(login, '-')) {
            error = <Translate className="text--red heading--sm" content='auth.error_login_end_letter' />
        }

        if (error) {
            this.handleError(error);
        }
        else {
            loginValid = true;
            this.handleError('');
        }

        states.login = login.toLowerCase();
        states.loginValid = loginValid;
        this.setState({login: login.toLowerCase(), loginValid});

        /*check actual states*/
        this.checkForm(states)
    };

    handlePassword = (e) => {
        let states = this.state;

        states.passwordValid = e.isValid;
        states.password = e.password;
        this.setState({passwordValid: e.isValid, password: e.password});

        this.checkForm(states);
    };

    handleRetype = (e) => {
        let states = this.state;

        states.password2 = e.target.value;
        this.setState({password2: e.target.value});

        this.checkForm(states)
    };

    handleAgreement = (index) => {
        const a = this.state.agreement;
        let states = this.state;

        a[index] = !a[index];

        states.agreement = a;
        this.setState({agreement: a});

        this.checkForm(states)
    };

    checkForm = (states) => {
        const {loginValid, passwordValid, agreement, password, password2} = states;
        if (password && password === password2 && passwordValid && agreement[1] && agreement[2] && agreement[0] && loginValid) {
            this.setState({disabled: false});
        } else {
            this.setState({disabled: true});
        }
    };

    doRegister = () => {
        const {login, password} = this.state;
        const {register: action, referrer} = this.props;

        this.setState({showConfirm: false});

        action({ login, password, referrer })
            .then(() => this.setState({enter: true}))
            .catch((error) => {
                this.setState({loading: false});
                this.handleError(error.message)
            })
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {login, password, password2, agreement, passwordValid} = this.state;
        checkLogin(login).then(e => {
            if (e !== null) {
                return this.handleError(<Translate className="text--red heading--sm" content='auth.already_taken' />)
            } else {
                if (!login || !password) {
                    return this.handleError(<Translate className="text--red heading--sm" content='auth.error_sign_in' />)
                }

                if (password !== password2) {
                    return this.handleError(<Translate className="text--red heading--sm" content='auth.error_confirm' />)
                }

                if (agreement.filter(item => !item).length > 0) {
                    let agreementError;

                    if (!agreement[0]) {
                        agreementError = <Translate className="text--red heading--sm" content='auth.ask_write_pass' with={{terms: <Translate content='auth.terms' />}} />
                    }
                    else if (!agreement[1]) {
                        agreementError = <Translate className="text--red heading--sm" content='auth.ask_write_pass' />
                    }
                    else if (!agreement[2]) {
                        agreementError = <Translate className="text--red heading--sm" content='auth.ask_recover_pass' />
                    }

                    return this.handleError(agreementError)
                }

                if (!passwordValid) {
                    return this.handleError(<Translate className="text--red heading--sm" content='auth.error_stronger_pass' />)
                }

                return this.setState({loading: true, showConfirm: true})
            }
        });
    };

    render() {
        const {login, agreement, loading, password, password2, passwordValid, showModal, showConfirm, disabled, alert, enter} = this.state;

        if(enter){
            return <Redirect to='/' />
        }

        return (

            <form onSubmit={this.handleSubmit}>
                {
                    alert && <span className="text--red heading--sm">{alert}</span>
                }
                <Karma_input className='karma__input auth__field'
                             placeHolder={<Translate className="placeholder" content='auth.placeholder_login' />}
                             required
                             value={login}
                             onChange={this.handleLogin}
                             validate="login"/>
                <Karma_input
                    passStrength
                    placeHolder={<Translate className="placeholder" content='auth.placeholder_pass' />}
                    minLength={12}
                    minScore={2}
                    value={password}
                    scoreWords={[
                        <Translate content='auth.pass_strength_weak' />,
                        <Translate content='auth.pass_strength_simple' />,
                        <Translate content='auth.pass_strength_good' />,
                        <Translate content='auth.pass_strength_strong' />,
                        <Translate content='auth.pass_strength_perfect' />
                    ]}
                    tooShortWord={<Translate content='auth.pass_strength_too_short_word' />}
                    changeCallback={this.handlePassword}
                    inputProps={{
                        name: 'password',
                        autoComplete: 'off',
                    }}
                />
                <div className="karma__field">
                    <Karma_input className='karma__input auth__field password-confirmation'
                                 placeHolder={<Translate className="placeholder" content='auth.placeholder_confirm' />}
                                 type="password"
                                 required
                                 disabled={!passwordValid}
                                 value={password2}
                                 onChange={this.handleRetype}/>
                    {(password2 && password !== password2) &&
                        <Translate component='small' className="hint text--red" content='auth.pass_mismatch' />
                    }
                    {(password2 && password === password2) &&
                        <Translate component='small' className="hint text--success" content='auth.pass_confirmed' />
                    }
                </div>

                <div className="agreements">
                    <Checkbox className='karma__checkbox auth__checkbox text--main'
                              label={<Translate component='label' content='auth.checkbox_terms' with={{terms: <Translate className='btn--red_text'
                                      onClick={this.toggleModal} content='auth.terms' />}} />}
                              defaultChecked={agreement[0]} onChange={() => this.handleAgreement(0)}/>
                    <Checkbox className='karma__checkbox auth__checkbox text--main'
                              label={<Translate component='label' content='auth.checkbox_write_pass' />}
                              defaultChecked={agreement[1]} onChange={() => this.handleAgreement(1)}/>
                    <Checkbox className='karma__checkbox auth__checkbox text--main'
                              label={<Translate component='label' content='auth.checkbox_recover_pass' />}
                              defaultChecked={agreement[2]} onChange={() => this.handleAgreement(2)}/>
                </div>
                <div className='btn_wrapper'>
                    <button type="submit" disabled={loading || disabled} className={`btn btn--red`}>
                        {!loading
                            ? <Translate content='btn.sign_up' />
                            : <Spinner className='spinner--inside'/>
                        }
                    </button>
                </div>

                {showModal &&
                <Modal title={<Translate component="h4" className='heading--sm' content='auth.terms' />} toggleModal={this.toggleModal} xl>
                    <Terms/>
                </Modal>
                }

                {showConfirm
                && <Modal
                    title={<Translate className="heading--sm" content='auth.modal_title' />}
                    className='modal--many_actions'
                    toggleModal={this.toggleConfirm}
                    lg
                    footerActions={[
                        <Translate
                            key='btn'
                            component='button'
                            type="button"
                            className="btn btn--red"
                            onClick={this.doRegister}
                            content='btn.auth_saved' />
                    ]}
                >
                    <div className='message--confirm'>
                        <Translate component='p' className='text--main' content='auth.modal_accost' />
                        <Translate component='p' className='text--main' content='auth.modal_greeting' />
                        <p className='text--main'>
                            <Translate component='b' content='auth.your_login' /><br/>
                            {login}
                        </p>
                        <p className='text--main'>
                            <Translate component='b' content='auth.your_pass' /><br/>
                            {password}
                        </p>
                        <p className="text--main text--long">
                            <Translate component='b' content='auth.your_prvkey' /><br/>
                            {Karma.generateKeys(login, password).privKey}
                        </p>
                    </div>
                </Modal>
                }


            </form>
        )
    }

    static propTypes = {
        register: PropTypes.func.isRequired
    }
}

const mapDispatchToProps = dispatch => ({
    register: data => dispatch(register(data)),
});

export default connect(null, mapDispatchToProps)(FormRegister)
