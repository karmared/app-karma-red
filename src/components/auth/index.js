import React from 'react'
import {Link} from 'react-router-dom'

import {Grid} from 'semantic-ui-react'

import {Logo} from '../../svg/'
import {LanguageSelect} from '../helpers/local_setting'
import Form from './form'

const Auth = ({register, referrer}) => {
    return (
        <section id="auth_page" className='content'>
            <Grid celled className='karma__grid container auth__container'>
                <Grid.Row className='karma__wrapper auth__wrapper'>
                    <Grid.Column
                        computer={12}
                        tablet={16}
                        className='karma__column auth__main'>
                        <Logo alt="karma.red"/>
                        <h1 className='auth__title'>Economy of Trust</h1>
                    </Grid.Column>
                    <Form register={register} referrer={referrer}/>
                </Grid.Row>
            </Grid>
            <div className='sidebar__item  sidebar__item--lang auth__lang'>
                <LanguageSelect/>
            </div>
        </section>
    )
}


export default Auth
