import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import Translate from 'react-translate-component'
import { Grid, Card } from 'semantic-ui-react'

import NoConnection from './no-connection';
import FormRegister from './form-register'
import FormLogin from './form-login'

class Form extends Component {
    state = {
        reg: false,
    };

    componentDidMount(){
        if(this.props.register){
            this.setState({reg: true})
        }
    }

    render() {
        const {reg} = this.state;
        const {init, referrer} = this.props;

        return (
            <Grid.Column computer={4}
                         tablet={16}
                         className='karma__column auth__form'>
                {
                    !init
                        ? ''
                        : <Translate
                            component='h2'
                            className='auth__header-form heading--md'
                            content={`auth.heading_${reg ? 'register' : 'login'}`} />
                }


                <Card className='auth__card'>
                    { !init
                        ? <NoConnection />
                        : [
                            reg
                                ? <FormRegister key='regiser' referrer={referrer}/>
                                : <FormLogin key='login'/>
                            ,

                            <button key='btnSubmit'
                                    type="button"
                                    className="btn btn--ghost btn--red_text"
                                    onClick={() => this.setState({reg: !this.state.reg})}>
                                <Translate content={`btn.${reg ? 'sign_in' : 'register'}`} />
                            </button>
                        ]
                    }
                </Card>

            </Grid.Column>
        )
    }

    static propTypes = {
        init: PropTypes.bool.isRequired,
    }
}

const mapStateToProps = state => ({
    init: state.authorization.init,
});

export default connect(mapStateToProps)(Form)
