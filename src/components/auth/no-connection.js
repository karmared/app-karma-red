import React, {Component} from 'react'
import Translate from 'react-translate-component'

class NoConnection extends Component {
    reload = () => {
        window.location.reload();
    };

    render() {
        return (
            <div className="form--noconnection">
                <Translate component="b" content="no_connection.title" className="heading--sm text--red"/>
                <Translate component="p" content="no_connection.text" className='text--main'/>
                <Translate
                    component="p"
                    content="no_connection.message"
                    className='text--main'
                    btn={
                       <Translate component="button" content="btn.reload" type="button" className="btn btn--red_text btn--link" onClick={this.reload}/>
                    }
                />
            </div>
        )
    }
};

export default NoConnection
