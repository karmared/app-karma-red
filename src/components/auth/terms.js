import React, { Component } from 'react'
import 'whatwg-fetch'

import { ApiURI } from '../../libs/'
import { Spinner } from '../helpers/index'


class Terms extends Component {
  state = {
    text: null,
    alert: null,
  };

  componentDidMount() {
    this.getText()
  };

  getText = async () => {
    fetch(`${ApiURI.extraApi}/static_pages/terms`)
      .then(e => e.json())
      .then((res) => {
        this.setState({ text: res.body })
      })
      .catch(error => this.setState({ alert: error.message }))
  };

  render() {
    const { text, alert } = this.state;

    if (!text) {
      return <Spinner />
    }

    if (alert) {
      return (
        <div className="alert alert-warning">{alert}</div>
      )
    }

    return (
      <div className="std" dangerouslySetInnerHTML={{ __html: text }} />
    )
  }
}

export default Terms
