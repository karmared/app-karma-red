const merge = require('webpack-merge')
const {optimize} = require('webpack')
const webpack = require('webpack')
const UglifyJs = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')


const {config} = require('./common')
const extractSass = new ExtractTextPlugin({
    filename: '[name].css',
})

module.exports = merge(config, {
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: [
                        'css-loader',
                        'postcss-loader',
                        'sass-loader',
                    ],
                    fallback: 'style-loader',
                }),
            },
        ],
    },

    cache: false,
    watch: false,
    profile: true,

    plugins: [
        new UglifyJs({uglifyOptions: {compress: {warnings: false}}}),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new optimize.ModuleConcatenationPlugin(),
        extractSass,
    ],
})
