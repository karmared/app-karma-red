To install:

1) Clone this git to local directory

2) Run $: npm install

3) To run development build (with hot reload) run $: npm run dev

4) To build production build run $: NODE_ENV=production npm run build -- -p

5) To check all project with ESLINT run $: npm test
